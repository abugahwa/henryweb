<?php

Route::get('list-banner', 'BannerController@index')->name('banner.list');
Route::get('add-banner', 'BannerController@create')->name('banner.add');
Route::get('edit-banner/{id}', 'BannerController@edit')->name('banner.edit');
Route::post('save-banner', 'BannerController@store')->name('banner.save');
Route::post('update-banner/{id}', 'BannerController@update')->name('banner.update');
Route::post('delete-banner/{id}', 'BannerController@destroy')->name('banner.delete');