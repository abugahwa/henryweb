<?php

namespace App\Classes;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

use App\Models\MUserProfile;

use App\Models\MPost;
use App\User;
use App\Models\MPostCategory;


use DB;
use Auth;
use Carbon;

class Profile extends BaseClass {
	
	public function insertProfile(Request $request, $id)
	{
		$userid								= MUserProfile::where('id_user', $id)->get();
		if($userid->count() > 0 ){
			foreach($userid as $b){
				$id							= $b->id_user;
			}
			$detail							= MUserProfile::find($id);
			
			$detail->first_name				= $request->first_name;
			$detail->last_name				= $request->last_name;
			$detail->company				= $request->company;
			$detail->brand					= $request->brand;
			$detail->phone					= $request->phone;
			$detail->mobile_phone			= $request->mobile_phone;
			$detail->address				= $request->address;
			$detail->id_user				= \Auth::user()->id;
			$detail->gender					= $request->gender;
			$detail->save();
		}else{
			$detail							= new MUserProfile;
			$detail->first_name				= $request->first_name;
			$detail->last_name				= $request->last_name;
			$detail->company				= $request->company;
			$detail->brand					= $request->brand;
			$detail->phone					= $request->phone;
			$detail->mobile_phone			= $request->mobile_phone;
			$detail->address				= $request->address;
			$detail->id_user				= \Auth::user()->id;
			$detail->gender					= $request->gender;
		$detail->save();
		}
		
		$pass								= $request->password;
		\DB::update('update users set password = ? where id = ?',[bcrypt($pass),$detail->id_user]);
		\DB::update('update users set name = ? where id = ?',[$detail->first_name,$detail->id_user]);
		
		return $detail;
	}
	public function profileUpdate(Request $request)
	{
		$id						= $request->id;
		$name_image = Str::random(10);
		$nm			= \Auth::user()->name;
		
		$names		= "".$name_image."-".$nm."" ;
		
		$strings                = new StringClass();
        $title_img             = $strings->str2alias($names);
		
        $now = \Carbon\Carbon::now();
		$year = date('Y', strtotime($now));
		$month = date('m', strtotime($now));
        $days = date('d', strtotime($now));

        $bs                     = $request->file('b_image')->getClientOriginalExtension();
        $nombreCarpeta = preg_replace('/\s+/', '.', $year . "/" . $month . "/" . $days);
        $fileimg                = $title_img . '.' .$bs;
        $user_img               = 'img/user/'.$nombreCarpeta .'/' .$fileimg;
        $path = base_path() .'/public/img/user/'.$nombreCarpeta;
		
		\DB::update('update users set avatar = ? where id = ?',[$user_img,\Auth::user()->id]);
		
		$imageName = $strings->str2alias($names);
        $nmImg      = strtolower($imageName) . '.' .
        $request->file('b_image')->getClientOriginalExtension();
		$request->file('b_image')->move($path, $nmImg);
		
		$datas					= User::join('user_profile','users.id','user_profile.id_user')
								->find($id);
		return $datas;
	}
	public function getGender($id)
	{
		$catid          				= MUserProfile::where('id', $id)->first()->gender;
        $profile        				= MUserProfile::groupBy('gender')->get();
		
		if($profile->count()){
            foreach($profile as $role){
				echo '<option value="'.$role->gender.'" '.($catid == $role->gender ?'selected="selected"':"").'>'.$role->gender.'</option>';
            }
			echo'<option value="female">Female</option>';
		}
		return $profile;
	}
	public function UpdateDataUser(Request $request)
	{
		$id								= $request->id;
		
		$data							= MUserProfile::find($id);
		
		$data->first_name				= $request->first_name;
		$data->last_name				= $request->last_name;
		$data->company					= $request->company;
		$data->brand					= $request->brand;
		$data->mobile_phone				= $request->mobile_phone;
		$data->phone					= $request->phone;
		$data->address					= $request->address;
		$data->id_user					= \Auth::user()->id;
		$data->gender					= $request->gender;
		
		$data->save();
		
		$fullname						= '' . $data->first_name . ' '. $data->last_name .'';
		\DB::update('update users set name = ? where id = ?',[$fullname,$data->id_user]);
		
		return $data;
	}
}