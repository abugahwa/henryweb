<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\User;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Social\FacebookServiceProvider;
use GuzzleHttp\Exception\ClientException;
use League\OAuth1\Client\Credentials\CredentialsException;

use Socialite;
use Auth;
use Exception;

class TwitterLogin extends Controller
{
     public function redirectToProvider()
    {
        return Socialite::driver('twitter')->redirect();
    }
	public function handleProviderCallback()
    {
		try {
            $user = Socialite::driver('twitter')->user();
            
			$authUser = $this->findOrCreateUser($user,'twitter');
			Auth::login($authUser, true);
			
			$id 		= $user->getId();
			$name 		= $user->getName();
			$email 		= $user->getEmail();
            return redirect()->route('homemember')
							->with('id', $id)
							->with('name', $name)
							->with('email', $email);
			;
        } catch (Exception $e) {
            return redirect('loginmember');
        }
    }
	private function findOrCreateUser($provideruser,$provider)
    {
        $authUser = User::where('email', $provideruser->email)->first();
 
        if ($authUser){
            return $authUser;
        }
        return User::create([
            'name' => $provideruser->name,
            'email' => $provideruser->getEmail(),
            'department_id' => 3,
            'status' => 'activated',
            'social_id' => $provideruser->getId(),
			'social_provider' => $provider,
            'avatar' => $provideruser->getAvatar()
        ]);
    }
}
