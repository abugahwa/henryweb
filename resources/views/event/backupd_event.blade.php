<!DOCTYPE html>
<html>
<head>
  <title>{{ $txt_events->GetName($idmenu) }}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{ asset('assets/bower_components/font-awesome/web-fonts-with-css/css/fontawesome-all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/event/css/animate.css') }}">
     <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/event/css/icomoon.css') }}">
    <link rel="stylesheet" href="{{ asset('css/event/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/event/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/event/css/style.css') }}">
  
  
  
<!--  <script src="{{ asset('js/custom.js') }}"></script>-->
  <style>
 @import url(http://fonts.googleapis.com/css?family=Kreon:400,700);
#nav{
	z-index: 999;
}
.thumbnail {
	margin-bottom:7px;
}
@media (min-width: 767px) {
	.logo_footer{
		float: left;
		width: 105px;
		padding: 10px;
	}
	.logo_footer img{
		width: 100%;
	}
	.animate-box img{
		width: 100%;
	}
 }
.navbar-expand-sm {
    background-color: #292b2c!important;
	color: #fff !important;
}
.navbar-light .navbar-nav .nav-link{
	color: #fff !important;
}
.navbar-inverse .navbar-nav .active>.nav-link, .navbar-inverse .navbar-nav .nav-link.active, .navbar-inverse .navbar-nav .nav-link.open, .navbar-inverse .navbar-nav .open>.nav-link {
    color: #fff;
}
.tab-content{
	height: 606px;
    overflow-y: scroll;
}
footer{
	background: #c69b2c;
    padding: 32px;
}
.sect .list-group-item.active {
    z-index: 2;
    color: #fff;
    background-color: #c79c29;
    border-color: #c79c29;
}
.sect a{
	color: #000000 !important;
}
.sect .list-group-item{
	background: #efefef;
	text-align: center;
}
.sect img{
	margin: 16px 37px;
}
@media screen and (max-width: 768px) {
    .visible-xs-inline {
        display: inline-block; !important
    }
    .visible-xs-ds {
        display: none; !important
    }
}

@media screen and (min-width: 768px) {
    .visible-xs-inline {
        display: none; !important
    }
    .visible-xs-ds {
        display: block; !important
    }
}
.events{
	background-color: #efefef !important;
	width: 100%;
}
.events a{
	color: #000000 !important;
}
  </style>
</head>
<body>
<div id="colorlib-page">
	<div class="container row-space">
      <div class="row row-space">
          <div class="col-md-8">
            <img class="img-responsive img-fluid" src="{{ asset('assets/images/logo-infobrand.png') }}" alt="infobrand" />
          </div>
          <div class="col-md-4">
            <div class="pull-right hidden-lg-down" style="padding:18px 0px;">

                <a href="http://trasnco.com/event/1100001376/pertama-di-indonesia.html">
                   <img src="{{ asset('images/TrasNCo.png') }}" width="100%" alt="Tras N CO Indonesia" class="img-responsive center-block">
                </a>
                </div>
            </div>
          </div>
      </div>
	<nav class="navbar navbar-expand-sm navbar-light bg-faded navbar-inverse navbar-infobrand">
		<div class="container">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-content" aria-controls="nav-content" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="nav-content">   
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="{{ route('home') }}"><span style="font-size:15pt;" class="fa fa-home"></span> <span class="sr-only">(current)</span></a>
				  </li>
				  <li class="nav-item active">
					<a class="nav-link" href="{{ URL::route('posts.blog', 'opinions') }}">{{ strtoupper('Opinions') }}</a>
				  </li>
				  <li class="nav-item active">
					<a class="nav-link" href="{{ route('posts.blog', 'brand-headline') }}">{{ strtoupper('Brand Headline') }}</a>
				  </li>
				  <li class="nav-item active">
					<a class="nav-link" href="{{ route('posts.blog', 'brand-update') }}">{{ strtoupper('Brand Update') }}</a>
				  </li>
				  <li class="nav-item active">
					<a class="nav-link" href="{{ route('videos.index') }}">{{ strtoupper('InfoBrand.tv') }}</a>
				  </li>
				  <li class="nav-item active">
					<a class="nav-link" href="{{ route('photos.view', 6) }}">{{ strtoupper('Brand Gallery') }}</a>
				  </li>
			</ul>
		</div>
		</div>
	</nav>
	@if(empty($images_slide))
	@else
	<img src="{{ asset($images_slide)}}" class="img-responsive center-block" width="100%">
	@endif
				<nav class="navbar navbar-expand-sm navbar-light bg-faded navbar-inverse events visible-xs-inline">
					<div class="container">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#nav-tabs" aria-controls="nav-content" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
					</button>

					<div class="collapse navbar-collapse" id="nav-tabs">   
						<ul class="navbar-nav nav nav-tabs">
							@foreach($menu as $b)
							<li class="nav-link"><a href="#{{ $b->menu_alias }}" data-toggle="tab">{{ $b->menu_name }}</a></li>
							@endforeach
						</ul>
					</div>
					</div>
				</nav>
	<div class="container" id="tabs">
		<div class="row">
			<ul class="col-md-3 nav nav-tabs list-group sect visible-xs-ds">
				<li class="list-group-item"><img src="{{ asset($images_icon)}}" class="img-responsive center-block"></li>
				<li class="list-group-item active">{{ $txt_events->GetName($idmenu) }}<li>
						 <?php $n = 0;?>
						@foreach($menu as $b)
							<li class="list-group-item"><a href="#{{ $b->menu_alias }}" data-toggle="tab">{{ $b->menu_name }}</a></li>
						@endforeach
				
			</ul>
			
		
			<div class="col-md-9">
				<div class="tab-content">
				<?php $n = 0;?>
				@foreach($menu as $txt)
				<section class="colorlib-about tab-pane <?php if($n == 0) echo "active"; $n++; ?>" id="{{ $txt->menu_alias}}">
					<div class="colorlib-narrow-content animate-box fadeInLeft animated">
						<span class="heading-meta">{{ $txt_events->GetName($idmenu)}}</span>
						<h2 class="colorlib-heading">{{ $txt->menu_name}}</h2>
						@foreach($txt_events->GetTextsByMenuID($txt->id) as $txtEvent) 
						{!! $txtEvent->css !!}
						{!! $txtEvent->text_event !!}
						@endforeach
					</div>
				</section>
				@endforeach

				</div>
			</div>
		</div>
	</div>

<footer class="page-footer blue text-center text-md-left mt-0">
    <div class="container row-space">
		<div class="row">
			<div class="col-md-6">
				<h5 class="title mb-3">PT TRAS MEDIACOM</h5>
				Rukan Avenue blok 8-139 Jakarta Garden City<br />
				Cakung Timur, Cakung<br />
				Jakarta Timur 13910 - Indonesia<br />
				Telp. : (021) 224 65 480<br />
				Faks. : (021) 224 65 586<br />
          		Email : infobrandindonesia@gmail.com<br /><br />
            </div>
            <div class="col-md-6">
				<h5 class="title mb-3">Our Award</h5>
				<div class="logo_footer">
					<a href="{{ url('pertama-di-indonesia.html') }}"><img src="images/logo/1.png" class="img-responsive center-block"></a>
				</div>
				<div class="logo_footer">
				<a href="{{ url('top-digital-pr.html') }}"><img src="images/logo/2.png" class="img-responsive center-block"></a>
				</div>
				<div class="logo_footer">
				<a href="{{ url('digital-popular-brand-award.html') }}"><img src="images/logo/3.png" class="img-responsive center-block"></a>
				</div>
				<div class="logo_footer">
				<a href="{{ url('mobile-application-choice.html') }}"><img src="images/logo/4.png" class="img-responsive center-block"></a>
				</div>
				<div class="logo_footer">
				<a href="{{ url('anugerah-brand-indonesia.html') }}"><img src="images/logo/5.png" class="img-responsive center-block">
				</div>
			</div>
        </div>
    </div>
</footer>

  
<ul class="nav pull-right scroll-top">
  <li><a href="#" title="Scroll to top"><i class="glyphicon glyphicon-chevron-up"></i></a></li>
</ul>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
 <script src="http://leafo.net/sticky-kit/src/jquery.sticky-kit.js"></script>
 <script src="{{ asset('css/event/js/jquery.easing.1.3.js') }}"></script>
	<script src="{{ asset('css/event/js/owl.carousel.min.js') }}"></script>
	
<script>


$(function(){
	$('body').on('click', '#video_rows a', function(e) {
		e.preventDefault();
		$("#LoadingDulu").html("<div id='LoadingContent'><i class='fa fa-spinner fa-spin'></i> Mohon tunggu ....</div>");
		$("#LoadingDulu").show();
		$( "#video_rows a" ).attr( "disabled", true );
		var url = $(this).attr('href');  
		getArticles(url);
		window.history.pushState("", "", url);
		return false;
	});

	function getArticles(url) {
		$.ajax({
			url : url  
		}).done(function (data) {
			$('#video_rows2').html(data);  
			$("#LoadingDulu").fadeOut();
		}).fail(function () {
			alert('Articles could not be loaded.');
		});
	}
});

$(".nav-link a").click(function(){
	$("#nav-tabs").collapse('hide');
});
</script>
</body>
</html>
