<?php

namespace App\Classes;

use Illuminate\Http\Request;
use App\Models\MBanners;

class Banners extends BaseClass {

	public function getById($id) {
		return MBanners::findOrFail($id);
	}

	public function getByPosition($pos) {
		return MBanners::where('b_category', $pos)->get();
	}

	public function getByCatId($bc_id) {
		$now			= "";
		$bcresult = MBanners::where('b_status', 1)
													->where('b_category', $bc_id)
													//->where('b_start_publishing', "<=", $now)
													//->where('b_finish_publishing', ">=", $now)
													->orderBy('b_sort_order')
													->get(); // b_start_publishing / b_finish_publishing

		$res = "";
		foreach($bcresult as $row) {
			$res .= $this->showBanner($row->id);
		}

		return $res;
	}

	public function showBanner($b_id, $class = "") {
		$row			= $this->getById($b_id);

			return "	<div style='margin-bottom:10px;' class='banner $class'>\n" .
						 "		<a rel='nofollow' target='_blank' href='". $row->b_link ."'>\n" .
						 "      <img class='img-responsive img-fullwidth' src='". route('cdn', $row->b_image) ."' alt='". $row->b_name ."' />\n" .
						 "    </a>\n" .
						 "	</div>\n<hr>\n";

	}

	public function addHits($id) {
		return MBanners::whereId($id)->increment('hits');
	}

}
