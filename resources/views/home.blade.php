@extends('layouts.admin')
@section('content')
	<div class="" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Dashboard User</h3>
            </div>
			<div class="title_right">
				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('/query/') }}" method="GET" role="search">
					<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
						<div class="input-group">
							<input type="text" class="form-control" name="search" placeholder="Search for...">
							<span class="input-group-btn">
							  <button class="btn btn-default" type="button">Go!</button>
							</span>
					  </div>
					</div>
				</form>
			</div>
    </div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Dashboard</h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Settings 1</a></li>
									<li><a href="#">Settings 2</a></li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>

					<div class="x_content">

						<div class="panel panel-info">
							<div class="panel-heading">Popular in This Month</div>
							<div class="panel-body">
								@foreach($post as $pos)
									<p><a target="_blank" href="{{ url($pos->post_alias.'.phtml')}}">{{ $pos->post_title }}</a> </p>
									<p><i class="fa fa-eye" style="color: #1e277d;"></i> {{ $pos->post_hits }} </p>
								@endforeach

							</div>
						</div>
						<div id="container"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script src="{{ asset('assets/bower_components/gentelella/vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.4/highcharts.js" type="text/javascript"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">


Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Click Article'
    },

    xAxis: {
       min: 0,
            max: 5,
            type: 'category',
        plotBands: [{ // visualize the weekend

            color: 'rgba(68, 170, 213, .2)'
        }]
    },
     yAxis: [{
        className: 'highcharts-color-0',
        title: {
            text: 'Primary axis'
        }
    },],
	labels: {
        items: [{
            html: 'Total Click',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
    tooltip: {
        shared: true,
        valueSuffix: ' Article'
    },
    credits: {
        enabled: false
    },
  plotOptions: {
        column: {
            borderRadius: 5
        }
    },
    series: [
			{
				name: 'Click Article',
				data: [
					<?php echo $chrt ?>
				]
			}
			]
});
</script>
@endsection
