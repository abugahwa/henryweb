<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function(Blueprint $table)	{
  			$table->increments('id');
  			$table->string('title');
  			$table->string('slug');
  			$table->string('file');
  			$table->string('link');
  			$table->text('description');
  			$table->integer('album_id');
  			$table->integer('sort_order');

  			$table->integer('user_id');

  			$table->integer('is_deleted')->default(0);
  			$table->timestamps();
  			$table->softDeletes();
  		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
