@foreach($video as $videos)
<div class="card" id="categories_card">
    <div class="row ">
        <div class="col-md-4">
            <img src="https://img.youtube.com/vi/{{ $videos->video_id }}/0.jpg" class="w-100">
        </div>
        <div class="col-md-8 px-3">
            <div class="card-block px-3">
                <h4 class="card-title"><a href="{{ url('event/'.$alias .'/video/'.$videos->id) }}">{{ $videos->title }}</a></h4>
                <p class="card-text">{!! str_limit(strip_tags($videos->description), $limit = 280, $end = '...') !!}</p>
            </div>
        </div>
    </div>
  </div>
  @endforeach
  <div class="float-right">
    {{ $video->appends(\Request::except('_token'))->render('pagination::bootstrap-4') }}
  </div>
