@extends('layouts.event')
@section('content')
<style>
#video-player {
  background-color: #000000;
  color: #FFFFFF;
  margin-bottom: 10px;
}

.playlist {
  position: relative;
  margin: 0px !important;
  padding: 5px !important;
}

.playlist a {
  color: #000000;
}

.playlist a:hover {
  text-decoration: none;
}

.playlist .image img {
  width:100%;
  margin-bottom: 10px;
}

.playlist .body {
  display:block;
  overflow-y: hidden;
  height: 60px;
  line-height: 15pt;
}
.media-body{
  padding-left: 10px;
}
</style>


    <div class="container">
      <div class="" style="margin:10px 0px;"></div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-9">

            <div id="video-player">
              <iframe class="youtube mx-auto d-block p-3" src="https://www.youtube-nocookie.com/embed/{{ $video->video_id }}?rel=0&amp;controls=0" width="720" height="405" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>

            <div class="">
              <h1 style="font-size:20pt;"><strong>{{ $video->title }}</strong></h1>
              <p>{{ $video->description }}</p>
              <p style="float:left;"><small>{{ $video->hits }}x ditonton</small></p>
              <div style="float:right;">

                    <!-- AddToAny BEGIN -->
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="display:block;margin:0px auto;">
                    <a class="a2a_button_facebook"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_google_plus"></a>
                    <a class="a2a_button_whatsapp"></a>
                    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                    </div>
                    <script async src="https://static.addtoany.com/menu/page.js"></script>
                    <!-- AddToAny END -->

              </div>
              <div class="clearfix"></div>
            </div>
            <hr />

            <div class="row">

              <div class="col-md-12">
                <h2 class="category_title">{{ strtoupper('Video Lainnya') }}</h2>

                <div class="row">
                <?php foreach($videos as $vid) { ?>
                <div class="col-md-3 playlist">
                  <div class="">
                    <div class="image">
                        <img class="" src="https://img.youtube.com/vi/{{ $vid->video_id }}/0.jpg" alt="" />
                    </div>
                    <div class="body">
                        <p><a href="{{ url('event/'.$alias .'/video/'.$vid->id) }}"><strong>{{ $vid->title }}</strong></a><br />
                    </div>
                    <p><small>{{ $vid->hits }}x ditonton</small>
                  </div>
                </div>
                <?php } ?>

                </div>
              </div>

            </div>

        </div>

        <div class="col-md-3 order-3">
            <div class="sticky_column" data-sticky_column="">
            <h2 class="category_title">{{ strtoupper('Most Viewed') }}</h2>
            <?php foreach($most as $m) { ?>
            <div class="media">
              <div class="media-left">
                  <img class="media-object" src="https://img.youtube.com/vi/{{ $m->video_id }}/0.jpg" alt="{{ $m->title }}" width="120" height="120" />
              </div>
              <div class="media-body">
                  <p style="font-size:10pt;"><a href="{{ url('event/'.$alias .'/video/'.$m->id) }}">{{ ucwords($m->title) }}</a></p>
                  <p style="font-size:10pt;"> {!! str_limit(strip_tags($m->description), $limit = 40, $end = '...') !!}</p>
              </div>
            </div>
            <hr />
            <?php } ?>
          </div>

        </div>

      </div>
    </div>

@endsection
