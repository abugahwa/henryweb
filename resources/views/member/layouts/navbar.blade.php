<?php
			$userid			=  \Auth::user()->id;
		
					use App\User;
					$user			= User::where('id', $userid)->get();
					foreach($user as $b){
						$image			= $b->avatar;
						$name			= $b->name;
					}
	?>
<h4 class="text-center">Dashboard Infobrand</h4>
<nav class="navbar navbar-default">
                    <div class="container-fluid">

                        <div class="navbar-header">
                            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                                <i class="glyphicon glyphicon-align-left"></i>
                                <span>Toggle Sidebar</span>
                            </button>
                        </div>

                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right info">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle profile-image" data-toggle="dropdown">
									<img src="{{asset($image)}}" width="20%" class="img-circle special-img"> {{ $name }} <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li> <a href="{{ url('/profile-member') }}">Account</a></li>
										<li>
										<a href="{{ route('logoutmember') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
											Logout
										</a>
										<form id="logout-form" action="{{ route('logoutmember') }}" method="POST" style="display: none;">
											{{ csrf_field() }}
										</form>
										</li>
									</ul>
								</li>
                            </ul>
                        </div>
                    </div>
                </nav>
