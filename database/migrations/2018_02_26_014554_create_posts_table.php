<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('post_title');
			$table->string('post_alias');
			$table->text('post_lead_in', 65535)->nullable();
			$table->string('post_type', 55)->default('post');
			$table->integer('post_category')->default(1);
			$table->string('post_img');
			$table->string('post_img_caption', 225);
			$table->text('post_text');
			$table->string('post_file');
			$table->dateTime('post_date_added')->nullabel();
			$table->dateTime('post_last_modified')->nullabel();
			$table->integer('post_status');
			$table->integer('post_author')->default(1);
			$table->bigInteger('post_hits');
			$table->string('post_tags');
			$table->string('post_lang')->default('id');
			$table->integer('brand_related')->default(0);
			$table->dateTime('start_publishing');
			$table->dateTime('finish_publishing');
			$table->integer('comment_status')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
