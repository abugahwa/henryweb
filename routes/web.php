<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// If your Laravel is not working after some modifications in .env
//
// php artisan config:clear
// php artisan cache:clear
// composer dump-autoload
//

/*
Route::group(array('domain' => env('CDN_URL')), function() {
    Route::get('', function () {
      //return abort(404);
    });

    Route::get('/images/{path}', 'CDNController@image')->name('cdn.image')->where('path', '(.*)');
    Route::options('/images/{path}', 'CDNController@image')->name('cdn');

    Route::get('/banners/{path}', 'CDNController@banner')->name('cdn.banner');
});
*/

Route::prefix('/iamadmin')->group(function () {
    Route::get('', function () {
        return view('admin.index');
    })->name('admin.home');

    Route::get('posts',               'Admin\PostsController@index')->name('admin.posts');

    Route::get('photos',              'Admin\PhotosController@index')->name('admin.photos');
    Route::get('photos/add',          'Admin\PhotosController@add')->name('admin.photos.add');
    Route::post('photos/store',       'Admin\PhotosController@store')->name('admin.photos.store');
    Route::get('photos/edit/{id}',    'Admin\PhotosController@edit')->name('admin.photos.edit');
    Route::post('photos/update/{id}', 'Admin\PhotosController@update')->name('admin.photos.update');

    Route::get('photos-albums',       'Admin\PhotosController@index')->name('admin.photosalbums');

    Route::get('videos',              'Admin\VideosController@index')->name('admin.videos');
    Route::get('videos/add',          'Admin\VideosController@add')->name('admin.videos.add');
    Route::post('videos/store',       'Admin\VideosController@store')->name('admin.videos.store');
    Route::get('videos/edit/{id}',    'Admin\VideosController@edit')->name('admin.videos.edit');
    Route::post('videos/update/{id}', 'Admin\VideosController@update')->name('admin.videos.update');

    Route::get('auth/logout',         'Admin\AuthController@logout')->name('admin.logout');
});

/* ###################################################################### */
Route::get('/', 'HomeController@index')->name('home');
Route::get('/contact', 'HomeController@contact')->name('home.contact');
Route::post('/konsultasi/store', 'KonsultasiController@store')->name('konsultasi.store');

Route::get('/hip-lawfirm', 'HIPController@index')->name('hip.home');
Route::get('/hip-lawfirm/{alias}.phtml', 'HIPController@profile')->name('hip.profiles');

Route::get('/{alias}.phtml', 'PostsController@view')->name('posts.view');
Route::get('/c/{alias}', 'PostsController@blog')->name('posts.blog');
Route::get('/search', 'PostsController@search')->name('posts.search');

Route::get('/videos', 'VideosController@index')->name('videos.index');
Route::get('/videos/category/{id}', 'VideosController@detail')->name('videoscatdetail');
Route::get('/videos/view/{id}', 'VideosController@view')->name('videos.view');

Route::get('/photos', 'PhotosController@index')->name('photos.index');
Route::get('/photos/view/{id}', 'PhotosController@view')->name('photos.view');

Route::get('/admin', 'AdminController@index')->name('adminhome');

Route::get('/posts/rss', 'PostsController@rss')->name('posts.rss');
Route::get('/tag/{tag}/', 'PostsController@tag')->name('posts.tag');

Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);
Route::get('405',['as'=>'405','uses'=>'ErrorHandlerController@errorCode405']);

include_once base_path("routes/admin/") . "login.php";
include_once base_path("routes/admin/") . "posts.php";
include_once base_path("routes/admin/") . "banners.php";
include_once base_path("routes/admin/") . "galleries.php";
include_once base_path("routes/admin/") . "videos.php";
include_once base_path("routes/admin/") . "members.php";
include_once base_path("routes/admin/") . "members.php";
include_once base_path("routes/admin/") . "event.php";

//Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/laravel-filemanager', '\Unisharp\Laravelfilemanager\controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\Unisharp\Laravelfilemanager\controllers\UploadController@upload');
    // list all lfm routes here...
});


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});
