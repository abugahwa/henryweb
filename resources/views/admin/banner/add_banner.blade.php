@extends('layouts.admin')
@section('content')
<link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
	<div class="" role="main">
		<div class="page-title">
			<div class="title_left">
					<h3>Add Post</h3>
			</div>
			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search for...">
						<span class="input-group-btn">
						  <button class="btn btn-default" type="button">Go!</button>
						</span>
                  </div>
                </div>
			</div>
        </div>
		
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<div class="row">
							<div class="col-md-6">
								<div class="btn-group btn-breadcrumb">
									<a href="{{ url('/') }}" class="btn btn-default"><i class="glyphicon glyphicon-home"></i></a>
									<a href="{{ url()->previous() }}" class="btn btn-default">Banner</a>
									<a href="#" class="btn btn-default">Add Banner</a>
								</div>
							</div>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#">Settings 1</a></li>
										<li><a href="#">Settings 2</a></li>
									</ul>
								</li>
								<li><a class="close-link"><i class="fa fa-close"></i></a></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					
					<div class="x_content">
						{{ csrf_field() }}
						{{ method_field('post') }}
							{!! Form::open(['method' => 'POST','route' => ['banner.save'],'files' => true, 'class' => 'form-horizontal'])  !!}
								<input type="hidden" name="id_user" value="<?php echo \Auth::user()->id;?>">
								@include('admin.banner.form_banner')
							{!! Form::close() !!}		
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="{{ asset('assets/bower_components/gentelella/vendors/jquery/dist/jquery.min.js') }}"></script>
	
	<script src="{{ asset('js/bootstrap-datetimepicker.js') }}" charset="UTF-8"></script>
	<script src="{{ asset('js/bootstrap-datetimepicker.fr.js') }}" charset="UTF-8"></script>


<script type="text/javascript">
    $('#start_publishing').datetimepicker({
        format: 'yyyy-mm-dd hh:ii'
    }); 
    $('#finish_publishing').datetimepicker({
        format: 'yyyy-mm-dd hh:ii'
    });
</script>  
@endsection


