<?php

Route::get('post-list', 'Admin\PostsController@index')->name('posts.list');
Route::get('add-post', 'Admin\PostsController@create')->name('posts.add');
//Route::get('edit-post/{id}', 'Admin\PostsController@edit')->name('posts.edit');
Route::post('edit-post', 'Admin\PostsController@edit')->name('posts.edit');
Route::post('save-post', 'Admin\PostsController@store')->name('posts.save-post');
Route::post('update-post/{id}', 'Admin\PostsController@update')->name('posts.update');
Route::get('delete-post/{id}', 'Admin\PostsController@destroy')->name('posts.delete');

Route::get('category-posts', 'Admin\PostsController@CategoryView')->name('categorypost');
Route::post('insert-category-post', 'Admin\PostsController@InsertCategories')->name('insertcategorypost');
Route::get('edit-category-posts/{id}', 'Admin\PostsController@EditCategory')->name('editcategorypost');
Route::post('update-category-post/{id}', 'Admin\PostsController@UpdateCategories')->name('updatecategorypost');
Route::get('delete-category-posts/{id}', 'Admin\PostsController@DeleteCategories')->name('deletecategorypost');

Route::get('viewer-report', 'Admin\Chartbar@index')->name('chartviewer');

