<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <img width="500" class="img-fluid" src="{{ asset('images/hip-law-office.jpg') }}" />
    </div>
    <div class="col-md-12">
      <div id="hiplawoffice" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="row">
              <div class="col-md-6">
                <img style="width:300px;" class="d-block mx-auto" src="{{ asset('images/lawyers/2.png') }}" alt="HIP Lawyers">
              </div>
              <div style="font-size:16pt;" class="col-md-6 py-4">
                Pengacara<br />
                <h3>Henry Indraguna</h3><br />
                <em style="font-size:20pt;">“Kalau mencari kebenaran, carilah saya. Tapi kalau mau cari kemenangan, carilah pengacara lain”</em>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="row">
              <div class="col-md-6">
                <img style="width:300px;" class="d-block mx-auto" src="{{ asset('images/lawyers/3.png') }}" alt="HIP Lawyers">
              </div>
              <div style="font-size:16pt;" class="col-md-6 py-4">
                Pengacara<br />
                <h3>Adi Sutrisno Simanjuntak</h3><br />
                <em style="font-size:20pt;">“Perkara apapun harus dimenangkan, dengan tetap berpijak pada kebenaran.”</em>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="row">
              <div class="col-md-6">
                <img style="width:300px;" class="d-block mx-auto" src="{{ asset('images/lawyers/4.png') }}" alt="HIP Lawyers">
              </div>
              <div style="font-size:16pt;" class="col-md-6 py-4">
                Pengacara<br />
                <h3>Kayaruddin Hasibuan</h3><br />
                <em style="font-size:20pt;">“Tegakkan Hukum untuk mencapai keadilan.”</em>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="row">
              <div class="col-md-6">
                <img style="width:300px;" class="d-block mx-auto" src="{{ asset('images/lawyers/5.png') }}" alt="HIP Lawyers">
              </div>
              <div style="font-size:16pt;" class="col-md-6 py-4">
                Pengacara<br />
                <h3>Hendry Sangapta Sitepu</h3><br />
                <em style="font-size:20pt;">“Justitia et Pereat Mundus (hendaklah keadilan ditegakkan agar langit tidak runtuh).”</em>
              </div>
            </div>
          </div>

          <div class="carousel-item">
            <div class="row">
              <div class="col-md-6 py-4">
                <img style="width:300px;" class="d-block mx-auto" src="{{ asset('images/lawyers/6.png') }}" alt="HIP Lawyers">
              </div>
              <div style="font-size:16pt;" class="col-md-6">
                Pengacara<br />
                <h3>Apriwanto Manik</h3><br />
                <em style="font-size:20pt;">“Tegakkan keadilan walaupun langit akan Runtuh. Carilah keadilan yang seadil-adilnya tanpa mengesampingkan kebenaran.”</em>
              </div>
            </div>
          </div>

          <a class="carousel-control-prev" href="#hiplawoffice" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#hiplawoffice" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>

      <div class="text-center py-4"><a href="#konsultasi"><button class="btn btn-lg btn-secondary">Set Up Meeting</button></a></div>
    </div>
  </div>
</div>
