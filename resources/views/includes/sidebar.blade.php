@guest

@else
<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ url('/') }}" class="site_title"><!--<i class="fa fa-paw"></i>--><img src="{{ asset('img/LOGO_INFOBRAND_TRANSPARANT-04.png') }}" width="100"> <span>InfoBrand</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
				<img src="{{ asset('assets/bower_components/gentelella/production/images/img.jpg') }}" alt="Avatar of {{ Auth::user()->name }}" class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{ Auth::user()->name }}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br />

        <!-- sidebar menu -->
		<?php
			if (Auth::check()) {
		?>
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <ul class="nav side-menu">
					<br />
					<br />
					<br />
					<li>
					    <a href="{{ route('posts.list') }}"><i class="fa fa-file-text" aria-hidden="true"></i> Post</a>
					</li>
					<li>
					    <a href="{{ route('banner.list') }}"><i class="fa fa-cube" aria-hidden="true"></i> Banner</a>
					</li>
					<li>
						<a href="{{ route('video.list') }}"><i class="fa fa-youtube" aria-hidden="true"></i> Video</a>
					</li>
					<li>
					    <a href="{{ route('galleries.list') }}"><i class="fa fa-file-image-o" aria-hidden="true"></i> Gallery</a>
					</li>
					<?php if (Auth::user()->department_id == 1){ ;?>
					<li>
						<a href="">
						<i class="fa fa-users" aria-hidden="true"></i> User Management
						</a>
					</li>
					<?php } ?>

                </ul>
            </div>


        </div>
		<?php } ?>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
            </a>
			<a data-placement="top" title="Logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
			</a>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
            </form>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>
@endguest