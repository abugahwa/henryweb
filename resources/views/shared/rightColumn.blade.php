
          <h4>{{ strtoupper('Berita & Artikel') }}</h4>
          <?php foreach($postsRight as $p) { ?>
            <div class="py-1 postSide">
              <img class="mb-2" src="{{ url($p->post_img) }}" />
              <p><a href="{{ url($p->post_alias . '.phtml') }}">{{ $p->post_title }}</a></p>
            </div>
          <?php } ?>

          <a href="{{ route('posts.blog', 'latest-news') }}" class="pull-right ahref">Index <i class="fa fa-angle-double-right"></i></a>
          <div class="clearfix"></div>

          <h4 class="category_title">{{ strtoupper('Henry Indraguna Channel') }}</h4>
          <?php
          foreach($postsRight1 as $p) { ?>
            <div class="py-1 postSide">
              <img class="mb-2" src="https://img.youtube.com/vi/{{ $p->video_id }}/0.jpg" />
              <p><a href="{{ route('videos.view', $p->id) }}">{{ $p->title }}</a></p>
            </div>
          <?php } ?>

          <a href="{{ route('videos.index') }}" class="pull-right ahref">Index <i class="fa fa-angle-double-right"></i></a>

          <div class="clearfix"></div>
