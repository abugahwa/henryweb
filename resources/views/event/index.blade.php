@extends('layouts.event')
@section('content')
<link href="{{ URL::asset('css/event/index.css') }}" rel="stylesheet">
@if(empty($images_slide))
@else
	<img src="<?php echo route('cdn', $images_slide ); ?>" class="img-responsive center-block" width="100%">
@endif
<div class="container">

  <div class="col-sm-12">

  <br />

      @include('event.home.achievment')

    <div style="clear: both;margin: 7px;"></div>
    <!-- LIPUTAN MEDIA -->

    <div id="Lmedia">

      @include('event.home.LiputanMedia')
    </div>

    <div style="clear:both; margin: 7px;"></div>
		@if($alias == 'indonesia-digital-popular-brand-award')
    <img src="{{ asset('images/logo/para-peraih-indonesia-digital-popular-brand-award-2018.jpg') }}" class="img-responsive center-block" width="100%">
		@endif

		@if($alias == 'anugerah-brand-indonesia')
    <img src="{{ asset('images/logo/banner-anugerah-brand-indonesia.jpg') }}" class="img-responsive center-block" width="100%">
    @endif
    @if($alias == 'mobile-application-choice-award')
    <img src="{{ asset('images/logo/mobile-application-choice-award.png') }}" class="img-responsive center-block" width="100%">
    @endif
    @if($alias == 'pertama-di-indonesia')
    <img src="{{ asset('images/logo/mobile-application-choice-award.png') }}" class="img-responsive center-block" width="100%">
		@endif
    <div style="clear:both; margin: 7px;"></div>
		@if($videolist->count() > 0)
    @include('event.home.video')
		@endif

    <div style="clear:both; margin: 7px;"></div>
    <!-- GALLERIES -->

    <div id="GalleriesBrand">

      @include('event.home.galleries')
    </div>

  </div>
</div>
<br />



@endsection
