<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MPostCategory extends Model
{
    protected $table 	= 'post_categories';
    protected $fillabel = ['id','category_name','category_alias','parent_id','sort_order','category_status'];
	public $timestamps = false;
}
