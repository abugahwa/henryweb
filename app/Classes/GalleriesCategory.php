<?php
namespace App\Classes;

use Illuminate\Http\Request;
use App\Models\MImageCategory;

use App\Classes\StringClass;

use DB;
use Carbon;

class GalleriesCategory extends BaseClass {

	public function GetAll()
	{
		return MImageCategory::where('status', 1)->paginate(10);
	}
	public function pluckCatAll()
	{
		return MImageCategory::pluck('category_image', 'id');
	}
	public function CatImageAll()
	{
		return MImageCategory::all();
	}
	public function InsertCategory(Request $request)
	{
		$cats							= new MImageCategory;
		$cats->category_image			= $request->category_image;
		$cats->status					= $request->gallery_status;

		$cats->save();
		return $cats;
	}
	public function FindId($id)
	{
		return MImageCategory::find($id);
	}
	public function PutStatus($id)
	{
		return MImageCategory::where('id', $id)->first()->status;
	}
	public function UpdateCategory(Request $request, $id)
	{
		$cats							= MImageCategory::find($id);
		$cats->category_image			= $request->category_image;
		$cats->status					= $request->gallery_status;

		$cats->save();

		return $cats;
	}
	public function DeleteCatImage(Request $request, $id)
	{
		return \DB::update('update image_category set status = ? where id = ?',[0,$id]);
	}
}
