@extends('member.layouts.member')
@section('content')

<div class="main">
	{{ csrf_field() }}
	{!! Form::model($data, ['method' => 'POST','route' => ['postsupdate', $data->id], 'class' => 'form-horizontal ', 'enctype' => 'multipart/form-data'])  !!}
		<input type="hidden" name="post_img" value="<?php echo $data->post_img; ?>" />
		{!! Form::hidden('post_author', null, array('autofocus' => 'autofocus','placeholder' => 'Hits','class' => 'form-control')) !!}
		{!! Form::hidden('post_hits', null, array('autofocus' => 'autofocus','placeholder' => 'Hits','class' => 'form-control')) !!}
		@include('admin.posts.form_post')
	{!! Form::close() !!}
</div>
	<script src="{{ asset('assets/bower_components/gentelella/vendors/jquery/dist/jquery.min.js') }}"></script>
	
	<script src="{{ asset('assets/bower_components/gentelella/vendors/moment/min/moment.min.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
	    
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
  <script type="text/javascript">  

     CKEDITOR.replace( 'technig' );  

  </script>

<script type="text/javascript">
    $('#start_publishing').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    }); 
    $('#finish_publishing').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
</script> 
@endsection