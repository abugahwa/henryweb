@extends('member.layouts.member')
@section('content')
<style>
@import 'https://code.highcharts.com/css/highcharts.css';

#container {
	height: 400px;
	max-width: 100%;
	margin: 0 auto;
}

/* Link the series colors to axis colors */
.highcharts-color-0 {
	fill: #7cb5ec;
	stroke: #7cb5ec;
}
.highcharts-axis.highcharts-color-0 .highcharts-axis-line {
	stroke: #7cb5ec;
}
.highcharts-axis.highcharts-color-0 text {
	fill: #7cb5ec;
}


.highcharts-yaxis .highcharts-axis-line {
	stroke-width: 2px;
}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="main">
			<div id="container"></div>
		</div>
	</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="{{ asset('js/highcharts/highstock.js') }}"></script>
<script src="{{ asset('js/highcharts/series-label.js') }}"></script>
<script src="{{ asset('js/highcharts/exporting.js') }}"></script>
<script type="text/javascript">


Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Click Article'
    },
   
    xAxis: {
       min: 0,
            max: 5,
            type: 'category',
        plotBands: [{ // visualize the weekend
          
            color: 'rgba(68, 170, 213, .2)'
        }]
    },
     yAxis: [{
        className: 'highcharts-color-0',
        title: {
            text: 'Primary axis'
        }
    },],
	labels: {
        items: [{
            html: 'Total Click',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
    tooltip: {
        shared: true,
        valueSuffix: ' Article'
    },
    credits: {
        enabled: false
    },
  plotOptions: {
        column: {
            borderRadius: 5
        }
    },
    series: [
			{
				name: 'Click Article',
				data: [
					<?php echo $chrt ?>
				]
			}
			]
});
</script>
@endsection