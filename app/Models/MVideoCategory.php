<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MVideoCategory extends Model
{
    protected $table        = 'video_category';
    protected $fillabel     = ['id', 'name', 'status', 'parent', 'created_at', 'updated_at'];
}
