@extends('layouts.frontend')
@section('pageTitle', 'Videos InfoBrand TV')
@section('content')
<style>
#category .card{
    border: none !important;
}
#category .card-body{
  padding: 0px !important;
}
</style>
    <div class="container">
        <div class="row">
            <div class="col-md-9 order-1 order-md-2">
            <br />
                <h2>{{ $nm_cat }}</h2>
                <hr style="margin-top: -1px;" />
                <div id="category" class="row">
                    @foreach($videos as $v)
                    <div class="col-md-3">
                        <div class="card">
                            <img class="card-img-top" src="https://img.youtube.com/vi/{{ $v->video_id }}/0.jpg" alt="Card image cap">
                            <div class="card-body">
                            <a href="{{ route('videos.view', $v->id) }}" style="color: #000000;">{!! str_limit(strip_tags($v->title), $limit = 80, $end = '...') !!}</a>
                                <p class="text-secondary" style="font-size:8pt;margin-bottom: 3px;"><a target="_blank" href="https://www.youtube.com/channel/UCfydaxXyvjJeu0uqo7yVXeg" style="color:#6c757d!important;">Infobrand ID</a></p>
                                <p class="text-secondary" style="font-size:8pt;">{{ $v->hits }}x ditonton</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
					<div class="col-md-12">
						{{ $videos->appends(\Request::except('_token'))->render('pagination::bootstrap-4') }}
					</div>
            </div>
            <div class="col-md-3 order-3">

            <h2 class="category_title">{{ strtoupper('Most Viewed') }}</h2>
                <?php foreach($most as $m) { ?>
                <div class="media">
                <div class="media-left">
                    <img class="media-object" src="https://img.youtube.com/vi/{{ $m->video_id }}/0.jpg" alt="{{ $m->title }}" width="120" height="120" />
                </div>
                <div class="media-body">
                    <p style="font-size:10pt;"><a href="{{ route('videos.view', $m->id) }}">{{ $m->title }}</a></p>
                </div>
                </div>
                <hr />
                <?php } ?>


            </div>
        </div>
    </div>

@endsection