<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MEvents extends Model
{
    protected $table		= 'events';
	protected $fillable		= ['id' , 'text_event', 'menu_id', 'created_at', 'updated_at'];
	
	public function categ()
	{
	    return $this->belongsTo('App\Models\MMenuEvents','menu_id','id');
	}
}
