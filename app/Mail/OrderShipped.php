<?php

namespace App\Mail;


use App\Order;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
		$ids = $request->id;
        return $this->markdown('emails.orders.shipped')
					->attach('create/'.$ids, [
						'as' => $ids.'.pdf',
						'mime' => 'application/pdf',
					])
                        ->with('content',$this->content);
    }
/*
    public function build(Request $request)
    {
			 return $this->markdown('emails.orders.signup')
                        ->with('content',$this->content);
    }
	*/
}
