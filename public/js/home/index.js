
$(function() {
    $("#video-player a").on("click", function() {
          $(".videoarea iframe").remove();
          $('<iframe width="720" height="405" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>')
              .attr("src", 'https://www.youtube-nocookie.com/embed/'+$(this).attr('videoid')+'?rel=0&amp;controls=0')
              .appendTo(".videoarea");
    });
    $("#video-player a").first().trigger('click');


    $("#gallery-player a").on("click", function() {
          $("#gallery-player .area img").remove();
          $('<img class="img-responsive" src="" alt="">')
              .attr("src", $(this).attr('photo-src'))
              .appendTo("#gallery-player .area");
    });
    $("#gallery-player a").first().trigger('click');

});
