    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
					{{ method_field('post') }}
						{!! Form::model($data, ['method' => 'POST', 'files' => true,'class' => 'form-horizontal ', 'enctype' => 'multipart/form-data','id' => 'edit_videos'])  !!}
						{{ csrf_field() }}
							{!! Form::hidden('id', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Slide Title','class' => 'form-control','id' => 'id')) !!}
							{!! Form::hidden('hits', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Video Title','class' => 'form-control')) !!}
						<input type="hidden" name="gallery_file">
						@include('admin.video.form')
						
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success add" id="btnSave">
                            <span id="" class='glyphicon glyphicon-check'></span> Edit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
				{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
<script>
	/* edit gallery */
	
	function edit_data(id)
	{
		var catid = 0;
		$('#edit_videos')[0].reset(); 
		$('#btnSave').text('update');
		
		$.ajax({
			url : '{{ url("/video-edit")}}/' + id,
			type: "get",
			dataType: "JSON",
			success: function(data)
			{
				
				/*
				tinymce.get('post_text').setContent(data.post_text);
				*/
				$('[name="id"]').val(data.data1.id);

				$('[name="title"]').val(data.data1.title);
				$('[name="video_id"]').val(data.data1.video_id);
				$('[name="description"]').val(data.data1.description);
				$('[name="hits"]').val(data.data1.hits);
				$('[name="video_category"]').val(data.data1.video_category);
				var catid = data.data1.video_category;
				$('#editModal').modal('show'); 
				$('.modal-title').text('Edit '+data.title); 
				 $('#btnSave').text('Edit');
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error get data from ajax');
			}
		
		});	
		$.get('get-video/'+id,function(data){	
				$('#category').html(data);
			});
	}
	
$("form#edit_videos").submit(function(event){
		event.preventDefault();
		var id	= $("#id").val();
		$('#btnSave').text('Update'); //change button text
		$('#btnSave').text('update..'); //change button text
		$('#btnSave').attr('disabled',true); //set button disable

		var formData = new FormData($(this)[0]);
		  $.ajax({
			url: "{{ url('/video-update') }}/"+id,
			type: 'POST',
			data: formData,
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			dataType: "JSON",
			 success: function(data)
				{
					toastr.success('Successfully updated Video!', 'Success Alert', {timeOut: 5000});
					$('.item'+ data.id).replaceWith('<ul class="thumbnail y_video item'+ data.id +'"><a class="various fancybox fancybox.iframe" data-fancybox-type="iframe" href="https://www.youtube.com/embed/'+ data.video_id +' " rel="gallery" title="'+ data.title +'"><img src="https://img.youtube.com/vi/'+data.video_id +'/0.jpg"  align="center" alt="" class="videoy"></a><div class="caption">'+ data.title +'</div><a href="javascript:void(0)" onclick="edit_data('+data.id+')" class="btn-sm btn-danger pull-right" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a><a href="#" class="btn-sm btn-info pull-right" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="Delete Article" onclick="return confirm("Apakah Anda yakin menghapus video ini?")" style="margin-right: 4px;">Delete <i class="fa fa-trash-o" aria-hidden="true"></i></a></ul>');

					$('#editModal').modal('toggle');

					$('#btnSave').text('saving'); //change button text
					$('#btnSave').attr('disabled',false); //set button enable 


				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Error adding / update data');
					$('#btnSave').text('save'); //change button text
					$('#btnSave').attr('disabled',false); //set button enable 

				}
		  });
	 
	  return false;
	});
</script>