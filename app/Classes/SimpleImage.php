<?php

namespace App\Classes;

/*
* File: SimpleImage.php
* Author: Simon Jarvis
* Copyright: 2006 Simon Jarvis
* Date: 08/11/06
* Link: http://www.white-hat-web-design.co.uk/articles/php-image-resizing.php
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details:
* http://www.gnu.org/licenses/gpl.html
*
*/

class SimpleImage extends BaseClass {

   var $image;
   var $image_type;

   function load($filename) {

      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
      if( $this->image_type == IMAGETYPE_JPEG ) {

         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {

         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {

         $this->image = imagecreatefrompng($filename);
      }
   }
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {

      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {

         imagegif($this->image,$filename);
      } elseif( $image_type == IMAGETYPE_PNG ) {

         imagepng($this->image,$filename);
      }
      if( $permissions != null) {

         chmod($filename,$permissions);
      }
   }
   function output($image_type=IMAGETYPE_JPEG) {

      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {

         imagegif($this->image);
      } elseif( $image_type == IMAGETYPE_PNG ) {

         imagepng($this->image);
      }
   }
   function getWidth() {

      return imagesx($this->image);
   }
   function getHeight() {

      return imagesy($this->image);
   }
   function resizeToHeight($height) {

      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }

   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getHeight() * $ratio;
      $this->resize($width,$height);
   }

   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getHeight() * $scale/100;
      $this->resize($width,$height);
   }

   function resize($width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;
   }

	 public function bestResize($source_image, $destination_filename, $width = 200, $height = 150, $quality = 100, $crop = true) {

    	if( ! $image_data = getimagesize( $source_image ) )
    	{
    		return false;
    	}

    	switch( $image_data['mime'] )
    	{
    		case 'image/gif':
    			//$get_func = 'imagecreatefromgif';
				$img_original = @imagecreatefromgif($source_image);
    			$suffix = ".gif";
    		break;
    		case 'image/jpeg';
    			//$get_func = 'imagecreatefromjpeg';
				$img_original = @imagecreatefromjpeg($source_image);
    			$suffix = ".jpg";
    		break;
    		case 'image/png':
    			//$get_func = 'imagecreatefrompng';
				$img_original = @imagecreatefrompng($source_image);
    			$suffix = ".png";
    		break;
    	}

    	//$img_original = call_user_func( $get_func, $source_image );
    	$old_width = $image_data[0];
    	$old_height = $image_data[1];
    	$new_width = $width;
    	$new_height = $height;
    	$src_x = 0;
    	$src_y = 0;
    	$current_ratio = round( $old_width / $old_height, 2 );
    	$desired_ratio_after = round( $width / $height, 2 );
    	$desired_ratio_before = round( $height / $width, 2 );

    	if( $old_width < $width || $old_height < $height )
    	{
    		/**
    		 * The desired image size is bigger than the original image.
    		 * Best not to do anything at all really.
    		 */
    		//return false;
    	}


    	/**
    	 * If the crop option is left on, it will take an image and best fit it
    	 * so it will always come out the exact specified size.
    	 */
    	if( $crop )
    	{
    		/**
    		 * create empty image of the specified size
    		 */
    		$new_image = imagecreatetruecolor( $width, $height );

    		/**
    		 * Landscape Image
    		 */
    		if( $current_ratio > $desired_ratio_after )
    		{
    			$new_width = $old_width * $height / $old_height;
    		}

    		/**
    		 * Nearly square ratio image.
    		 */
    		if( $current_ratio > $desired_ratio_before && $current_ratio < $desired_ratio_after )
    		{
    			if( $old_width > $old_height )
    			{
    				$new_height = max( $width, $height );
    				$new_width = $old_width * $new_height / $old_height;
    			}
    			else
    			{
    				$new_height = $old_height * $width / $old_width;
    			}
    		}

    		/**
    		 * Portrait sized image
    		 */
    		if( $current_ratio < $desired_ratio_before  )
    		{
    			$new_height = $old_height * $width / $old_width;
    		}

    		/**
    		 * Find out the ratio of the original photo to it's new, thumbnail-based size
    		 * for both the width and the height. It's used to find out where to crop.
    		 */
    		$width_ratio = $old_width / $new_width;
    		$height_ratio = $old_height / $new_height;

    		/**
    		 * Calculate where to crop based on the center of the image
    		 */
    		$src_x = floor( ( ( $new_width - $width ) / 2 ) * $width_ratio );
    		$src_y = round( ( ( $new_height - $height ) / 2 ) * $height_ratio );
    	}
    	/**
    	 * Don't crop the image, just resize it proportionally
    	 */
    	else
    	{
    		if( $old_width > $old_height )
    		{
    			$ratio = max( $old_width, $old_height ) / max( $width, $height );
    		}else{
    			$ratio = max( $old_width, $old_height ) / min( $width, $height );
    		}

    		$new_width = $old_width / $ratio;
    		$new_height = $old_height / $ratio;

    		$new_image = imagecreatetruecolor( $new_width, $new_height );
    	}

    	/**
    	 * Where all the real magic happens
    	 */
    	imagecopyresampled( $new_image, $img_original, 0, 0, $src_x, $src_y, $new_width, $new_height, $old_width, $old_height );

    	/**
    	 * Save it as a JPG File with our $destination_filename param.
    	 */
		imagejpeg( $new_image, NULL, $quality );
		//imagejpeg( $new_image, $destination_filename, $quality  );

    	/**
    	 * Destroy the evidence!
    	 */
    	imagedestroy( $new_image );
    	imagedestroy( $img_original );

    	/**
    	 * Return true because it worked and we're happy. Let the dancing commence!
    	 */
		//return true;
	}

}
?>
