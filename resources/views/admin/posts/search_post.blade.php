<style>
  .load-bar {
  position: relative;
  margin-top: 20px;
  width: 100%;
  height: 6px;
  background-color: #fdba2c;
}
.bar {
  content: "";
  display: inline;
  position: absolute;
  width: 0;
  height: 100%;
  left: 50%;
  text-align: center;
}
.bar:nth-child(1) {
  background-color: #da4733;
  animation: loading 3s linear infinite;
}
.bar:nth-child(2) {
  background-color: #3b78e7;
  animation: loading 3s linear 1s infinite;
}
.bar:nth-child(3) {
  background-color: #fdba2c;
  animation: loading 3s linear 2s infinite;
}
@keyframes loading {
    from {left: 50%; width: 0;z-index:100;}
    33.3333% {left: 0; width: 100%;z-index: 10;}
    to {left: 0; width: 100%;}
}
.btn-edit{
	float: left;
    display: inline-flex;
}
.btn-edit .btn-danger{
	height: 33px;
    padding: 11px;
    margin: 0 3px;
}
</style>
<div class="load-bar">
  <div class="bar"></div>
  <div class="bar"></div>
  <div class="bar"></div>
</div>


    						<table class="table table-striped">
    							<thead bgcolor="#9853bb" style="color: #fff;">
    								<tr>
    									<th width="400px">Post Title</th>
    									<th>Post Category</th>
    									<th>Publish</th>
    									<th>Action</th>
    								</tr>
    							</thead>
    							<tbody>
    								@foreach($post as $b)
    								<tr>
    									<td>{{ $b->post_title }}</td>
    									<td>{{ $b->PostCategories->category_name }}</td>
    									<td>{{ Carbon\Carbon::parse($b->created_at)->format('D M Y i H:i:s') }}</td>
    								
    									<td>
											<div class="btn-edit">
										{!! Form::open(['method' => 'POST','route' => ['posts.edit'],'files' => true, 'class' => 'form-horizontal'])  !!}
										<input type="hidden" name="id" value="{{ $b->id }}">
											<button type="submit" class="btn-sm btn-info "><i class="fa fa-pencil-square-o"></i></button>
										{!! Form::close() !!} 
    									<a href="{{ route('posts.delete', $b->id) }}" class="btn-sm btn-danger" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="Delete Article" onclick="return confirm('Apakah Anda yakin menghapus post ini?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
										</div>
										</td>
    								</tr>
    								@endforeach
    							</tbody>
    						</table>
						@if($post->count() === 0)
							<div class="alert alert-danger">
								No data found.
							</div>
						@endif
					{!! $post->render() !!}