<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;


use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Social\FacebookServiceProvider;
use GuzzleHttp\Exception\ClientException;
use League\OAuth1\Client\Credentials\CredentialsException;

use App\Models\MUserProfile;

use Socialite;
use Auth;
use Exception;
class SocialiteController extends Controller
{
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }
	public function handleProviderCallback()
    {
		try {
            $user = Socialite::driver('google')->user();
            
			$authUser = $this->findOrCreateUser($user,'google');
			Auth::login($authUser, true);
			
			$id 		= $user->getId();
			$name 		= $user->getName();
			$email 		= $user->getEmail();
			$nickname	= $user->getNickname();
			
            return redirect()->route('homemember')
							->with('id', $id)
							->with('name', $name)
							->with('nickname', $nickname)
							->with('email', $email);
			;
        } catch (Exception $e) {
            return redirect('/login-member');
        }
    }
	private function findOrCreateUser($provideruser,$provider)
    {
        $authUser = User::where('email', $provideruser->email)->first();
 
        if ($authUser){
            return $authUser;
        }
        return User::create([
            'name' => $provideruser->name,
            'email' => $provideruser->getEmail(),
            'department_id' => 3,
            'status' => 'activated',
            'social_id' => $provideruser->getId(),
			'social_provider' => $provider,
            'avatar' => $provideruser->getAvatar()
        ]);
    }
	 protected function sendFailedResponse($msg = null)
    {
        return redirect()->route('loginmember')
            ->withErrors(['msg' => $msg ?: 'Unable to login, try with another provider to login.']);
    }

}
