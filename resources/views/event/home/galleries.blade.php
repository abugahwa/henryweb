@if($galleries->count() > 0)
<div class="row">
  <div class="col-md-6">
      <h4><strong>GALLERY</strong></h4>
      <div class="list3"></div>
      <br />
  </div>

  <div class="col-md-6">
      <div class="form-group pull-right" id="pagination_galleries">
        <a href="#GalleriesHome" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
        <a href="#GalleriesHome" data-slide="next"><i class="fas fa-chevron-right"></i></a>
    </div>
  </div>
</div>

<div style="clear:both;"></div>

<div class="row">
    <div id="GalleriesHome" class="carousel slide" data-ride="carousel" data-interval="9000">
        <div class="carousel-inner row w-100 mx-auto" role="listbox">
          <?php $n = 0;?>
          @foreach($galleries as $img_detail)
          <div class="carousel-item col-md-4  <?php if($n == 0) echo "active"; $n++; ?>">
              <div class="card">
                  <a class="gallery_card" data-exif="Exif #1" data-fancybox="images" href="<?php echo route('cdn', $img_detail['gallery_file'] ); ?>" data-caption="{{ $img_detail->gallery_caption }}">
                      <img class="card-img-top" src="<?php echo route('cdn', $img_detail['gallery_file'] ); ?>" alt="{{ $img_detail->gallery_caption}}">
                  </a>
                  <div class="card-body">
                      <h4 class="card-title">{{ $img_detail->gallery_title }}</h4>
                      <p>{!! str_limit(strip_tags($img_detail->gallery_caption), $limit = 80, $end = '...') !!}</p>
                  </div>
              </div>
          </div>
          @endforeach
        </div>

    </div>
    <div class="col-md-12">
      <br />
      <div class="form-group pull-right">
        <a href="{{ route('gallery_detail', $alias) }}" class="float-right"><u>Index</u></a>
      </div>
    </div>
</div>
@endif
