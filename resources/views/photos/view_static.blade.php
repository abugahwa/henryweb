@extends('henryweb.layouts.frontend')
@section('pageTitle', $title)
@section('content')

    <div class="bg-navbar"></div>

<style>
.carousel-custom {
  list-style: none;
  padding: 0px;
  margin: 0px;
  width: 100%;
  max-width: 100%;
  overflow-x: scroll;
  overflow-y: hidden;
  height: 120px;
  white-space: nowrap;
}
.carousel-custom li {
  display: inline-block;
  cursor: pointer;
}
.carousel-custom li img {
  width:120px;
  height:80px;
}
</style>

    <div class="container">
      <div class="" style="margin:10px 0px;"></div>
    </div>

    <div class="container">
      <div class="row">
      <div class="col-md-8">
        <div id="photos" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner" role="listbox">
            <?php
            $n = 0;
            foreach($slides as $slide) { ?>
            <div class="carousel-item <?php if($n == 0) echo "active"; $n++; ?>">
              <img class="first-slide" src="<?php echo $slide['gallery_file']; ?>" alt="<?php echo $slide['gallery_title']; ?>">
              <div class="">
                <div class="carousel-caption d-md-block text-left">
                  <h2><?php echo $slide['gallery_caption']; ?></h2>
                </div>
              </div>
            </div>
            <?php } ?>
          </div>
          <a class="carousel-control-prev" href="#photos" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#photos" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

        <div class="">
                    <!-- Indicators -->
                    <ol class='carousel-custom'>
                      <?php
                      $n = 0;
                      foreach($slides as $slide) { ?>
                        <li data-target='#photos' data-slide-to='<?php echo $n; ?>' class='<?php if($n == 0) echo "active"; $n++; ?>'>
                          <img src="<?php echo $slide['gallery_file']; ?>" alt='<?php echo $slide['gallery_caption']; ?>' />
                          <p><?php echo $slide['gallery_title']; ?></p>
                        </li>
                        <?php } ?>
                    </ol>
                    <!-- / Indicators -->
        </div>
      </div>


      <div class="col-md-4">
        <h2 class="category_title">{{ strtoupper('Gallery Terbaru') }}</h2>
        <?php foreach($another as $post) { ?>
        <div class="media">
          <div class="media-left">
              <img class="media-object" src="<?php echo route('cdn', $post['gallery_file']); ?>" alt="<?php echo $post['gallery_title']; ?>" width="160" height="120" />
          </div>
          <div class="media-body">
              <p style="font-size:14pt;"><a href="<?php echo route('photos.view', $post->Galleries->id); ?>"><?php echo $post->Galleries->category_image; ?></a></p>
          </div>
        </div>
        <hr />
        <?php } ?>

      </div>
    </div>
    </div>

@endsection
