<div class="form-group">
	<label for="event_name">Event Name</label>
	{!! Form::text('event_name', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Banner Name','class' => 'form-control')) !!}
</div>
<div class="form-group">
    <label for="event_date">Date</label>
	{!! Form::text('event_date', $datenow, ['id' => 'start_publishing','class' => 'form-control', 'placeholder' => 'Please Select']) !!}
</div>
<div class="form-group">
    <label for="fase">Fase</label>
	{{ Form::selectRange('fase', 1,10,$fase,['class' => 'form-control']) }}
</div>
<div class="form-group">
	<label>Description</label>
	{{ Form::textarea('survey_results', null, ['class' => 'field','id' => 'technig']) }}
</div>
<div class="form-group">
	<button type="submit" class="btn btn-success cbutton">Add</button>
</div>