@extends('layouts.event')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
          <br />
          <h1 class="text-center">{{ ucwords(str_replace('-', ' ', $title_galleri)) }}</h1>
          <hr />
            <div class="">
                @foreach($galleries as $img_detail)
                    <div class="card galleri_detail">
                        <a class="gallery_card_detail" data-exif="Exif #1" data-fancybox="images" href="<?php echo route('cdn', $img_detail['gallery_file'] ); ?>" data-caption="{{ $img_detail->gallery_caption }}">
                            <img class="card-img-top" src="<?php echo route('cdn', $img_detail['gallery_file'] ); ?>" alt="{{ $img_detail->gallery_caption}}">
                        </a>
                        <div class="card-body">
                            <h5 class="card-title">{{ $img_detail->gallery_title }}</h5>
                            <p class="card-text">{!! str_limit(strip_tags($img_detail->gallery_caption), $limit = 80, $end = '...') !!}</p>
                        </div>
                    </div>
                @endforeach
            </div>
            <div style="clear:both;"></div>
            <div class="">
            {!! $galleries->render('pagination::bootstrap-4') !!}
          </div>
        </div>
    </div>
</div>
@endsection
