<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Auth;
use App\Models\MPost;
use App\Models\MPostCategory;

use App\Classes\StringClass;
use App\Classes\Posts;
use App\Classes\PostCategory;

use DB;
use Carbon;

class PostsController extends Controller
{
	protected $posts;
	protected $category;

	public function __construct()
    {
        $this->posts = new Posts;
        $this->category = new PostCategory;

    }
    public function index(Request $request)
    {
        $post       	= MPost::orderBy('id', 'desc')->paginate(10);
		$strings    	= new StringClass();
		$post_category	= $this->category->PluckCat();
		if($request->ajax() ){
			$strings    = new StringClass();

		$query 		= $request->title;
		$category	= $request->post_category;
	
			$post		= MPost::where('post_title', 'LIKE', '%' . $query . '%')
						->where(function($query) use ($category){
							$query->where('post_category', 'LIKE', '%' . $category . '%');
						})->paginate(10);
			return view('admin.posts.table_post', compact('post','strings'))->render();
		
		}
		return view('admin.posts.list_post', compact('post','strings', 'post_category'));
	}

	public function create()
    {

			$datenow 		= Carbon\Carbon::now();
			$datenow->setTimezone('Asia/Jakarta');
			//$catpost    	= $this->posts->pluckCategory();
			$catpost		= $this->category->GetCatByParentNull(0);
			$catid      	= 0; 
			$cat			= 0;
			$child			= $this->category;

			return view('admin.posts.add_post',compact('catpost', 'catid','datenow', 'cat', 'child'));

    }

		public function store(Request $request)
    {
        $post			= $this->posts->InsertPost($request);
        $request->session()->flash('alert-success', 'was successful Add!');
        return redirect()->route('posts.list')->with('success','Article created successfully');
    }

		public function edit(Request $request)
    {
		$id			= $request->id_post;
        $data       = $this->posts->findPosts($id);
        $catid      = $this->posts->getCategorypost($id);
        $catpost    = $this->category->GetCatByParentNull(0);
		$datenow	= $this->posts->startPublishByid($id);
		$cat		= $catid;
		$child		= $this->category;
		
        return view('admin.posts.edit_post',compact('data','id','catpost','catid','datenow', 'cat', 'child'));
    }

		public function update(Request $request, $id)
    {
        $post				= $this->posts->UpdatePost($request,$id);
        $request->session()->flash('alert-success', 'was successful Update!');
        return redirect()->route('posts.list')->with('success','Article created successfully');
    }

		public function destroy(Request $request,$id)
    {
        $post				= $this->posts->DeletePosts($request, $id);
        $request->session()->flash('alert-success', 'was successful delete!');
        return redirect()->route('posts.list')->with('success','Article created successfully');
    }

	public function CategoryView()
	{
		$cats							= $this->category->GetAll();
		$category_status 				= 0;

		return view('admin.posts.categories', compact('cats','category_status'));
	}

	public function InsertCategories(Request $request)
	{
		$cats							= $this->category->InsertCategory($request);

		return response()->json($cats);
	}

	public function EditCategory($id)
	{
		$cats							= $this->category->FindById($id);

		return response()->json($cats);
	}

	public function UpdateCategories(Request $request, $id)
	{
		$cats							= $this->category->UpdateCategory($request, $id);

		return response()->json($cats);
	}
		
	public function DeleteCategories(Request $request, $id)
	{
		$cats							= $this->category->DeleteCategory($request, $id);
		return response()->json($cats);
	}
	
}
