<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <img width="500" class="img-fluid" src="{{ asset('images/hip-law-office.jpg') }}" />
    </div>
    <div class="col-md-12">
      <div id="hiplawoffice" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="{{ asset('images/hip-lawyer-slide1.png') }}" alt="HIP Lawyers">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('images/hip-lawyer-slide2.png') }}" alt="HIP Lawyers">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="{{ asset('images/hip-lawyer-slide3.png') }}" alt="HIP Lawyers">
          </div>
          <a class="carousel-control-prev" href="#hiplawoffice" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#hiplawoffice" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>

      <div class="text-center py-4"><a href="#konsultasi"><button class="btn btn-lg btn-secondary">Set Up Meeting</button></a></div>
    </div>
  </div>
</div>
