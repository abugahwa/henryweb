<ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        @foreach($menu as $b)
        <li class="nav-item">
            <a class="nav-link" href="#">{{ $b->menu_name }}</a>
        </li>
        @endforeach 
    </ul>