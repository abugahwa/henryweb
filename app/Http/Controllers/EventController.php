<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\MImageCategory;
use App\Models\MGallery;
use App\Models\MVideos;
use App\Models\MPost;
use App\Models\MPostCategory;
use App\Models\EventSurver;
use App\Models\MCatScore;

use App\Classes\Event;
use App\Classes\Videos;
use App\Classes\Posts;
use App\Classes\PostCategory;
use App\Classes\Galleries;

use Carbon\Carbon;
use MetaTag;

class EventController extends Controller
{
	public function __construct()
    {
		$this->events		= new Event;
		$this->videos 	= new Videos;
		$this->posts		= new Posts;
		$this->cats			= new PostCategory;
		$this->galleri	= new Galleries;
	}
  public function event(Request $request,$alias)
	{
		$galleri_Name				= str_replace('.html', '', $alias);
		$galleri_Name				= str_replace('-', ' ', $alias);

		$idmenu							= $this->events->GetIdByAlias($alias);
		$menu								= $this->events->GetByID($idmenu);
		$images_icon				= $this->events->ImageBYid($idmenu);
		$txt_events					= $this->events;
		$images_slide				= $this->events->SlideByID($idmenu);

		$category_id				= $this->cats->GetNameByAlias($alias); /* ambil idnya */
		$perent							= $this->cats->GetParentByID($category_id); /* ambil parent category by id */

		$achievment					= $this->posts->GetPostByCat($category_id, 'Brand Achievement', 20, 0);
		$liputanMedia				= $this->posts->GetPostByCat($category_id, 'Liputan Media', 20, 0);

		$videolist 					= $this->videos->getAll(['limit' => 4, 'sortby' => 'rand', 'query' => $galleri_Name]);
		$galleries					= $this->galleri->IDbyALias($galleri_Name, 20, 0);

		if(!empty($idmenu)){
		$tags								= $this->events->FindByiD($idmenu);

			MetaTag::set('description', $tags->description);
		}
		MetaTag::set('title', ucwords($galleri_Name) . ' - Infobrand');
    MetaTag::set('image', asset($this->events->IconByAlias($alias)));
		MetaTag::set('keyword', 'indonesia digital popular brand award,awarding,brand indonesia');

		if($idmenu > 0){
			if($request->ajax()){
				if($request->get('brand-achievment') ){
					return view('event.home.achievment', compact('achievment', 'alias'))->render();
				}
				if($request->get('liputan-media') ){
					return view('event.home.LiputanMedia', compact('liputanMedia', 'alias'))->render();
				}
				if($request->get('galleri')){
					return view('event.home.galleries', compact('galleries', 'alias'))->render();
				}
			}
				return view('event.index', compact('menu', 'images_icon', 'txt_events', 'idmenu', 'images', 'videolist', 'images_slide', 'achievment', 'liputanMedia', 'videolist', 'galleries', 'alias'));
		}else{
			return abort(404);
		}
	}
	public function Detail(Request $request,$alias,$id,$name_event)
	{
		$idmenu						= $this->events->GetByAliasNSubAlias($id);
		$menu							= $this->events->GetByID($idmenu);
		$txt_events				= $this->events;

		$images_icon			= $this->events->ImageBYid($idmenu);
		$images_slide			= $this->events->SlideByID($idmenu);
		$datenow 					= \Carbon\Carbon::now();
		$datenow->setTimezone('Asia/Jakarta');
		$category					= EventSurver::pluck('event_name', 'id');
		$survey						= 0;
		$years						= \Carbon\Carbon::parse($datenow)->format('Y'); 

	
		MetaTag::set('title', str_replace('-', ' ', ucwords($name_event)) .' '. $txt_events->GetName($idmenu).' - Infobrand');
    	MetaTag::set('image', asset($this->events->IconByAlias($alias)));
		MetaTag::set('keyword', 'indonesia digital popular brand award,awarding,brand indonesia');
		return view('event.detailEvent', compact('survey', 'event_name','years','menu', 'images_icon', 'txt_events', 'idmenu','images_slide', 'id', 'alias', 'name_event', 'datenow', 'category', 'survey'));
	}
	public function DetailSurvey(Request $request)
	{
		//if($request->event_name){

			$category			= $request->post_category;
			$fase				= $request->fase;
			$years				= $request->year;
			$name_event			= str_replace('-', ' ', $request->alias);

			$survey				= EventSurver::where(function($queries) use ($name_event){
											$queries->where('event_name', 'LIKE', '%' . $name_event . '%');
										})
										->where(function($queries) use ($years){
											$queries->where('event_date', 'LIKE', '%' . $years . '%');
										})
										->where(function($queries) use ($fase){
											$queries->where('fase', 'LIKE', '%' . $fase . '%');
										})->get();
			$idmenu				= $this->events->GetIdByAlias($request->alias);
			$d_survey			= MCatScore::where('menu_id', $idmenu)
								->where('parent_id', 0)
								
								->orderBy('id', 'desc')
								->groupBy('cat_name')->get();
			$scores				= $this->events;
								
		
		return view('event.home.survey_result')->with('survey', json_decode($d_survey, true))->with('event_name', $name_event)->with('fase', $fase)->with('years', $years)->with('scores', $scores);

	}
	public function indexCategories(Request $request,$category)
	{
		$start						= Carbon::now();
		$enddate					= $start->subDays($start->dayOfWeek)->subWeek();
		$cat1 						= collect(request()->segments())->last();
		$cats							= str_replace('.html', '', $cat1);
		$cat2							= str_replace('-', ' ', $cats);

		$idmenu						= $this->events->GetIdByAlias($category);
		$images_icon			= $this->events->ImageBYid($idmenu);
		$menu							= $this->events->GetByID($idmenu);

		$idcategory				= MPostCategory::where('category_alias', 'LIKE', '%'. $category .'%')->first()->id;
		$title_cat				= MPostCategory::where('category_alias', $cats)->first()->category_name;

		$category_id				= $this->cats->GetNameByAlias($category); /* ambil idnya */

		$categories				= $this->posts->GetPostByCat($category_id,$cat2, 0, 8);
		$alias						= $category;
		$cat_id						= $this->cats->GetIDByParent($idcategory);



		$postPopuler			= $this->events;
		$categories_name	= $this->cats;

		MetaTag::set('title', $title_cat . ' - Infobrand');
    MetaTag::set('image', asset($this->events->IconByAlias($alias)));
		MetaTag::set('description', $title_cat);
		MetaTag::set('keyword', $title_cat);

		if($request->ajax()){
			return view('event.includes.card_categories', compact('categories', 'alias', 'images_icon', 'menu', 'postPopuler', 'title_cat', 'categories_name', 'cat_id'))->render();
		}

		return view('event.categories', compact('categories', 'alias', 'images_icon', 'menu', 'postPopuler', 'title_cat', 'categories_name', 'cat_id'));
	}
	public function Post(Request $request, $post_alias)
	{
		$post_alias 			= collect(request()->segments())->last();
		$post_alias				= str_replace('.html', ' ', $post_alias);
		$alias						= $request->alias;
		$cat_alias				= str_replace('-', ' ', ucwords($alias));
		$idmenu						= $this->events->GetIdByAlias($alias);
		$images_icon			= $this->events->ImageBYid($idmenu);
		$menu							= $this->events->GetByID($idmenu);
		$post							= MPost::where('post_alias', $post_alias)->get();

		$postcat					= $this->posts->GetCategoryByAlias($post_alias);
		$getPostId				= $this->posts->GetiDByAlias($post_alias);

		$idcategory				= $this->cats->GetNameByAlias($alias);
		$categories_name1	= $request->category;
		$categories_name	= str_replace('-', ' ', ucwords($categories_name1));
		$tags							= $this->posts->findPosts($getPostId);

		$addHits    			= $this->posts->addHits($getPostId);
		$categories				= $this->posts->GetPostBayCategory($idcategory, $postcat, $getPostId,4);

		$cat_id						= $this->cats->GetIDByParent($idcategory);

		$artikel_terkait	= $this->posts->GetPostBayCategory($idcategory, $postcat, $getPostId, 3);

		$category_link		= $request->category;

		MetaTag::set('title', $tags->post_title .' - Infobrand');
		MetaTag::set('image', asset($tags->post_img));
		MetaTag::set('description', str_limit(strip_tags($tags->post_text), $limit = 200, $end = '...'));
		MetaTag::set('keyword', $tags->post_tags);

		$postPopuler			= $this->events;
		if($request->ajax()){
			return view('event.includes.artikel_terkait', compact('artikel_terkait', 'category_link', 'post_alias', 'alias', 'categories_name', 'categories_name1', 'tags'));
		}
		return view('event.post', compact('post', 'images_icon', 'alias', 'menu', 'categories', 'cat_alias', 'postPopuler', 'cat_id', 'artikel_terkait', 'category_link', 'post_alias', 'categories_name', 'categories_name1', 'tags'));
	}
	public function searching(Request $request, $alias)
	{
		$idmenu						= $this->events->GetIdByAlias($alias);
		$images_icon			= $this->events->ImageBYid($idmenu);
		$menu							= $this->events->GetByID($idmenu);

		$searching				= $request->search;
		$postPopuler			= $this->events;
		$idcategory				= $this->cats->GetNameByAlias($alias);
		$idcategories			= $this->cats->GetNameByAlias($alias);
		$categories				= $this->posts->postByTitleTextCatName($idcategories, $searching);
		$title_cat				= $searching;
		$cat_id						= $this->cats->GetIDByParent($idcategory);
		$categories_name1	= $request->category;
		$categories_name	= $this->cats;

		MetaTag::set('title', ucwords($searching) .' - Infobrand');
		MetaTag::set('image', asset($images_icon));
		MetaTag::set('description', ucwords($searching));
		MetaTag::set('keyword', ucwords($searching));

		return view('event.search', compact('categories', 'postPopuler', 'alias', 'images_icon', 'menu', 'title_cat', 'cat_id', 'categories_name'));
	}
	public function IndexGallery(Request $request,$alias)
	{
			$galleri_Name				= str_replace('.html', '', $alias);
			$galleri_Name				= str_replace('-', ' ', $alias);
			$title_galleri			=	str_replace('', ' ', $alias);

			$idmenu							= $this->events->GetIdByAlias($alias);
			$menu								= $this->events->GetByID($idmenu);
			$images_icon				= $this->events->ImageBYid($idmenu);

			$galleries					= $this->galleri->IDbyALias($galleri_Name, 0, 12);

			MetaTag::set('title', ucwords($galleri_Name) . ' - Infobrand');
			MetaTag::set('image', asset($images_icon));
			MetaTag::set('description', $alias);
			MetaTag::set('keyword', 'indonesia digital popular brand award,awarding brand');

			return view('event.galleries', compact('galleries', 'images_icon', 'alias', 'menu', 'title_galleri'));
	}
	public function IndexVideo(Request $request, $alias)
	{
			$start							= Carbon::now();
			$enddate						= $start->subDays($start->dayOfWeek)->subWeek();

			$video_Name					= str_replace('.html', '', $alias);
			$video_Name					= str_replace('-', ' ', $alias);
			$title_video				=	str_replace('-', ' ', $alias);

			$idmenu							= $this->events->GetIdByAlias($alias);
			$menu								= $this->events->GetByID($idmenu);
			$images_icon				= $this->events->ImageBYid($idmenu);

			$video 							= $this->videos->VideoByTitle($title_video, 8);

			MetaTag::set('title', ucwords($title_video) . '- Infobrand');
			MetaTag::set('image', asset($images_icon));
			MetaTag::set('description', 'Indonesia Digital Popular Brand Award (IDPBA)  diberikan kepada para pelaku brand berdasarkan hasil survei Indonesia Digital Popular Brand Index');
			MetaTag::set('keyword', 'indonesia digital popular brand award,awarding brand');

			$postPopuler				= MPost::where('post_category', [2,10])->whereBetween('post_date_added', [$start,$enddate])
													->orderBy('post_hits', 'desc')->limit(10)->get();
			if($request->ajax()){
					return view('event.includes.card_video', compact('title_video', 'video','images_icon', 'alias', 'menu', 'postPopuler', 'title_video'))->render();
			}
			return view('event.videos', compact('title_video', 'video','images_icon', 'alias', 'menu', 'postPopuler', 'title_video'));
	}
	public function DetailVideo($alias,$id)
	{
			$video_Name					= str_replace('.html', '', $alias);
			$video_Name					= str_replace('-', ' ', $alias);
			$title_video				=	str_replace('-', ' ', $alias);

			$idmenu							= $this->events->GetIdByAlias($alias);
			$menu								= $this->events->GetByID($idmenu);
			$images_icon				= $this->events->ImageBYid($idmenu);

			$video							= $this->videos->getById($id);
			$addHits  					= $this->videos->addHits($video->id);
			$videos   					= $this->videos->getAll(['limit' => 8]);
			$most     					= $this->videos->getAll(['sortby' => 'hits', 'limit' => 8]);

			MetaTag::set('title', ucwords(str_replace('-', '',$video->title)) . ' - Infobrand');
			MetaTag::set('image', 'https://www.youtube-nocookie.com/embed/'.$video->video_id .'/0.jpg');
			MetaTag::set('description', $video->description);
			MetaTag::set('keyword', 'indonesia digital popular brand award,awarding brand');

			return view('event.detail_video', compact('videos', 'video', 'most', 'images_icon', 'alias', 'menu'));
	}

}
