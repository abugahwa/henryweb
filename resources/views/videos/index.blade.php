@extends('henryweb.layouts.frontend')
@section('pageTitle', 'Videos Henry Indraguna')
@section('content')

    <div class="bg-navbar"></div>

    <div class="home-videos pb-4">
    <div class="container">
      <div class="row py-4">
        <div class="col-md-12 text-center">
          <h1>Video Henry Indraguna</h1>
          <h5>update video terbaru, tips, dan knowledge</h5>
        </div>
      </div>
      <div class="row">
    @foreach ($videos as $p)
        <div class="col-md-4 py-3">
          <img class="img-fluid mb-2" src="https://img.youtube.com/vi/{{ $p->video_id }}/0.jpg" />
          <h5><a style="color:#000000;" href="{{ route('videos.view', $p->id) }}">{{ $p->title }}</a></h5>
        </div>
    @endforeach
      </div>
      <nav>{{ $videos->links("pagination::bootstrap-4") }}</nav>
    </div>
    </div>
@endsection
