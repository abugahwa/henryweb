<ul id="tree1">
	@foreach($event as $category)
	<li>
	<i class="fa fa-plus-square"></i> {{ $category->menu_name }}
		@if($nm_menu->GetByID($category->id)->count() > 0)
		<ul>
			@foreach($nm_menu->GetByID($category->id) as $child)
				<li>
					<?php
						//foreach($nm_menu->GetIdEvenByParent($child->id) as $b){
					?>
					{{ $child->menu_name }} <a href="{{ route('eventedit', $child->id) }}" style="margin-left: 10px;"><i class="fa fa-pencil"></i></a>
						<?php //} ?>
				</li>
			@endforeach
		</ul>
		@endif
	</li>
	@endforeach
</ul>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script>

	function eventnya(id) {
		$('#btn-cancel').html("Close");
		$('#btn-save').hide();
		$('.modal-body').load("{{ url('/get-text-event') }}/"+ id);
		$("#myModal").modal('show');
	}
</script>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-body" id="ModalContent">
			{!! $text !!}
			</div>
			<div class="modal-footer" id="ModalFooter"></div>
		</div>
	</div>
</div>
