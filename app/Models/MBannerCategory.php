<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MBannerCategory extends Model
{
    protected $table    = 'banner_categoris';
    protected $fillabel = ['id','category_name','parent_id','sort_order','category_status','created_at','updated_at'];
}
