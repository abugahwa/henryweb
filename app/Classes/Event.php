<?php

namespace App\Classes;

use Illuminate\Http\Request;
use App\Models\MBanners;

use App\Models\MMenuEvents;
use App\Models\MEvents;
use App\Models\MPost;
use App\Models\MPostCategory;
use App\Models\MScore;
use App\Models\MCatScore;

use App\Classes\StringClass;

class Event{

	public function GetAllCatEvent()
	{
		return MMenuEvents::orderBy('menu_name', 'DESC')->get();
	}
	public function FindByiD($id)
	{
		$event		= MMenuEvents::find($id);
		if(!empty($event)){
			return $event;
		}
	}
	public function GetByID($id)
	{
		return MMenuEvents::where('parent', $id)->where('parent', 'NOT LIKE', '%0%')->orderBy('sort_order', 'asc')->get();
	}
	public function GetIdEvenByParent($id)
	{
		return MEvents::where('menu_id', $id)->get();
	}
	public function AllEventPaginate()
	{
		return MMenuEvents::paginate(10);
	}
	public function PluckEventParent($parent)
	{
		return MMenuEvents::where('parent', $parent)->orderBy('menu_name', 'DESC')->pluck('menu_name', 'id');
	}
	public function byParentMenu($parent)
	{
		return MMenuEvents::where('parent', $parent)->get();
	}
	public function findEventByID($id)
	{
		return MMenuEvents::find($id);
	}
	public function SearchMenuEvent($cat)
	{
		return MMenuEvents::where('parent','LIKE', '%'. $cat . '%')->paginate(10);
	}
	public function GetName($parent)
	{
		$event					= MMenuEvents::where('id', $parent)->where('parent', 0)->first();
		if(!empty($event)){
			return $event->menu_name;
		}
	}
	public function GetParents($parent)
	{
		$event					= MMenuEvents::where('id', $parent)->first();
		if(!empty($event)){
			return $event->parent;
		}
	}
	public function GetTexts($id)
	{
		$event					= MEvents::where('id', $id)->first();
		if(!empty($event)){
			return $event->text_event;
		}
	}
	public function GetTextsByMenuID($id)
	{
		$event					= MEvents::where('menu_id', $id)->get();
		if(!empty($event)){
			return $event;
		}
	}
	public function GetIdByAlias($alias)
	{
		$b	= MMenuEvents::where('menu_alias', $alias)->first();
		if(!empty($b)){
			return $b->id;
		}
	}
	public function GetByAliasNSubAlias($id)
	{
		$b	= MMenuEvents::where('id', $id)->first();
		if(!empty($b)){
			return $b->parent;
		}
	}
	public function ImageBYid($id)
	{
		$images		= MMenuEvents::where('id', $id)->first();
		if(!empty($images)){
			return $images->logo_menu;
		}
	}
	public function IconByAlias($alias)
	{
		$icon			= MMenuEvents::where('menu_alias', $alias)->first();
		if(!empty($icon)){
			return $icon->logo_menu;
		}
	}
	public function SlideByID($id)
	{
		$images		= MMenuEvents::where('id', $id)->first();
		if(!empty($images)){
			return $images->slide;
		}
	}
	public function InsertMenuEvent(Request $request)
	{
		$strings                = new StringClass();
        $name		            = $strings->str2alias($request->menu_name);

		$now = \Carbon\Carbon::now();
		$year = date('Y', strtotime($now));
		$month = date('m', strtotime($now));
        $days = date('d', strtotime($now));
		if($request->hasFile('logo_menu') ){
			$bs                     = $request->file('logo_menu')->getClientOriginalExtension();
			$nombreCarpeta 			= preg_replace('/\s+/', '.', $year . "/" . $month . "/" . $days);
			$fileimg                = $name . '.' .$bs;
			$imgs		            = 'img/event/'.$nombreCarpeta .'/' .$fileimg;
			$path 					= base_path() .'/public/event/'.$nombreCarpeta;
		}else{
			$imgs					= NULL;
		}

		$event					= new MMenuEvents;

		$event->menu_name		= $request->menu_name;
		$event->menu_alias		= $name;
		$event->parent			= $request->parent;
		$event->status			= 1;
		$event->logo_menu		= $imgs;
		$event->description		= $request->description;
		$event->sort_order		= $request->sort_order;

		$event->save();
		if($request->hasFile('logo_menu') ){
			$imageName 				= $strings->str2alias($event->menu_name);
			$nmImg      			= strtolower($imageName) . '.' .
			$request->file('logo_menu')->getClientOriginalExtension();
			$request->file('logo_menu')->move($path, $nmImg);
		}

		return $event;
	}
	public function updateEventMenu(Request $request, $id)
	{
		$strings                = new StringClass();
        $name		            = $strings->str2alias($request->menu_name);

		$now = \Carbon\Carbon::now();
		$year = date('Y', strtotime($now));
		$month = date('m', strtotime($now));
        $days = date('d', strtotime($now));
		if($request->hasFile('logo_menu') ){
			$bs                     = $request->file('logo_menu')->getClientOriginalExtension();
			$nombreCarpeta 			= preg_replace('/\s+/', '.', $year . "/" . $month . "/" . $days);
			$fileimg                = $name . '.' .$bs;
			$imgs		            = 'img/event/'.$nombreCarpeta .'/' .$fileimg;
			$path 					= base_path() .'/public/img/event/'.$nombreCarpeta;
		}else{
			$imgs					= $request->logo_menu;
		}
		if($request->hasFile('slide') ){
			$bs                     = $request->file('slide')->getClientOriginalExtension();
			$nombreCarpeta 			= preg_replace('/\s+/', '.', $year . "/" . $month . "/" . $days);
			$fileslide                = $name . '-slide.' .$bs;
			$slides		            = 'img/event/'.$nombreCarpeta .'/' .$fileslide;
			$pathslide 					= base_path() .'/public/img/event/'.$nombreCarpeta;
		}else{
			$slides					= $request->slide;
		}

		$event					= MMenuEvents::find($id);

		$event->menu_name		= $request->menu_name;
		$event->menu_alias		= $name;
		$event->parent			= $request->parent;
		$event->status			= 1;
		$event->logo_menu		= $imgs;
		$event->slide			= $slides;
		$event->description		= $request->description;
		$event->sort_order		= $request->sort_order;

		if($request->hasFile('logo_menu') ){
			$imageName 				= $strings->str2alias($event->menu_name);
			$nmImg      			= strtolower($imageName) . '.' .
			$request->file('logo_menu')->getClientOriginalExtension();
			$request->file('logo_menu')->move($path, $nmImg);
		}
		if($request->hasFile('slide') ){
			$imageName 				= $strings->str2alias($event->menu_name .'-slide');
			$nmImg      			= strtolower($imageName) . '.' .
			$request->file('slide')->getClientOriginalExtension();
			$request->file('slide')->move($pathslide, $nmImg);
		}
		$event->save();

		return $event;
	}

	/* Event */

	public function EventPaginate()
	{
		return MEvents::paginate(10);
	}
	public function Insertevents(Request $request)
	{
		$event					= new MEvents;

		$event->text_event		= $request->text_event;
		$event->menu_id			= $request->menu_id;
		$event->css				= $request->css;

		$event->save();

		return $event;
	}
	public function updateEvents(Request $request, $id)
	{
		$event					= MEvents::find($id);

		$event->text_event		= $request->text_event;
		$event->menu_id			= $request->menu_id;
		$event->css				= $request->css;

		$event->save();

		return $event;
	}
	public function postByAlias($alias, $category, $limit, $paginate)
	{
		$post			= MPostCategory::join('posts', 'post_categories.id', 'posts.post_category')
							->where(function($queries) use ($alias){
									$queries->where('post_categories.category_alias', 'LIKE', $alias);
							})
							->where(function($queries) use ($category){
									$queries->where('post_categories.category_name', 'LIKE', '%' .$category .'%');
							});
		if($limit > 0){
				$posts 	= $post->orderBy('posts.id', 'asc')->limit($limit)->get();
		}
		if($paginate > 0){
			$posts		= $post->orderBy('posts.id', 'asc')->paginate($paginate);
		}
		if(!empty($post)){
				return $posts;
		}
	}
	public function GetPosByHitstByCategory($post_category)
	{
		$post			= MPost::where('post_category', $post_category)
							->orderBy('post_hits', 'desc')->limit(5)->get();
		if(!empty($post)){
			return $post;
		}
	}
	public function getCatScore($parent,$fase,$years)
	{
		$cat			= MCatScore::where('parent_id', $parent)
		->where(function($queries) use ($years){
			$queries->where('years', 'LIKE', '%' . $years . '%');
		})
		->where(function($queries) use ($fase){
			$queries->where('fase', 'LIKE', '%' . $fase . '%');
		})->get();
		return $cat;
	}
	public function getScore($cat_id)
	{
		$score			= MScore::where('cat_score_id', $cat_id)
						->get();
		return $score;
	}
}
