
<div class="pb-4" id="konsultasi">
<div class="container">
  <div class="row py-4">
    <div class="col-md-12">
      <h1 class="p-0 m-0">Konsultasi Hukum</h1>
      <h4 class="p-0 m-0">Bersama Kami</h4>
    </div>
  </div>
<form method="POST" action="{{ route('konsultasi.store') }}">
  {{ csrf_field() }}
  <div class="row py-4">
    <div class="col-md-6">

        <div class="form-group">
          <label for="name">Nama Lengkap</label>
          <input type="text" class="form-control" name="name" id="name" placeholder="Full Name,.." required>
        </div>

        <div class="form-group">
          <label for="email">Email address</label>
          <input type="email" class="form-control" name="email" id="email" placeholder="email@example.com" required>
        </div>

        <div class="form-group">
          <label for="telp">Telp / HP</label>
          <input type="text" class="form-control" name="telp" id="telp" placeholder="08xxxxxxxxxxx" required>
        </div>

        <input type="hidden" class="form-control" name="jobs" id="jobs" placeholder="Marketing Manager">
        <input type="hidden" class="form-control" name="address" id="address" placeholder="Indah Apartement">

        <!-- <div class="form-group">
          <label for="jobs">Pekerjaan</label>
          <input type="hidden" class="form-control" name="jobs" id="jobs" placeholder="Marketing Manager">
        </div>

        <div class="form-group">
          <label for="address">Alamat Lengkap</label>
          <input type="hidden" class="form-control" name="address" id="address" placeholder="Indah Apartement">
        </div> -->

      </div>

      <div class="col-md-6">
        <div class="form-group">
          <label for="case">Pertanyaan</label>
          <textarea class="form-control" name="case" id="case" rows="6"></textarea>
        </div>

        <!--<label>Butuh pengacara?</label><br />
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="advokat" id="advokat1" value="1" required>
          <label class="form-check-label" for="advokat1">Ya</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="radio" name="advokat" id="advokat2" value="0">
          <label class="form-check-label" for="advokat2">Tidak</label>
        </div>

        <div class="form-group">
          <label for="question">Pertanyaan</label>
          <input type="text" class="form-control" name="question" id="question" placeholder="Bagaimana,...">
        </div>-->

        <div class="py-4 pull-right"><button class="btn btn-md btn-secondary">KIRIMKAN</button></div>

      </div>
  </div>
</form>
</div>
</div>
