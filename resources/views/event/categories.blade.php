@extends('layouts.event')
@section('content') 
<link href="{{ URL::asset('css/event/categories.css') }}" rel="stylesheet">
<div class="container">
    <div class="row">
        <div class="col-sm-3" style="padding: 21px">
          <div class="sticky_column" data-sticky_column="">
          <div class="row">
            <h3>Most Popular</h3>
            <hr />
            @foreach($cat_id as $idCat)
              @foreach($postPopuler->GetPosByHitstByCategory($idCat->id) as $cat)
              <div class="lib-panel">
                  <div class="row box-shadow">
                      <div class="col-md-6">
                          <a href="{{ route('detailPost', [ $alias, $categories_name->FirstAliasByID($cat->post_category), $cat->post_alias] ) }}">
                              <img class="lib-img" src="<?php echo route('cdn', $cat['post_img'] ); ?>">
                          </a>
                      </div>
                      <div class="col-md-6">
                          <div class="lib-row lib-header">
                              <a href="{{ route('detailPost', [ $alias, $categories_name->FirstAliasByID($cat->post_category), $cat->post_alias] ) }}">
                                  {!! str_limit(strip_tags($cat->post_title), $limit = 25, $end = '...') !!}
                              </a>
                              <div class="lib-header-seperator"></div>
                          </div>
                          <div class="lib-row lib-desc">
                              <a href="{{ route('detailPost', [ $alias, $categories_name->FirstAliasByID($cat->post_category), $cat->post_alias] ) }}">{!! str_limit(strip_tags($cat->post_text), $limit = 80, $end = '...') !!}</a>
                          </div>
                      </div>
                  </div>
              </div>
              @endforeach
            @endforeach
          </div>
          </div>
        </div>
        <div class="col-sm-9" style="padding: 21px">
        <h2>{{ $title_cat }}</h2>
        <hr />
            <div id="categories">
                @include('event.includes.card_categories')
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">

$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        getArticles(url);
        window.history.pushState("", "", url);
    });

    function getArticles(url) {
        $.ajax({
            url : url
        }).done(function (data) {
            $('#categories').html(data);
        }).fail(function () {
            alert('Articles could not be loaded.');
        });
    }
});
</script>
@endsection
