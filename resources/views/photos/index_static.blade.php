@extends('henryweb.layouts.frontend')
@section('pageTitle', 'Album Photo')
@section('content')

    <div class="bg-navbar"></div>

    <div class="container">
      <div class="row">

        <div class="col-md-12">
        <h2>Photos List</h2>
        <hr />
          <div class="row">

            <div class="col-md-4 py-3">
              <img class="img-fluid mb-2" style="width:100%;height:250px;object-fit:cover;" src="img/gallery/2020/01/17/henry-indraguna.jpeg" />
              <h5><a href="{{ route('photos.view', 2) }}">Award Ceremony IDPBA 2016</a></h5>
            </div>

          <hr />
          <nav></nav>
          </div>
        </div>


      </div>
    </div>

@endsection
