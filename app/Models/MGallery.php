<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MGallery extends Model
{
    protected $table        = 'gallery';
    protected $fillabel     = ['id', 'gallery_title', 'gallery_caption', 'gallery_status', 'category_image', 'gallery_file'];
    
    public function Galleries()
    {
        return $this->belongsTo('App\Models\MImageCategory','category_image','id');
    }
}
