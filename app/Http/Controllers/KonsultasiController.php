<?php

namespace App\Http\Controllers;

use App\Mail\KonsultasiEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class KonsultasiController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    Mail::to("henryindragunachannel@gmail.com")->send(new KonsultasiEmail($request));

    $success = "Sukses";

    return view('henryweb.email.konsultasiSent', compact('success'));
  }
}
