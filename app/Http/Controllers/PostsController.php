<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Models\MPost;
use App\Models\MPostCategory;

use App\Classes\StringClass;
use App\Classes\Posts;
use App\Classes\PostCategory;

use DB;
use Carbon\Carbon;

class PostsController extends Controller
{
    protected $posts;
    protected $postsLeft;
    protected $postsRight;

    public function __construct()
    {
        parent::__construct();
        $this->posts = new Posts;
        $this->postcategory = new PostCategory;

		    $this->middleware('auth', ['only' => ['create', 'store', 'edit', 'delete']]);

    }

    public function view(Request $request, $alias)
    {
        $postview   = MPost::where('post_alias', $alias)->where('post_status', 1)->firstOrFail();
        $addHits    = $this->posts->addHits($postview->id);
        $postsBlog  = $this->posts->getAll(['limit' => 4, 'category' => $postview->post_category]);
        return view('posts.view', compact('postsBlog', 'postview'))->with(['postsLeft'=>$this->postsLeft, 'postsRight'=>$this->postsRight]);
    }

    public function blog(Request $request, $alias)
    {

            $category = $this->postcategory->getByAlias($alias);
            $categoryName = $category->category_name;

            /* Add Sub Category */
            $subCat = $this->postcategory->getCategoryIdByParentId($category->id, ['toArray' => true]);
            $subCat[] = $category->id;
            /* /Add Sub Category */

            $postsBlog = $this->posts->getAll(['category' => $subCat, 'paginate' => 11]);

            return view('posts.blog', compact('postsBlog', 'categoryName'))->with(['postsLeft'=>$this->postsLeft, 'postsRight'=>$this->postsRight]);

    }

    public function tag(Request $request, $tag)
    {
        $postsBlog = $this->posts->postByTags(str_replace('-', ' ', $tag));
        $categoryName = $tag;
        return view('posts.blog', compact('postsBlog', 'categoryName'))->with(['postsLeft'=>$this->postsLeft, 'postsRight'=>$this->postsRight]);
    }

    public function search(Request $request)
    {
        $query = $request->get('query');
        $categoryName = $query;
        $postsBlog = $this->posts->getAll(['query' => $query, 'paginate' => 20]);
        return view('posts.blog', compact('postsBlog', 'categoryName'))->with(['postsLeft'=>$this->postsLeft, 'postsRight'=>$this->postsRight]);
    }

    public function rss()
    {
        $postsBlog = array();
        $post = $this->posts->getAll(['limit' => 10, 'category' => [2,3,4,5,6,7,8,9,10]]);

        foreach($post as $p) {
          $postsBlog[] = [
              'post_id' => $p->post_id,
              'post_title' => htmlentities($p->post_title),
              'post_alias' => $p->post_alias,
              'start_publishing' => Carbon::createFromFormat('Y-m-d H:i:s', $p->start_publishing, 'Asia/Jakarta')->toRssString(),
              'post_text' => htmlentities(strip_tags($p->post_text)),
            ];
        }

        return response()->view('posts.rss', compact('postsBlog'))->header('Content-Type', 'text/xml');
    }

}
