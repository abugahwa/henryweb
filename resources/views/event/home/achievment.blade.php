@if($achievment->count() > 0)
<div class="row">
  <div class="col-md-6">
      <h4><strong>BRAND</u></strong> ACHIEVMENT</h4>
      <div class="list"></div>
      <br />
  </div>

  <div class="col-md-6">
    <div class="form-group pull-right" id="pagination_achievment">
      <a href="#Achiev" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
      <a href="#Achiev" data-slide="next"><i class="fas fa-chevron-right"></i></a>
    </div>
  </div>
</div>
@endif
<div style="clear:both;"></div>
@if(!empty($achievment))
<div class="row">
    <div id="Achiev" class="carousel slide" data-ride="carousel" data-interval="9000">
        <div class="carousel-inner row w-100 mx-auto" role="listbox">
          <?php $n = 0;?>
          @foreach($achievment as $achiev)
          <div class="carousel-item col-md-4  <?php if($n == 0) echo "active"; $n++; ?>">
              <div class="card">
                  <a class="card_achiev" href="{{ route('detailPost', [$alias,$achiev->category_alias,$achiev->post_alias])}}">
                    @if(route('cdn', $achiev['post_img'] ) < 1)
                      <img class="card-img-top" src="<?php echo route('cdn', $achiev['post_img'] ); ?>" alt="{{ $achiev->post_im_caption }}">
                    @else
                      <img class="img-fluid mx-auto d-block" src="//via.placeholder.com/600x400?text=infobrand" alt="{{ $achiev->post_im_caption }}">
                    @endif
                  </a>
                  <div class="card-body">
                      <p class="card-text"><a href="{{ route('detailPost', [$alias,$achiev->category_alias,$achiev->post_alias])}}">{!! str_limit(strip_tags($achiev->post_text), $limit = 80, $end = '...') !!}</a></p>
                  </div>
              </div>
          </div>
          @endforeach
        </div>

    </div>
    @if($achievment->count() > 0)
    <div class="col-md-12">
      <br />
      <div class="form-group pull-right">
        <a href="{{ route('postcategories', [$alias,$achiev->category_alias]) }}" class="float-right"><u>Index</u></a>
      </div>
    </div>
    @endif
</div>
@endif
