<script>
function edit_image(id){
	$('[name="id"]').val(id);
	$('#editImage').modal('show');
}
</script>
<div id="editImage" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
		{{ method_field('post') }}
		{!! Form::open(['method' => 'POST', 'files' => true, 'class' => 'form-horizontal', 'id' => 'edit_image'])  !!}
		{{ csrf_field() }}
      <div class="modal-body">
        <p>
				<input type="hidden" name="id">
				<div class="form-group" style="padding: 10px;">
					<input type="file" name="b_image" id="uploadFile">
				</div>
		</p>
      </div>
      <div class="modal-footer">
        <button type="submti" class="btn btn-danger" id="btnSave">Save</button>
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
      </div>
		{!! Form::close() !!}
    </div>

  </div>
</div>
<script>
$("form#edit_image").submit(function(event){
		event.preventDefault();
		var formData = new FormData($(this)[0]);
		$('#btnSave').text('saving...');
		$('#btnSave').attr('disabled',true);
		$.ajax({
			url: "{{ url('/update-profile-member') }}",
			type: 'POST',
			data: formData,
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			dataType: "JSON",
			success: function(data)
			{
				toastr.success('Successfully Edit Your Image!', 'Success Alert', {timeOut: 5000});
				
				$('.item'+ data.id).replaceWith('<div class="item'+ data.id +'"><a href="" onclick="edit_image('+data.id+')"><img src="'+data.avatar +'" class="img-circle center-block"></a></div>');
				
				$('#btnSave').text('save'); //change button text
				$('#btnSave').attr('disabled',false); //set button enable
				$('#editImage').modal('toggle');
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error adding / update data');
				$('#btnSave').text('save'); //change button text
				$('#btnSave').attr('disabled',false); //set button enable 

			}
		
		});
		return false;
	});	
</script>