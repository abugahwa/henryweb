<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MMenuEvents extends Model
{
    protected $table	= 'menu_events';
	protected $fillable	= ['id', 'menu_name', 'menu_alias', 'parent', 'status', 'logo_menu', 'description', 'created_at', 'updated_at', 'sort_order'];
}
