@extends('henryweb.layouts.frontend')
@section('pageTitle', $title)
@section('content')
<style>
.carousel-indicators{
    list-style: none!important;
}
.carousel-indicators li, .carousel-indicators li.active{
    width: 70px!important;
    height: 70px!important;
    background-color: #fff!important;
    position: relative!important;
    margin: 10px!important;
}
.carousel-indicators img{
    position: absolute!important;
    width: 100%!important;
    height: 100%!important;
    top: 0!important;
    left: 0!important;
    z-index: 1000!important;
}
</style>
    <div class="bg-navbar"></div>

    <div class="container">
      <div class="row">
        <div class="col-md-8 py-2">

          <h1>{{ $title }}</h1>
          <hr />
          <div id="photos" class="carousel slide crsl-custom" data-ride="carousel">
            <div class="carousel-inner">
              <?php
              $n = 0;
              foreach($slides as $p) { ?>

              <div class="carousel-item <?php if($n == 0) echo "active"; ?>">
                <img class="d-block" style="width:100%;height:450px !important;object-fit:cover;" src="{{ url($p->gallery_file) }}" alt="">
              </div>

              <?php $n++; } ?>

              <a class="carousel-control-prev" href="#photos" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#photos" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>

            </div>

            <ol class="carousel-indicators">
              <?php
              $n = 0;
              foreach($slides as $p) { ?>
              <li data-target="#photos" data-slide-to="{{ $n }}" <?php if($n == 0) echo "class='active'"; ?>>
                <img width="50" height="50" src="{{ url($p->gallery_file) }}" alt="...">
              </li>
              <?php $n++; } ?>
            </ol>
          </div>


        </div>
        <div class="col-md-4 py-2">
          @include('shared.rightColumn')
        </div>
      </div>
    </div>


@endsection
