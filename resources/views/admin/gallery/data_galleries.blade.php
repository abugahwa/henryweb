@foreach($gallery as $cb)
<div class="thumb_gal item{{$cb->id}}">
  <a class="thumbnail fancybox" rel="ligthbox" href="{{ asset($cb->gallery_caption )}}">
    <img class="img-responsive" alt="" src="{{ asset($cb->gallery_file )}}" />
    <div class="text-center">
      <label>{{ $cb->gallery_title }}</label>
    </div>
  </a>
  <a id="cat_name" class="btn btn-sm btn-primary pull-right" href="javascript:void(0)" title="Edit" onclick="edit_data({{$cb->id}})"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
  <a href="javascript:void(0)" class="btn-sm btn-info pull-right" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="Delete Data" onclick="delete_data({{$cb->id}})" style="margin-right: 4px;">Delete <i class="fa fa-trash-o" aria-hidden="true"></i></a>
</div>
@endforeach

@if($gallery->count() === 0)
  <div class="alert alert-danger">
    No data found.
  </div>
@endif
{!! $gallery->render() !!}
