<?php

Route::get('report', 'Admin\ReportController@index')->name('report');
Route::get('report-search', 'Admin\ReportController@searchPost')->name('search.report');
Route::post('print-report', 'Admin\ReportController@printOut')->name('print.report');