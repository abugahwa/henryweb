@extends('henryweb.layouts.frontend')
@section('pageTitle', $video->title)
@section('content')

    <div class="bg-navbar"></div>

<style>
#video-player {
  background-color: #000000;
  color: #FFFFFF;
  margin-bottom: 10px;
}

.playlist {
  position: relative;
  margin: 0px !important;
  padding: 5px !important;
}

.playlist a {
  color: #000000;
}

.playlist a:hover {
  text-decoration: none;
}

.playlist .image img {
  width:100%;
  margin-bottom: 10px;
}

.playlist .body {
  display:block;
  overflow-y: hidden;
  height: 60px;
  line-height: 15pt;
}
@media only screen and (max-width: 599px) {
	#video-player iframe{
		width: 100%;
	}
}
@media only screen and (min-width: 600px) and (max-width: 960px) {
	#video-player iframe{
		width: 100%;
	}
}
</style>


    <div class="container">
      <div class="" style="margin:10px 0px;"></div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-9">

            <div id="video-player">
              <iframe class="mx-auto d-block p-3" src="https://www.youtube-nocookie.com/embed/{{ $video->video_id }}?autoplay=1&rel=0&controls=0" width="720" height="405" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>

            <div class="">
              <h1 style="font-size:20pt;"><strong>{{ $video->title }}</strong></h1>
			        <p style="text-align: justify;">{{ $video->description }}</p>
              <p style="float:left;"><small>{{ $video->hits }}x ditonton</small></p>
              <div style="float:right;">

                    <!-- AddToAny BEGIN -->
                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="display:block;margin:0px auto;">
                    <a class="a2a_button_facebook"></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_google_plus"></a>
                    <a class="a2a_button_whatsapp"></a>
                    <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                    </div>
                    <script async src="https://static.addtoany.com/menu/page.js"></script>
                    <!-- AddToAny END -->

              </div>
              <div class="clearfix"></div>
            </div>
            <hr />

            <div class="row">

              <div class="col-md-12">
                <h3 class="category_title">{{ strtoupper('Video Lainnya') }}</h3>

                <div class="row">
                <?php foreach($videos as $p) { ?>
                  <div class="col-md-4 py-3">
                    <img class="img-fluid mb-2" src="https://img.youtube.com/vi/{{ $p->video_id }}/0.jpg" />
                    <h5><a style="color:#000000;" href="{{ route('videos.view', $p->id) }}">{{ $p->title }}</a></h5>
                  </div>
                <?php } ?>

                </div>
              </div>

            </div>

        </div>

        <div class="col-md-3 order-3">

            <h3 class="category_title">{{ strtoupper('Most Viewed') }}</h3>
            <?php foreach($most as $p) { ?>
              <div class="col-md-12 py-3">
                <img class="img-fluid mb-2" src="https://img.youtube.com/vi/{{ $p->video_id }}/0.jpg" />
                <h5><a style="color:#000000;" href="{{ route('videos.view', $p->id) }}">{{ $p->title }}</a></h5>
              </div>
            <?php } ?>

        </div>

      </div>
    </div>


@endsection
