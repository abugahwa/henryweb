<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\MImageCategory;
use App\Models\MGallery;

use App\Classes\StringClass;
use App\Classes\Galleries;
use App\Classes\GalleriesCategory;

use DB;
use Carbon;

class GalleryController extends Controller
{
	protected $galleries;
	protected $categorygalleris;

	public function __construct()
	{
		$this->galleries 				= new Galleries;
		$this->categorygalleris 		= new GalleriesCategory;
	}
    public function index(Request $request)
    {
		$catid											= 0;
		$category_status    				= 0;
    $gallery        						= $this->galleries->GetAll();
		$CateImg										= $this->categorygalleris->CatImageAll();
		$category_image							= $this->categorygalleris->pluckCatAll();

		if($request->ajax()){
			$category_images					= $request->category_image;
			$title										= $request->title;

			$gallery									= $this->galleries->GalleriByCatIdNTitle($title, $category_images);
			
			return view('admin.gallery.data_galleries', compact('gallery','CateImg', 'catid', 'category_status', 'category_image'))->render();
		}
    return view('admin.gallery.gallery_list', compact('gallery','CateImg','catid','category_status', 'category_image'));
    }
	public function create(Request $request)
    {
        $catid              			= 0;
        $category_status    			= 0;
        $CateImg            			= $this->categorygalleris->GetAll();

        return view('admin.gallery.add_gallery', compact('CateImg','catid','category_status'));
    }
	public function store(Request $request)
    {
        $gal							= $this->galleries->InsertGalleries($request);
		return response()->json($gal);
    }
	public function edit($id)
    {
        $data           				= $this->galleries->FindById($id);
        $catid          				= $this->galleries->PutCatImg($id);
        $category_status    		= $this->galleries->PutStatus($id);
        $CateImg        				= $this->categorygalleris->CatImageAll();
		echo json_encode($data);
    }
	public function update(Request $request, $id)
    {
		$gal							= $this->galleries->UpdateGalleries($request, $id);
		return response()->json($gal);
    }
	public function destroy(Request $request,$id)
    {
        $gallery                		= $this->galleries->FindGalleriesId($request,$id);
        $gettitle               		= $this->galleries->PutTitleGalleries($id);
        $getlinkimg             		= $this->galleries->PutGalleriesFile($id);

        $gallery->delete();
        unlink(public_path($getlinkimg));
		$data							= array('id' => $id);
		return response()->json($data);
    }
	public function combo_data($id)
	{
		$id_cat											= MGallery::where('id', $id)->first()->post_category;
		$query											= MGallery::where('category_image', $id_cat)->get();

		$data          							= $this->galleries->FindById($id);
        $catid          				= $this->galleries->PutCatImg($id);
        $category_status				= $this->galleries->PutStatus($id);
        $CateImg        				= $this->categorygalleris->CatImageAll();
		$json							= '';

		if($CateImg->count()){
            foreach($CateImg as $role){
				echo '<option value="'.$role->id.'" '.($catid == $role->id ?'selected="selected"':"").'>'.$role->category_image.'</option>';
            }
		}
	}
	public function CategoryView()
	{
		$cats							= $this->categorygalleris->GetAll();
		$category_status 				= 0;

		return view('admin.gallery.categories', compact('cats','category_status'));
	}
	public function InsertCategoriesImage(Request $request)
	{
		$cats							= $this->categorygalleris->InsertCategory($request);

		return response()->json($cats);

	}
	public function EditCategoryGallery($id)
	{
		$data							= $this->categorygalleris->FindId($id);
		$b_status       				= $this->categorygalleris->PutStatus($id);

		$cats	= array('data' => $data, 'b_status' => $b_status);
		return response()->json($cats);
	}
	public function UpdateCatImage(Request $request, $id)
	{
		$cats							= $this->categorygalleris->UpdateCategory($request,$id);;

		return response()->json($cats);
	}
	public function DeleteCatImage(Request $request, $id)
	{
		$this->categorygalleris->DeleteCatImage($request, $id);
		$data							= array('id' => $id);
		return response()->json($data);
	}
}
