<?php

namespace App\Http\Controllers\Member\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

use App\Models\MUserProfile;

use App\Models\MPost;
use App\User;
use App\Models\MPostCategory;

use App\Classes\StringClass;
use App\Classes\Posts;
use App\Classes\PostCategory;
use App\Classes\Profile;

use DB;
use Auth;
use Carbon;

class MembersController extends Controller
{
    protected $profile;
	
	public function __construct()
    {
        parent::__construct();
		$this->profile = new Profile;
		$this->posts		= new Posts;
    }
	
	public function InsertProfile(Request $request, $id)
	{
		$insert				= $this->profile->insertProfile($request,$id);
		
		return redirect()->route('memberinfo');
	}
	public function index(Request $request)
	{
		if (Auth::check()) {
			$dates = new Carbon\Carbon;
			$dates->addDays(30);
			$post				= $this->posts->chartPostByUser(\Auth::user()->id, $dates);
			$counst				= $post->count();
			
			$hits				= MPost::select(DB::raw('sum(post_hits) as sums'))
								->where('post_author',\Auth::user()->id)
								->where('created_at','<', $dates->toDateTimeString())
								->orderBy('post_hits', 'asc')
								->get();
								
			foreach($hits as $sum){
				$total_hits		= $sum->sums;
			}	
			return view('member.members_index', compact('total_hits','post','counst'));
		}else{
			return redirect()->route('loginmember');
		}
	}
	public function getLogout()
    {
		Auth::logout();
		\Session::flush();
        return redirect()->route('loginmember');
    }
	public function update(Request $request)
	{
		$datas					= $this->profile->profileUpdate($request);
		
		return response()->json($datas);
	}
	public function GetProfile($id)
	{
		$data					= MUserProfile::find($id);
		
		return response()->json($data);
	}
	public function combo_data($id)
	{
       $data					= $this->profile->getGender($id);
	   
	   echo $data;
	}
	public function updateProfile(Request $request)
	{
		$data					= $this->profile->UpdateDataUser($request);
		return response()->json($data);
	}
}
