<?php

Route::get('add-gallery', 'Admin\GalleryController@create')->name('galleries.add');
Route::post('save-gallery', 'Admin\GalleryController@store')->name('galleries.save');
Route::get('list-gallery', 'Admin\GalleryController@index')->name('galleries.list');
Route::get('edit-gallery/{id}', 'Admin\GalleryController@edit')->name('galleries.edit');
Route::post('update-gallery/{id}', 'Admin\GalleryController@update')->name('galleries.update');
Route::get('delete-gallery/{id}', 'Admin\GalleryController@destroy')->name('galleries.delete');
Route::get('get-galleries/{id}', 'Admin\GalleryController@combo_data')->name('galleries.get-gallery');

Route::get('category-galleries', 'Admin\GalleryController@CategoryView')->name('categoryimage');
Route::post('insert-categories-gallery', 'Admin\GalleryController@InsertCategoriesImage')->name('insertcategoryimage');
Route::get('edit-categories-gallery/{id}', 'Admin\GalleryController@EditCategoryGallery')->name('editcategoryimage');
Route::post('update-categories-gallery/{id}', 'Admin\GalleryController@UpdateCatImage')->name('updatecategoryimage');
Route::get('delete-categories-gallery/{id}', 'Admin\GalleryController@DeleteCatImage')->name('deletecategoryimage');
