@extends('layouts.admin')
@section('content')

	<div class="" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Category Post</h3>
            </div>
			<div class="title_right">
				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('/query/') }}" method="GET" role="search">
					<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
						<div class="input-group">
							<input type="text" class="form-control" name="search" placeholder="Search for...">
							<span class="input-group-btn">
							  <button class="btn btn-default" type="submit">Go!</button>
							</span>
					  </div>
					</div>
				</form>
			</div>
        </div>
		
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>Category Post </h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Settings 1</a></li>
									<li><a href="#">Settings 2</a></li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					
					<div class="x_content">
						
						<a href="#" class="btn btn-info add-modal">Add</a>
						<div style="clear: both;"></div>
						<table class="table table-striped" id="tabs">
    							<thead bgcolor="#9853bb" style="color: #fff;">
    								<tr>
    									<th>Name</th>
    									<th>Action</th>
    								</tr>
    							</thead>
    							<tbody>
    							    
    								@foreach($cats as $key => $b)
    								<tr class="item{{$b->id}}">
    									<td>{{ $b->category_name }}</td>
    								
    									<td><a class="btn-sm btn-success" href="javascript:void(0)" title="Edit" onclick="edit_data( {{$b->id}} )"><i class="fa fa-pencil-square-o"></i></a> 
    									<a href="javascript:void(0)" class="btn-sm btn-danger" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="Delete Article" onclick="delete_data({{$b->id}})"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
    								</tr>
    								@endforeach
    							</tbody>
    						</table>	
							{!! $cats->render() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="{{ asset('assets/bower_components/gentelella/vendors/jquery/dist/jquery.min.js') }}"></script>

<script type="text/javascript">
$(document).ready(function(){
	

	$(window).load(function(){
		$('#table').removeAttr('style');
	})
	
	/* modal add */
	$(document).on('click', '.add-modal', function() {
        $('.modal-title').text('Add');
		$('#cats')[0].reset(); 
        $('#addModal').modal('show');
    });
	/* modal edit */
	$(document).on('click', '.edit_data', function() {
        $('.modal-title').text('Add');
        $('#editModal').modal('show');
    });

	
	
});	
</script>

@include('admin.posts.category.add')
@include('admin.posts.category.edit')
@include('admin.posts.category.delete')
@endsection

