<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MUserProfile extends Model
{
    protected $table		= 'user_profile';
	protected $fillable		= ['id', 'first_name', 'last_name', 'company', 'brand', 'phone', 'mobile_phone','address','id_user','created_at', 'updated_at', 'gender'];
	
	public function Users()
	{
		return $this->belongsTo('App\User','id_user','id');
	}
}
