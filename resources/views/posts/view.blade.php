@extends('henryweb.layouts.frontend')
@section('pageTitle', $postview->post_title)

@section('metaTags')
<!-- Gahwa SEO Attack!! -->
<meta name="description" content="{{ str_limit(strip_tags($postview->post_text), $limit = 150, $end = '...') }}"/>
<meta name="keywords" content="{{ $postview->post_tags }}"/>
<meta name="author" content="{{ $postview->post_author }}"/>
<link rel="canonical" href="<?php echo env('APP_URL') . "/" . $postview->post_alias .".phtml"; ?>"/>
<link rel="publisher" href="{{ env('APP_NAME') }}"/>
<meta property="og:locale" content="id_ID"/>
<meta property="og:type" content="article"/>
<meta property="og:title" content="{{ $postview->post_title }}"/>
<meta property="og:description" content="{{ str_limit(strip_tags($postview->post_text), $limit = 150, $end = '...') }}"/>
<meta property="og:url" content="<?php echo env('APP_URL') . "/" . $postview->post_alias .".phtml"; ?>"/>
<meta property="article:publisher" content="https://www.facebook.com/officialhenryindraguna/"/>
<meta property="article:author" content="https://www.facebook.com/officialhenryindraguna/"/>

<?php
$tags = explode(",", $postview->post_tags);
foreach($tags as $tag) { ?>
<meta property="article:tag" content="<?php echo trim($tag); ?>"/>
<?php } ?>

<meta property="article:section" content="article"/>
<meta property="article:published_time" content="<?php echo date("c", strtotime($postview->start_publishing)); ?>"/>
<meta property="article:modified_time" content="<?php echo date("c", strtotime($postview->finish_publishing)); ?>"/>
<meta property="og:updated_time" content="<?php echo date("c", strtotime($postview->finish_publishing)); ?>"/>
<meta property="fb:admins" content=""/>
<meta property="og:image" content="<?php echo url($postview->post_img); ?>" alt="<?php echo $postview->post_title; ?>"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:description" content="{{ str_limit(strip_tags($postview->post_text), $limit = 150, $end = '...') }}"/>
<meta name="twitter:title" content="{{ $postview->post_title }}"/>
<meta name="twitter:site" content="@henryindraguna2"/>
<meta name="twitter:image" content="<?php echo url($postview->post_img); ?>" alt="<?php echo $postview->post_title; ?>"/>
<meta name="twitter:creator" content="@henryindraguna2"/>
<!-- End :) -->
@endsection

@section('content')

    <div class="bg-navbar"></div>

    <div class="container">
      <div class="row">

        <div class="col-md-8 order-1 order-md-2 postView">
            <br />
            <h1 class="title"><?php echo $postview->post_title; ?></h1>
            <p class="detail">Posted by: <i class="fa fa-user"></i>
              <span itemprop="author" itemscope="" itemtype="http://schema.org/Person">
								<strong itemprop="name" style="margin-right: 10px;"><?php echo $postview->author->name; ?></strong>
							</span>
              <span style="margin-right: 10px;"><i class="fa fa-clock"></i> <?php echo date("d-m-Y H:i", strtotime($postview->start_publishing)); ?> WIB</span>
              <span style="margin-right: 10px;"><i class="fa fa-eye"></i> <?php echo $postview->post_hits; ?> viewer</span>
            </p>

            <img class="img-responsive" style="width:100%" src="<?php echo $postview->post_img; ?>" alt="<?php echo $postview->post_title; ?>" />
            <br />
            <small style="float:right;"><?php echo $postview->post_img_caption; ?></small>

            <div class="clearfix row-space"></div>

            <?php echo $postview->post_text; ?>

            <hr />

            <div class="container">
                <div class="row row-space">
                    <p>
                        <?php foreach(explode(',', $postview->post_tags) as $tag) { ?>
                        <span class="badge badge-dark"><a style="color:#FFFFFF;" href="{{ route('posts.tag', str_slug($tag, '-'))}}"><?php echo $tag; ?></a></span>
                        <?php } ?>
                    </p>
                </div>
            </div>

            <hr />

            <div class="container">

            <div class="row row-space">
                <div style="font-size:14pt;font-weight:bold;">Share this article! &nbsp;</div>
                <div class="">
                  <!-- AddToAny BEGIN -->
                  <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                  <a class="a2a_button_facebook"></a>
                  <a class="a2a_button_whatsapp"></a>
                  <a class="a2a_button_linkedin"></a>
                  <a class="a2a_button_copy_link"></a>
                  <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                  </div>
                  <script async src="https://static.addtoany.com/menu/page.js"></script>
                  <!-- AddToAny END -->
                </div>
            </div>
            </div>

            <div class="container my-4">
              <div class="row">
              <div class="col-md-12">
                <a href="https://api.whatsapp.com/send?phone=6281573000000&text=Halo%21%20HIP%20Law%20Firm" target="_blank">
                <img style="width:100%;" class="mx-auto d-none d-sm-block" src="{{ asset('images/iklan-layanan-konsultasi-hukum-new-1.jpg') }}" alt="HIP Lawyers">
                <img style="width:100%;" class="mx-auto d-sm-none" src="{{ asset('images/iklan-layanan-konsultasi-hukum-mobile-new-1.jpg') }}" alt="HIP Lawyers">
                </a>
                <!--<div class="alert alert-secondary text-center" role="alert">
                  Untuk informasi layanan konsultasi hukum: <br /><strong>0811-1919-182 / 0812-5280-9353</strong>
                </div>-->
              </div>
              </div>
            </div>

<!--            <br />
            <h2>Article Related</h2>
            <br />

            <?php foreach($postsBlog as $post) { ?>
            <div class="media">
              <div class="media-left">
                  <img class="media-object" src="<?php echo $post['post_img']; ?>" alt="<?php echo $post['post_title']; ?>" width="120" height="120" />
              </div>
              <div class="media-body">
                  <p><a href="<?php echo url($post['post_alias'].".phtml"); ?>"><?php echo $post['post_title']; ?></a><br />
                  <small>{{ str_limit(strip_tags($post['post_text']), $limit = 150, $end = '...') }}</small></p>
              </div>
            </div>
            <hr />
            <?php } ?>
-->
        </div>

        <div class="col-md-4 order-3 py-4">
          <div class="sticky_column" data-sticky_column="">
              @include('shared.rightColumn')
          </div>
        </div>
      </div>
    </div>

@endsection
