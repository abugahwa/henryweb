<div class="form-group">
	<label for="gallery_title">Category Image Name</label>
	{!! Form::text('category_image', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Gallery Name','class' => 'form-control')) !!}
</div>
<div class="form-group">
	<label for="gallery_status">Category Status</label>
	{{ Form::selectRange('gallery_status',null, 1,$category_status,['class' => 'form-control', 'id' => 'category']) }}
</div>