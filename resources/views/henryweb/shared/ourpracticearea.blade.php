
<div class="our-practice-area py-4 d-none d-md-block">
<div class="container">
  <div class="row">
    <div class="col-md-4">
      <img class="d-block w-100" src="{{ asset('images/our-practice-area.png') }}" alt="Our Practice Area">
    </div>
    <div class="col-md-4">
      <img class="d-block w-100" src="{{ asset('images/our-practice-area-2.png') }}" alt="Our Practice Area">
    </div>
    <div class="col-md-4">
      <img class="d-block w-100" src="{{ asset('images/our-practice-area-3.png') }}" alt="Our Practice Area">
    </div>
    <div class="col-md-4">
      <img class="d-block w-100" src="{{ asset('images/our-practice-area-4.png') }}" alt="Our Practice Area">
    </div>
    <div class="col-md-4">
      <img class="d-block w-100" src="{{ asset('images/our-practice-area-5.png') }}" alt="Our Practice Area">
    </div>
    <div class="col-md-4">
      <img class="d-block w-100" src="{{ asset('images/our-practice-area-6.png') }}" alt="Our Practice Area">
    </div>
    <div class="col-md-4">
      <img class="d-block w-100" src="{{ asset('images/our-practice-area-7.png') }}" alt="Our Practice Area">
    </div>
    <div class="col-md-4">
      <img class="d-block w-100" src="{{ asset('images/our-practice-area-8.png') }}" alt="Our Practice Area">
    </div>
    <div class="col-md-4">
      <img class="d-block w-100" src="{{ asset('images/our-practice-area-9.png') }}" alt="Our Practice Area">
    </div>
  </div>
</div>
</div>
