@extends('henryweb.layouts.frontend')
@section('pageTitle', 'Henry Indraguna - Advokat | Kurator dan Pengurus | Mediator | Tax')

@section('content')
<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ asset('images/henry-slider-2.jpg') }}" alt="Henry Indraguna">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('images/henry-slider-1.jpg') }}" alt="Henry Indraguna">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('images/henry-slider-3.jpg') }}" alt="Henry Indraguna">
    </div>
    <a class="carousel-control-prev" href="#carouselExampleSlidesOnly" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleSlidesOnly" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

@include('henryweb.shared.konsultasi-hukum')

<style>

.bgbgbg { background-position:left bottom; background-image: url('{{ asset('images/bg-home-profile.png') }}'); }
@media (max-width: 768px) {
  .bgbgbg { background-image: none; }
}
</style>

<div class="bg-home-profile bgbgbg" style="">
<div class="">
  <div class="row">
    <div class="col-md-8">

    </div>
    <!--<div class="col-md-4 py-4 textofprofile">
      <br /><br />
      <h2>Tentang</h2>
      <h1>ADV, Dr(c), K.P. Henry Indraguna, S.H., M.H., C.L.A., C.I.L., C.Med., C.R.A., C.T.A., C.T.L., C.M.L.C.</h1>

      <p>Henry Indraguna Pria kelahiran 29 Agustus 1973, sosoknya dikenal sebagai Pengacara sukses. Belakangan sering muncul di layar kaca, terutama saat turut menangani beberapa kasus hukum yang menyita perhatian publik. </p><p>Selain itu, ia juga merupakan ikon pengusaha sukses dengan konsep out of the box yang membawa tren baru ke dunia otomotif Indonesia.</p>
      <p><a href="{{ url('biografi.phtml') }}"><button class="btn btn-md btn-secondary">Read More,..</button></a></p>
    </div>-->
    <div class="col-md-4 py-4 px-4">
      <br />
      <br />
      <br />
      <img width="500" class="d-md-none d-lg-none d-xl-none img-fluid d-block mx-auto" src="https://henryindraguna.com/img/posts/2021/02/01/tim-kantor-hukum-hip.png" />
      <br />
      <h2>Tentang</h2>
      <h1>HENRY INDRAGUNA & PARTNERS LAW FIRM</h1>

      <p style="font-size:14pt;">HIP didukung oleh para advokat handal serta mempunyai pengalaman, kemampuan dan pengetahuan hukum yang telah teruji, mempunyai kemampuan dalam menemukan solusi hukum.</p>
      <p style="font-size:14pt;">Solusi penyelesaian perkara maupun tindakan akan dilakukan oleh H.I.P LAW FIRM sebagaimana aturan hukum yang berguna untuk melindungi kepentingan klien, melakukan pencegahan serta pembelaan dari berbagai resiko hukum yang sudah dan akan dialami klien.</p>
      <p><a href="{{ url('henry-indraguna-partners-law-firm.phtml') }}"><button class="btn btn-md btn-secondary">Read More,..</button></a></p>
    </div>
  </div>
</div>
</div>

<!--<div class="bg-home-news pb-4" style="background-image: url('{{ asset('images/bg-home-news.png') }}');">-->
<div class="bg-home-news pb-4">
<div class="container">
  <div class="row py-4">
    <div class="col-md-12 text-center">
      <h1>Henry Indraguna</h1>
      <h5>Advokat | Kurator dan Pengurus | Mediator | Tax</h5>
    </div>
  </div>
  <div class="row boxofnews">
@foreach ($postsNews as $p)
    <div class="col-md-4 py-3">
      <img class="img-responsive mb-2" src="{{ $p->post_img }}" />
      <h5><a href="{{ url($p->post_alias . '.phtml') }}">{{ $p->post_title }}</a></h5>
      <!--<p>{{ str_limit(strip_tags($p->post_text), $limit = 250, $end = '...') }}</p>-->
      <p><a href="{{ url($p->post_alias . '.phtml') }}"><button class="btn btn-sm btn-secondary pull-right">Read More,..</button></a></p>
      <div class="clearfix"></div>
    </div>
@endforeach
@foreach ($videolist as $p)
    <div style="border-left: 2px solid #CCCCCC;" class="col-md-4 py-3">
      <img class="img-responsive mb-2" src="https://img.youtube.com/vi/{{ $p->video_id }}/0.jpg" />
      <h5><a href="{{ route('videos.view', $p->id) }}">{{ $p->title }}</a></h5>
      <!--<p>{{ str_limit(strip_tags($p->post_text), $limit = 250, $end = '...') }}</p>-->
      <p><a href="{{ route('videos.view', $p->id) }}"><button class="btn btn-sm btn-secondary pull-right">Read More,..</button></a></p>
      <div class="clearfix"></div>
    </div>
@endforeach
  </div>
</div>

      <div class="text-center py-4"><a href="{{ route('posts.blog', 'latest-news') }}"><button class="btn btn-lg btn-secondary">Berita Lainnya</button></a></div>
</div>

@include('henryweb.shared.hiplawyers')

<?php /* @include('henryweb.shared.ourpracticearea') */ ?>

<!--
<div class="home-videos pb-4">
<div class="container">
  <div class="row py-4">
    <div class="col-md-12 text-center">
      <h1>Henry Indraguna Channel</h1>
      <h5>Liputan Media, Kegiatan & Tips Hukum</h5>
    </div>
  </div>
  <div class="row boxofvideos">
    <?php $n=0; ?>
@foreach ($videolist as $p)
    <div class="col-md-4 py-3">
      <img class="img-fluid mb-2 <?php if($n==0) echo "video-child-first"; ?>" src="https://img.youtube.com/vi/{{ $p->video_id }}/0.jpg" />
      <a href="{{ route('videos.view', $p->id) }}">{{ $p->title }}</a>
      <div class="clearfix"></div>
    </div>
    <?php $n++; ?>
@endforeach
  </div>

        <div class="text-center py-4"><a href="{{ route('videos.index') }}"><button class="btn btn-md btn-secondary">View More,..</button></a></div>
</div>
</div>
-->


<!--
<div class="bg-gallery-photos d-none d-md-block" style="background-image: url('{{ asset('images/bg-gallery-photos.jpg') }}');">
<div class="container">
  <div class="row">
    <div class="col-md-5">

    </div>
    <div class="col-md-7 py-4 px-0">
      <div class="row">
        <div class="col-md-12 p-0 text-right">
          <img class="pull-right" style="width:220px;height:220px;object-fit:cover;" src="https://henryindraguna.com/img/gallery/2020/01/23/captionnya-henry-indraguna-partner-law-firm-sidang-kasus-i-nyoman-dhamantra-1.jpg">
          <img class="pull-right" style="width:220px;height:220px;object-fit:cover;" src="https://henryindraguna.com/img/gallery/2020/01/23/henry-indraguna-1.jpg">
          <img class="pull-right" style="width:220px;height:220px;object-fit:cover;" src="https://henryindraguna.com/img/gallery/2020/01/23/pengacara-henry-indraguna-2.jpg">
          <img class="pull-right" style="width:220px;height:220px;object-fit:cover;" src="https://henryindraguna.com/img/gallery/2020/01/23/indonesia-digital-popular-brand.jpg">
          <img class="pull-right" style="width:220px;height:220px;object-fit:cover;" src="https://henryindraguna.com/img/posts/2020/01/19/pilkada-sukoharjo-henry-indraguna-hati-dan-mulut-saya-pasti-sama.jpg">
          <img class="pull-right" style="width:220px;height:220px;object-fit:cover;" src="https://henryindraguna.com/img/posts/2020/01/16/kp-henry-indraguna-sh-cla-cil-dedikasi-dalam-penegakan-hukum.jpg">
        </div>
        <div class="col-md-12 py-4 text-right">
          <h2>Ikuti Instagram kami <a href="https://www.instagram.com/hip_lawyers/" target="_blank"><button type="button" class="btn btn-md btn-dark">@hip_lawyers</button></a></h2>
        </div>

      </div>
    </div>
  </div>
</div>
</div>
-->
@endsection
