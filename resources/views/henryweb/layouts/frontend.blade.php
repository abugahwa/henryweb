<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156582037-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-156582037-1');
  </script>

    @yield('metaTags')

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('pageTitle') - HenryIndraguna.com</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="{{ asset('css/henryweb.css') }}" rel="stylesheet">

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicon/site.webmanifest') }}">

</head>
<body>
    <div id="app">

        <a href="{{ url('/') }}"><img width="500" class="d-md-none d-lg-none d-xl-none img-fluid d-block mx-auto" src="{{ asset('images/hip-law-office.jpg') }}" /></a>

              <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-alpha navtop">
                  <div class="container">
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                      <span class="navbar-toggler-icon"></span>
                  </button>
                      <a class="navbar-brand" href="#">
                        <hgroup>
                          <a href="{{ url('/') }}">
                            <img class="img-responsive logotop" src="{{ asset('images/logo-henry-indraguna.png') }}" />
                          </a>
                          <h1 class="site-title">Henry Indraguna</h1>
                          <h2 class="site-description">Henry Indraguna</h2>
                          <h3 class="site-tags">Henry Indraguna, Lawyer, Pengusaha</h3>
                        </hgroup>
                      </a>

                      <div class="collapse navbar-collapse" id="navbarSupportedContent">

                          <!-- Right Side Of Navbar -->
                          <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                              <a class="nav-link" href="{{ route('home') }}">{{ __('Beranda') }}</a>
                            </li>
                            <li class="nav-item">
                              <!--<a class="nav-link" href="{{ url('biografi.phtml') }}">{{ __('Tentang Kami') }}</a>-->
                              <a class="nav-link" href="{{ url('henry-indraguna-partners-law-firm.phtml') }}">{{ __('Tentang Kami') }}</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="{{ url('layanan-jasa-hukum.phtml') }}">{{ __('Layanan Jasa Hukum') }}</a>
                            </li>
                            <!--<li class="nav-item">
                              <a class="nav-link" href="{{ url('klien-kami.phtml') }}">{{ __('Klien Kami') }}</a>
                            </li>-->
                            <li class="nav-item">
                              <a class="nav-link" href="{{ route('posts.blog', 'latest-news') }}">{{ __('Berita & Artikel') }}</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="{{ route('videos.index') }}">{{ __('Galeri') }}</a>
                            </li>
                            <!--<li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Galeri
                              </a>
                              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('photos.index') }}">Photos</a>
                                <a class="dropdown-item" href="{{ route('videos.index') }}">Videos</a>
                              </div>
                            </li>-->
                            <!--<li class="nav-item">
                              <a class="nav-link" href="{{ url('testimonial.phtml') }}">{{ __('Testimonial') }}</a>
                            </li>-->

                          </ul>
                          <div class="nav-socialmedia">
                            <a target="_blank" href="https://www.facebook.com/officialhenryindraguna/" class="fa-stack" style="text-decoration:none;">
                              <i class="fa fa-square fa-stack-2x" style="color:#FFFFFF;"></i>
                              <i class="fa fa-facebook fa-stack-1x fa-inverse" style="color:#000000;"></i>
                            </a>
                            <a target="_blank" href="https://twitter.com/henryindraguna2" class="fa-stack" style="text-decoration:none;">
                              <i class="fa fa-square fa-stack-2x" style="color:#FFFFFF;"></i>
                              <i class="fa fa-twitter fa-stack-1x fa-inverse" style="color:#000000;"></i>
                            </a>
                            <a target="_blank" href="https://www.instagram.com/henryindraguna_official/" class="fa-stack" style="text-decoration:none;">
                              <i class="fa fa-square fa-stack-2x" style="color:#FFFFFF;"></i>
                              <i class="fa fa-instagram fa-stack-1x fa-inverse" style="color:#000000;"></i>
                            </a>
                            <a target="_blank" href="https://www.linkedin.com/in/henry-indraguna-channel-661b0a19b/" class="fa-stack" style="text-decoration:none;">
                              <i class="fa fa-square fa-stack-2x" style="color:#FFFFFF;"></i>
                              <i class="fa fa-linkedin fa-stack-1x fa-inverse" style="color:#000000;"></i>
                            </a>
                            <a target="_blank" href="https://www.youtube.com/channel/UCTc9yAARyqdtz2vfzf7FxpQ" class="fa-stack" style="text-decoration:none;">
                              <i class="fa fa-square fa-stack-2x" style="color:#FFFFFF;"></i>
                              <i class="fa fa-youtube fa-stack-1x fa-inverse" style="color:#000000;"></i>
                            </a>
                          </div>
                      </div>
                  </div>
              </nav>

        <main class="">
            @yield('content')
        </main>

        <footer class="py-4" style="background-color:#000000 !important;">
          <div class="container">
            <div class="row">
              <div class="col-md-3 pb-2">
                <img class="img-responsive w-100 mb-4" src="{{ asset('images/logo-henry-indraguna.png') }}" />
                TREASURY TOWER <br />
                9th Floor <br />
                Jl. Jend. Sudirman Kav. 52-53, <br />
                Lot 28 - District 8 SCBD, <br />
                Jakarta, 12190.<br />
                info@henryindraguna.com<br />
                021 50111278 (Hunting)<br />
              </div>
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-5">
                    <h5>Translator</h5>
                    <div id="google_translate_element"></div>

                    <div class="py-4">
                      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15865.100940276097!2d106.8069038!3d-6.2273969!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb4e00e59a970abc9!2sDistrict%208%20SCBD!5e0!3m2!1sid!2sid!4v1605177116811!5m2!1sid!2sid" style="width:100%;min-height:150px;" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>

                  </div>
                  <div class="col-md-7">
                    <ul class="nav-footer d-none d-md-block">
                      <li class="nav-item">
                        <a style="color:#FFFFFF;" class="nav-link" href="{{ route('home') }}">{{ __('Beranda') }}</a>
                      </li>
                      <li class="nav-item">
                        <a style="color:#FFFFFF;" class="nav-link" href="#">{{ __('Tentang Kami') }}</a>
                        <ol>
                          <li><a style="color:#FFFFFF;" href="{{ url('henry-indraguna-partners-law-firm.phtml') }}">Profile HIP LAW FIRM</a></li>
                          <li><a style="color:#FFFFFF;" href="{{ url('visi-dan-misi.phtml') }}">Visi dan Misi</a></li>
                          <li><a style="color:#FFFFFF;" href="{{ url('hip-lawfirm/henry-indraguna.phtml') }}">Profile Henry Indraguna</a></li>
                          <li><a style="color:#FFFFFF;" href="{{ url('tim-kantor-hukum-hip.phtml') }}">Tim Kantor Hukum HIP</a></li>
                        </ol>
                      </li>
                      <li class="nav-item">
                        <a style="color:#FFFFFF;" class="nav-link" href="{{ url('layanan-jasa-hukum.phtml') }}">{{ __('Layanan Jasa Hukum') }}</a>
                      </li>
                      <!--<li class="nav-item">
                        <a style="color:#FFFFFF;" class="nav-link" href="{{ url('klien-kami.phtml') }}">{{ __('Klien Kami') }}</a>
                      </li>-->
                      <li class="nav-item">
                        <a style="color:#FFFFFF;" class="nav-link" href="{{ route('posts.blog', 'latest-news') }}">{{ __('Berita & Artikel') }}</a>
                      </li>
                      <li class="nav-item">
                        <a style="color:#FFFFFF;" class="nav-link" href="#">{{ __('Galeri') }}</a>
                        <ol>
                          <li><a style="color:#FFFFFF;" href="{{ route('videos.index') }}">Video</a></li>
                          <li><a style="color:#FFFFFF;" href="{{ route('photos.index') }}">Foto</a></li>
                        </ol>
                      </li>
                      <!--<li><a style="color:#FFFFFF;" href="{{ route('home') }}">Beranda</a></li>
                      <li><a style="color:#FFFFFF;" href="{{ url('biografi.phtml') }}">Profil</a></li>
                      <li><a style="color:#FFFFFF;" href="{{ route('hip.home') }}">HIP</a></li>
                      <li>Bisnis
                        <ol>
                          <li><a style="color:#FFFFFF;" href="{{ route('hip.home') }}">HIP Law Firm</a></li>
                          <li><a style="color:#FFFFFF;" href="http://autobridal.com/">Auto Bridal Carwash</a></li>
                        </ol>
                      </li>

                      <li><a style="color:#FFFFFF;" href="{{ route('posts.blog', 'latest-news') }}">Berita & Artikel</a></li>
                      <!-''-<li><a style="color:#FFFFFF;" href="{{ route('videos.index') }}">Galeri</a></li>-''->
                      <li>Galeri
                        <ol>
                          <li><a style="color:#FFFFFF;" href="{{ route('photos.index') }}">Photos</a></li>
                          <li><a style="color:#FFFFFF;" href="{{ route('videos.index') }}">Videos</a></li>
                        </ol>
                      </li>
                      <li><a style="color:#FFFFFF;" href="{{ url('our-practice-area.phtml') }}">Layanan Jasa Hukum</a></li>
                      <!--<li>Our Practice Area
                        <ol>
                          <li><a style="color:#FFFFFF;" href="{{ url('our-practice-area.phtml') }}">Corporate Division</a></li>
                          <li><a style="color:#FFFFFF;" href="{{ url('our-practice-area.phtml') }}">Litigation</a></li>
                          <li><a style="color:#FFFFFF;" href="{{ url('our-practice-area.phtml') }}">Business Transactions</a></li>
                          <li><a style="color:#FFFFFF;" href="{{ url('our-practice-area.phtml') }}">Professional Malpractice</a></li>
                          <li><a style="color:#FFFFFF;" href="{{ url('our-practice-area.phtml') }}">Real Estate</a></li>
                          <li><a style="color:#FFFFFF;" href="{{ url('our-practice-area.phtml') }}">Healthcare Law</a></li>
                          <li><a style="color:#FFFFFF;" href="{{ url('our-practice-area.phtml') }}">Employment & Workers Compensation</a></li>
                          <li><a style="color:#FFFFFF;" href="{{ url('our-practice-area.phtml') }}">Truts & Estates</a></li>
                        </ol>
                      </li>-->
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <!-- Begin Mailchimp Signup Form -->
                <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                	#mc_embed_signup{background:#000; clear:left; font:14px Helvetica,Arial,sans-serif; }
                	/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
                	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
                </style>
                <div id="mc_embed_signup" class=" d-none d-md-block">
                <form action="https://franchiseglobal.us17.list-manage.com/subscribe/post?u=710e5bc719e328260b255953a&amp;id=072399f477" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                	<h2 class="mt-0 mb-2">Subscribe</h2>
                <div class="mc-field-group">
                	<label for="mce-EMAIL">Email Address </label>
                	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                </div>
                	<div id="mce-responses" class="clear">
                		<div class="response" id="mce-error-response" style="display:none"></div>
                		<div class="response" id="mce-success-response" style="display:none"></div>
                	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_710e5bc719e328260b255953a_072399f477" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                    </div>
                </form>
                </div>
                <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='BIRTHDAY';ftypes[3]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
                <!--End mc_embed_signup-->

              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <hr color="#eeeeee" />
                <div class="py-4">
                  <h5>Member of:</h5>
                  <img style="width:160px;" class="m-2 pull-left" src="{{ asset('images/logo-hkpi.png') }}" />
                  <img style="width:110px;" class="m-2 pull-left" src="{{ asset('images/logo-ikhapi.png') }}" />
                  <img style="width:80px;" class="m-2 pull-left" src="{{ asset('images/kongres-advokat-indonesia.png') }}" />
                  <img style="width:140px;padding:15px 0px;" class="m-2 pull-left" src="{{ asset('images/logo-peradi.png') }}" />
                  <img style="width:80px;" class="m-2 pull-left" src="{{ asset('images/LOGO-PERKHAPPI.jpg') }}" />
                  <img style="width:110px;" class="m-2 pull-left" src="{{ asset('images/logo-bamni.png') }}" />
                  <!--<img style="width:80px;" class="m-2 pull-left" src="{{ asset('images/pdip-logo.png') }}" />-->
                </div>
              </div>
            </div>
          </div>
        </footer>

        <div class="">
          <div class="container">
            <div class="row py-1">
              <div class="col-md-12 text-center">
                <span>&copy; 2021, Henry Indraguna - All Rights Reserved</span>
              </div>
            </div>
          </div>
        </div>

    </div>

    <a href="https://api.whatsapp.com/send?phone=6281573000000&text=Halo%21%20HIP%20Law%20Firm" class="float" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
    </a>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('assets/bower_components/jquery/dist/jquery.slim.min.js') }}"></script>
    <script>window.jQuery || document.write('<script src="{{ asset("assets/bower_components/jquery/dist/jquery.min.js") }}"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="{{ asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="{{ asset('assets/bower_components/holderjs/holder.min.js') }}"></script>
    <script src="{{ asset('assets/node_modules/sticky-kit/dist/sticky-kit.min.js') }}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{ asset('js/ie10-viewport-bug-workaround.js') }}"></script>


    <script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'id'}, 'google_translate_element');
    }
    </script>

    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

    <script src="{{ asset('js/custom.js') }}"></script>

    <script>
    $(window).scroll(function(){
        var scroll = $(window).scrollTop();
        if(scroll < 10){
            $('.fixed-top').css('background', 'transparent');
        } else{
            $('.fixed-top').css('background', 'rgba(0, 0, 0, 0.8)');
        }
    });
    </script>

    @if(isset($scripts))
      <script src="{{ asset('js/'.$scripts.'.js') }}"></script>
    @endif

</body>
</html>
