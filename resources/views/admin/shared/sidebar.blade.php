<style>
.newsi{
	border-radius: 47%;
    border: 1px solid #865dbd;
    padding: 5px;
    background-color: #865dbd;
    margin-left: 45px;
}
</style>
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
		<li>
			<a><i class="fa fa-home"></i> Home </a>
		</li>
		<li><a><i class="fa fa-edit"></i> Posts <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
			  <li><a href="{{ route('posts.add') }}">Add Post</a></li>
			  <li><a href="{{ route('posts.list') }}">Posts List</a></li>
			  <li><a href="{{ route('categorypost') }}">Post Category</a></li>
			</ul>
		</li>

		<li><a href="{{ route('video.list') }}"><i class="fa fa-youtube" aria-hidden="true"></i> Video</a></li>

		<li><a><i class="fa fa-file-image-o"></i> Gallery <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
			  <li><a href="{{ route('galleries.list') }}">Gallery List</a></li>
			  <li><a href="{{ route('categoryimage') }}">Gallery Category</a></li>
			</ul>
		</li>

		<li><a><i class="fa fa-file-image-o"></i> Photos <span class="fa fa-chevron-down"></span></a>
			<ul class="nav child_menu">
			  <li><a href="{{ route('admin.photos') }}">Photos</a></li>
			  <li><a href="{{ route('admin.photosalbums') }}">Photos Albums</a></li>
			</ul>
		</li>

    </ul>
  </div>

</div>
