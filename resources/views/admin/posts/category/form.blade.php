<div class="form-group">
	<label for="gallery_title">Category Image Name</label>
	{!! Form::text('category_name', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Gallery Name','class' => 'form-control')) !!}
</div>
<div class="form-group">
	<label for="gallery_status">Parent ID</label>
	{{ Form::selectRange('parent_id',null, 10,$category_status,['class' => 'form-control', 'id' => 'category']) }}
</div>
<div class="form-group">
	<label for="gallery_status">Sort Order</label>
	{{ Form::selectRange('sort_order',null, 20,$category_status,['class' => 'form-control', 'id' => 'category']) }}
</div>
<div class="form-group">
	<label for="gallery_status">Category Status</label>
	{{ Form::selectRange('category_status',null, 1,$category_status,['class' => 'form-control', 'id' => 'category']) }}
</div>