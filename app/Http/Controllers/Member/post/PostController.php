<?php

namespace App\Http\Controllers\Member\post;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;

use Auth;
use App\Models\MPost;
use App\Models\MPostCategory;

use App\Classes\StringClass;
use App\Classes\Posts;
use App\Classes\PostCategory;

use DB;
use Carbon;

class PostController extends Controller
{
	public function __construct()
    {
        $this->category     = new PostCategory;
        $this->posts        = new Posts;
    }
	public function index(Request $request)
	{
		$post       		= $this->posts->PostsByuser();
        if($post->count() > 0){
        $titles     		= MPost::first()->post_title;
        }else{
            $titles 		= 0;
        }
        $strings    		= new StringClass();

        if($request->ajax()){
			
			$post			= $this->posts->searchPostByuser($request);
		return view('member.post.search', compact('post','strings'));
		}

        return view('member.post.index', compact('post','strings'));
	}
    public function create()
    {
			
			$datenow 		= Carbon\Carbon::now();
			$datenow->setTimezone('Asia/Jakarta');
			$catpost        = $this->category->GetCatByParentNull(0);
            $catid          = 0;
            $cat			= 0;
            $child			= $this->category;
			return ('ini');
			return view('member.post.add',compact('catpost', 'catid','datenow', 'cat', 'child'));
		
    }
	public function store(Request $request)
    {
        $post			= $this->posts->InsertPost($request);
        $request->session()->flash('alert-success', 'was successful Add!');
        return redirect()->route('listpostmember')->with('success','Article created successfully');
    }
	public function edit(Request $request)
    {
		$id			= $request->id;
        $data       = $this->posts->findPosts($id);
        $catid      = $this->posts->getCategorypost($id);
        $catpost    = $this->category->GetCatByParentNull(0);
        $datenow	= $this->posts->startPublishByid($id);
        $cat		= $catid;
        $child		= $this->category;

        return view('member.post.edit',compact('data','id','catpost','catid','datenow', 'cat', 'child'));
    }
	public function update(Request $request, $id)
    {
        $post				= $this->posts->UpdatePost($request,$id);
        $request->session()->flash('alert-success', 'was successful Update!');
        return redirect()->route('listpostmember')->with('success','Article created successfully');
    }
	public function destroy(Request $request,$id)
    {
		$is_deleted			= 1 ;
		\DB::update('update posts set is_deleted = ? where id = ?',[$is_deleted,$id]);
		
        $request->session()->flash('alert-success', 'was successful delete!');
        return redirect()->route('listpostmember')->with('success','Article created successfully');
    }
}
