<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','status','department_id','is_deleted','social_id','social_provider','avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','status', 'remember_token',
    ];
	public function UserCategories()
	  {
		return $this->hasMany('App\Usercategory','id','department_id');
	  }
	public function Departments()
	  {
		return $this->belongsTo('App\Usercategory','department_id','id');
	  }
    public function Profile()
    {
        return $this->hasOne('App\Models\MUserProfile','id','id_user');
    }
}
