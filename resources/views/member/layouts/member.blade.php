<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>InfoBrand Member</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Our Custom CSS -->
		<link rel="stylesheet" href="{{ URL::asset('css/style2.css') }}" media="all">
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
		<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
<style>
.info a{
    background: #fff!important;
}
.info{
    background: #fff!important;
	width: 19% !important;
}
.footer.active{
	left:0;
	position: fixed;
	bottom: 0;
    width: 100%;
    background-color: #292b2c;
    color: white;
    text-align: center;
    padding: 10px;
}
.footer {
    position: fixed;
    left: 142px;
    bottom: 0;
    width: 100%;
    background-color: #292b2c;
    color: white;
    text-align: center;
    padding: 10px;
}
</style>
</head>
 <body>
	
	
 <div class="wrapper">
	@if(Auth::check())
		@include('member.layouts.sidebar')
		
	@endif
	
    <div id="content">
		@if(Auth::check())
			@include('member.layouts.navbar')
		@endif
		@yield('content')
	@if(Auth::check())
	<footer>
		<div id="sidebarCollapse" class="toggle footer">
			<div class="footer-copyright py-3 text-center">
				© 2018 Copyright:
				<a href="http://infobrand.id/">
					<strong> Tras Mediacom</strong>
				</a>
			</div>
		</div>
	</footer>
	@endif
	</div>    
</div>    



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
$(document).ready(function () {
                $("#sidebar").mCustomScrollbar({
                    theme: "minimal"
                });

                $('#sidebarCollapse').on('click', function () {
                    $('#sidebar, #content, .footer').toggleClass('active');
                    $('.collapse.in').toggleClass('in');
                    $('a[aria-expanded=true]').attr('aria-expanded', 'false');
                    
                });
            });
</script>
    </body>
</html>