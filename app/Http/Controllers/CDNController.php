<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Classes\SimpleImage;
use Config;
use Image;

class CDNController extends Controller
{

    protected $simage;

    public function __construct()
    {
      parent::__construct();
      //$simage = new SimpleImage();
    }

    public function image($path, Request $request)
    {

  		//$path = Config::get('assets.images') . $path;
      $img = Image::make($path);

      //if($img === false) return redirect()->route('cdn', '2018/02/23/dikuartal-1-ponds-age-miracle-kenalkan-tiga-brand-ambassador-baru.jpg?w=250&h=170');

      $w_ori = $img->width();
      $h_ori = $img->height();

      $w_req = $request->input('w') ? $request->input('w') : NULL;
      $h_req = $request->input('h') ? $request->input('h') : NULL;

      if(isset($w_req) || isset($h_req)) {

        if(isset($w_req) && isset($h_req)) {
          if($w_req >= $h_req) {
            $img->resize(NULL, $h_req, function ($constraint) {
              $constraint->aspectRatio();
            });
          } else {
            $img->resize($w_req, NULL, function ($constraint) {
              $constraint->aspectRatio();
            });
          }

          $img->resizeCanvas($w_req, $h_req, 'center', false);
        }

        if(isset($w_req) && !isset($h_req)) {
          $img->resize($w_req, NULL, function ($constraint) {
            $constraint->aspectRatio();
          });
        }

        if(!isset($w_req) && isset($h_req)) {
          $img->resize(NULL, $h_req, function ($constraint) {
            $constraint->aspectRatio();
          });
        }

      }

      $browserCache = 60*60*24*7;

          header('Cache-Control: public, max-age='. $browserCache);
          header('Expires: '.gmdate('D, d M Y H:i:s', time()+$browserCache).' GMT');
          //header('Content-Length: ' . strlen($output));
          //header('Last-Modified: ' . $lastModified);
          //header('ETag: ' . $eTag); ->header('Cache-Control: public, max-age=60*60*24*7')

      return $img->response('jpg', 100);
    }
}
