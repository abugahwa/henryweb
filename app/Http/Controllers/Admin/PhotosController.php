<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\MPhotos;
use App\Models\MPhotosAlbums;
use App\Classes\Photos;

class PhotosController extends Controller
{
  protected $photos;

  public function __construct()
  {
      $this->photos = new Photos;
  }

  public function index()

    {
        $photos       = $this->photos->getAll(['paginate' => 10]);
        $data         = 0;
        /*$CateImg      = $this->photos->catVideoAll();
        $catid				= 0;*/

        return view('admin.photos.index', compact('photos','data', 'CateImg', 'catid'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.photos.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $photos				= $this->photos->InsertVideos($request);
        $photos->save();

		    return response()->json($photos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data1     = $this->photos->getById($id);
        $catid     = $this->photos->GetVideoCategoryByid($id);

        $data = [
            'data1'     => $data1,
            'catid'     => $catid
        ];

		    return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $photos					= $this->photos->update($request, $id);
        $photos->save();

		    return response()->json($video);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
		    $video		 = $this->photos->delete($request, $id);
		    $data		   = array('id' => $id);

        return response()->json($data);
    }


    public function combo_data($id)
	   {
        $catid          				= $this->photos->GetVideoCategoryByid($id);
        $CateImg        				= $this->photos->catVideoAll();
        $json							      = '';

    		if($CateImg->count()){
                foreach($CateImg as $role){
    				      echo '<option value="'.$role->id.'" '.($catid == $role->id ?'selected="selected"':"").'>'.$role->name.'</option>';
                }
    		}
    	}

}
