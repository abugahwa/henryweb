@extends('henryweb.layouts.frontend')

@section('content')

    <div class="bg-navbar"></div>

    <div class="container">
      <div class="row">
        <div class="col-md-2 order-2">
            <?php /*@include('shared.leftColumn')*/ ?>
        </div>

        <div class="col-md-8 order-1 order-md-2">
          <h2>Email sent successfuly!</h2>
          <p>Kami akan menghubungi Anda segera, Terima Kasih</p>
        </div>

        <div class="col-md-2 order-3">
          <div class="sticky_column" data-sticky_column="">
              <?php /* @include('shared.rightColumn') */ ?>
          </div>
        </div>
      </div>
    </div>

@endsection
