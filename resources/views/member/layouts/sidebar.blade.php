<?php
			$userid			=  \Auth::user()->id;
		
					use App\User;
					$user			= User::where('id', $userid)->get();
					foreach($user as $b){
						$image			= $b->avatar;
						$name			= $b->name;
					}
	?>
<nav id="sidebar">
	
	<div class="sidebar-header">
		<img src="{{asset($image)}}" class="img-circle center-block" width="53%">
		<h3 class="text-center">{{ $name }}</h3>
    </div>

    <ul class="list-unstyled components">
		<p><a href="{{ route('memberinfo') }}">InfoBrand</a></p>
        <li clss="active">
            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false"><i class="fa fa-edit"></i> Post</a>
            <ul class="collapse list-unstyled" id="pageSubmenu">
				<li><a href="{{ url('/list-post-member')}}">Post Index</a></li>
                <li><a href="{{ url('/add-post-member')}}">Add Post</a></li>
            </ul>
        </li>
        <li>
            <a href="#report" data-toggle="collapse" aria-expanded="false"><i class="fa fa-edit"></i> Report</a>
            <ul class="collapse list-unstyled" id="report">
				<li><a href="{{ url('/report-viewer')}}">Click</a></li>
            </ul>
        </li>
	</ul>
	<ul class="list-unstyled CTAs">
        <li><a href="http://infobrand.id/" class="article">Back to article</a></li>
    </ul>
</nav>
