<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Posts;
use App\Classes\Videos;
use App\Classes\Photos;

class HomeController extends Controller
{

    protected $posts;
    protected $videos;
    protected $photos;
    protected $postsLeft;
    protected $postsRight;
    protected $postsRight1;

    public function __construct()
    {
        parent::__construct();
        //$this->middleware('auth');
        $this->posts = new Posts;
        $this->videos = new Videos;
        $this->photos = new Photos;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //$postsPromo = $this->posts->getAll(['limit' => 3, 'category' => 2]);
        $postsNews = $this->posts->getAll(['limit' => 2, 'category' => array(1,3,4,5)]);
        $videolist = $this->videos->getAll(['limit' => 1]);
        //$videolist = $this->videos->getAll(['limit' => 1, 'sortby' => 'rand']);
        /*$postTentang = $this->posts->getById(7);
        $postTentang = $postTentang['post_text']; // Biografi
        $postTentang = substr($postTentang,0, strpos($postTentang, "</p>")+4);
        $postTentang = str_replace("<p>", "", str_replace("<p/>", "", $postTentang));
        */
        $scripts = 'home/index';
        return view('henryweb.index', compact('postsNews', 'videolist'));
  //      return view('frontend-onelayout', compact('slides', 'postsCenter', 'postsProfessional', 'posts500brandchampions', 'businessEconomy', 'brandHeadline', 'brandUpdate', 'postsPopular', 'videolist', 'photolist', 'scripts'))->with(['postsLeft'=>$this->postsLeft, 'postsRight'=>$this->postsRight]);
    }

    /**
     * Show the application contact.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        $scripts = 'home/index';

        return view('contact', compact('scripts'));
    }
}
