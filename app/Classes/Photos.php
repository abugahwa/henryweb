<?php

namespace App\Classes;

use Illuminate\Http\Request;

use App\Models\MPhotos;

class Photos extends BaseClass {

	public function getById($id) {
		return MPhotos::findOrFail($id);
	}

	public function getByCategoryId($id) {
		return MPhotos::where('album_id', '=', $id)->get();
	}

	public function getAll($params) {
		$res = MPhotos::where('id', 'DESC');
//		$res = Photos::where('is_deleted', 0)->orderBy('id', 'DESC');
		if(isset($params['limit'])) {
		    $res->limit($params['limit']);
		}
		if(isset($params['query'])) {
		    $res->where('title', 'LIKE', '%'.$params['query'].'%')->orWhere('description', 'LIKE', '%'.$params['query'].'%');
		}
		if(isset($params['sortby'])) {
			if($params['sortby'] == "hits") {
	    	$res->orderBy('hits', 'DESC');
			} elseif($params['sortby'] == "group") {
			   $res->groupBy('album_id')->orderBy('album_id', 'DESC');
 			} elseif($params['sortby'] == "rand") {
 	    	$res->orderBy('album_id', 'DESC')->inRandomOrder();
			} else {
	    	$res->orderBy('id', 'DESC');
			}
		} else {
    	$res->orderBy('id', 'DESC');
		}
		if(isset($params['paginate'])) {
	    	return $res->paginate($params['paginate']);
		} else {
	    	return $res->get();
		}
	}

	public function create($data)
	{
		$photos 									= new MPhotos;
    $photos->title		        = $data->title;
    $photos->album_id        	= $data->album_id;
    $photos->is_active	      = isset($data->is_active) ? 1 : 0;
    $photos->description     	= $data->description;
    $photos->save();

		return [true, 'Photo has been created!'];
	}

	public function update($data)
	{
		$photos 									= $this->getById($data->id);
    $photos->title		        = $data->title;
    $photos->video_id       	= $data->video_id;
    $photos->is_active	      = isset($data->is_active) ? 1 : 0;
    $photos->description     	= $data->description;
    $photos->update();

		return [true, 'Photo has been updated!'];
	}

	public function addHits($id) {
		return MPhotos::whereId($id)->increment('hits');
	}

}
