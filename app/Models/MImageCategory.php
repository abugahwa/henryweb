<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MImageCategory extends Model
{
    protected $table    = 'image_category';
    protected $fillabel = ['id', 'category_image', 'status'];
}
