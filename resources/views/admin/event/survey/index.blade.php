@extends('layouts.admin')
@section('content')
<style>
</style>
	<div class="" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>Posts</h3>
            </div>
			
        </div>
		
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>List Posts </h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Settings 1</a></li>
									<li><a href="#">Settings 2</a></li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					
					<div class="x_content">
						<div class="flash-message">
							@foreach (['danger', 'warning', 'success', 'info'] as $msg)
							  @if(Session::has('alert-' . $msg))

							  <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
							  @endif
							@endforeach
						  </div> 
						<div class="col-md-1">
						<a href="{{ route('add_survey') }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Add</a>
						</div>
						{!! Form::open([ 'class' => 'form-horizontal form-label-left', 'id' => 'PostIndex','method' => 'get']) !!}
						{{ csrf_field() }}
						<div class="col-md-5">
								<div class="form-group">
									{!! Form::select('post_category', $category, null, ['id' => 'category_brand','class' => 'form-control', 'placeholder' => 'Brand Category']) !!}
								</div>
								
						</div>
						<div class="col-md-6">
							<div class="input-group">
							  <input type="text" class="form-control" name="title" placeholder="Search for...">
							  <span class="input-group-btn">
								<button class="btn btn-default" type="submit" id="search_post">Go!</button>
							  </span>
							</div>
						</div>
						{!! Form::close() !!}
						<div style="clear:both;"></div>
						<div class="load-bar"></div>
						<div id="postnya">
						<div class="loader"></div>
							@include('admin.event.survey.table')
						</div>
                    




					</div>
				</div>
			</div>
		</div>
	</div>
<script src="{{ asset('assets/bower_components/gentelella/vendors/jquery/dist/jquery.min.js') }}"></script>
<script>
$('.loader').hide();
$(document).ready(function () {
        $("#search_post").click(function() {
		var offerForm = $("#PostIndex"); 
		 offerForm.submit(function(e){
			 e.preventDefault();
			var formData = offerForm.serialize();
				$.ajax({
					url:"{{ route('posts.list') }}",
					type:'get',
					dataType: "html",
					data:formData,
					beforeSend:function () {
						$(".loader").show();
					},					
					success:function (data) {
						$("#postnya").html(data);
						$(".loader").fadeOut();
					}
				});
        });
        });
    });

$(function() {
		$('#postnya').on('click', '.pagination a', function(e) {
			e.preventDefault();
			$(".loader").show();

			var url = $(this).attr('href');  
			getArticles(url);
			window.history.pushState("", "", url);
			return false;
		});

	function getArticles(url) {
		$(".loader").show();
		$.ajax({
			url : url  
		}).done(function (data) {
			$('#postnya').html(data);  
			$(".loader").fadeOut();
		}).fail(function () {
			alert('Articles could not be loaded.');
		});
	}
});
</script>
@endsection

