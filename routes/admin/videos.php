<?php

Route::get('video-list', 'Admin\VideosController@index')->name('video.list');
Route::get('video-add', 'Admin\VideosController@create')->name('video.add');
Route::post('video-save', 'Admin\VideosController@store')->name('video.save');
Route::post('video-update', 'Admin\VideosController@update')->name('video.update');
Route::get('video-edit/{id}', 'Admin\VideosController@edit')->name('video.edit');
Route::post('video-update/{id}', 'Admin\VideosController@update')->name('video.update');
Route::get('video-delete/{id}', 'Admin\VideosController@destroy')->name('video.delete');
Route::get('get-video/{id}', 'Admin\VideosController@combo_data');
