<?php
echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">

<channel>
	<atom:link href="https://infobrand.id/posts/rss" rel="self" type="application/rss+xml" />
	<title>InfoBrand.id News Update</title>
	<link>https://infobrand.id/</link>
	<description>10 Berita dan Artikel terbaru</description>
	<language>id-ID</language>
	<generator>InfoBrand.id RSS Generator</generator>

<?php foreach($postsBlog as $post) { ?>
<item>
  <title><?php echo $post['post_title']; ?></title>
  <link><?php echo url($post['post_alias'].".phtml"); ?></link>
  <guid><?php echo url($post['post_alias'].".phtml"); ?></guid>
  <description>{{ str_limit(strip_tags($post['post_text']), $limit = 150, $end = '...') }}</description>
  <pubDate><?php echo $post['start_publishing']; ?></pubDate>
</item>
<?php } ?>

</channel>
</rss>
