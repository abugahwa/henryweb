<ul id="tree1">
	@foreach($event as $category)
	<a href="{{ route('event.edit', $category->id) }}" style="margin-left: 10px; margin-top: 10px;"><i class="fa fa-pencil"></i></a>
	<li>
	<i class="fa fa-plus-square"></i> {{ $category->menu_name }}
		@if($nm_menu->GetByID($category->id)->count() > 0)
		<ul>
			@foreach($nm_menu->GetByID($category->id) as $child)
				<li>
					{{ $child->menu_name }} <a href="{{ route('event.edit', $child->id) }}" style="margin-left: 10px;"><i class="fa fa-pencil"></i></a>
				</li>
			@endforeach
		</ul>
		@endif
	</li>
	@endforeach
</ul>
