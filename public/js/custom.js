$(document).ready(function(){

    var window_width = $( window ).width();

    if (window_width < 768) {
      $(".sticky_column").trigger("sticky_kit:detach");
    } else {
      make_sticky();
    }

    $( window ).resize(function() {

      window_width = $( window ).width();

      if (window_width < 768) {
        $(".sticky_column").trigger("sticky_kit:detach");
      } else {
        make_sticky();
      }

    });

    function make_sticky() {
      $(".sticky_column").stick_in_parent();
    }

    setTimeout(function () {
            make_sticky();
        }, 20000);

});
