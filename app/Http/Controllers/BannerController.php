<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Models\MBanners;
use App\Models\MBannerCategory;

use App\Classes\StringClass;

use DB;
use Carbon;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner     = MBanners::paginate(10);

        return view('admin.banner.list_banner', compact('banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $b_status       = 0;
        $b_sort_order   = 0;
        $catid          = 0;
        $catbanner           = MBannerCategory::all();
        return view('admin.banner.add_banner', compact('catbanner','b_status','b_sort_order','catbanner','catid'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $strings                        = new StringClass();
        $banner_name                    = $strings->str2alias($request->b_name);

        $now                            = \Carbon\Carbon::now();
		    $year                           = date('Y', strtotime($now));
		    $month                          = date('m', strtotime($now));
        $days                           = date('d', strtotime($now));

        $bs                             = $request->file('b_image')->getClientOriginalExtension();
        $nombreCarpeta                  = preg_replace('/\s+/', '.', $year . "/" . $month . "/" . $days);
        $fileimg                        = $banner_name . '.' .$bs;
        $b_image                        = 'img/banner/'.$nombreCarpeta .'/' .$fileimg;
        $path                           = base_path() .'/public/img/banner/'.$nombreCarpeta;

        $banner                         = new MBanners;
        $banner->brand_id               = 0;
        $banner->b_image                = $b_image;
        $banner->b_link                 = $request->b_link;
        $banner->b_name                 = $request->b_name;
        $banner->b_category             = $request->b_category;
        $banner->b_status               = $request->b_status;
        $banner->b_sort_order           = $request->b_sort_order;
        $banner->b_start_publishing     = $request->b_start_publishing;
        $banner->b_finish_publishing    = $request->b_finish_publishing;
        $banner->user_id                = $request->id_user;

        $banner->save();

        $imageName                      = $strings->str2alias($banner->b_name);
        $nmImg                          = strtolower($imageName) . '.' .

        $request->file('b_image')->getClientOriginalExtension();
        $request->file('b_image')->move($path, $nmImg);

        $request->session()->flash('alert-success', 'was successful Add!');

        return redirect()->route('banner.list')->with('success','Article created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = MBanners::find($id);
        $catid          = MBanners::where('id', $id)->first()->b_category;
        $b_status       = MBanners::where('id', $id)->first()->b_status;
        $b_sort_order   = MBanners::where('id', $id)->first()->b_sort_order;
        $catbanner       = MBannerCategory::all();
        return view('admin.banner.edit_banner', compact('data','id','catbanner','b_status','b_sort_order','catid'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $strings                = new StringClass();
        $b_name                 = $strings->str2alias($request->b_name);

        $b_title             = strtolower($b_name);

        $banner                         = MBanners::find($id);
        $banner->brand_id               = 0;

        if($request->hasFile('b_image')) {
          $now                  = \Carbon\Carbon::now();
  		    $year                 = date('Y', strtotime($now));
  		    $month                = date('m', strtotime($now));
          $days                 = date('d', strtotime($now));

          $bs                   = $request->file('b_image')->getClientOriginalExtension();
          $nombreCarpeta        = preg_replace('/\s+/', '.', $year . "/" . $month . "/" . $days);
          $fileimg              = $b_title . '.' .$bs;
          $b_image              = 'img/banner/'.$nombreCarpeta .'/' .$fileimg;
          $path                 = base_path() .'/public/img/banner/'.$nombreCarpeta;
          $banner->b_image      = $b_image;

          $imageName = $strings->str2alias($banner->b_name);
          $nmImg      = strtolower($imageName) . '.' .
          $request->file('b_image')->getClientOriginalExtension();
          $request->file('b_image')->move($path, $nmImg);

        } else {
          //$banner->b_image                = NULL;
        }

        $banner->b_link                 = $request->b_link;
        $banner->b_name                 = $request->b_name;
        $banner->b_category             = $request->b_category;
        $banner->b_status               = $request->b_status;
        $banner->b_sort_order           = $request->b_sort_order;
        $banner->b_start_publishing     = $request->b_start_publishing;
        $banner->b_finish_publishing    = $request->b_finish_publishing;
        $banner->user_id                = $request->id_user;

        $banner->save();
        $request->session()->flash('alert-success', 'was successful Update!');
        return redirect()->route('banner.list')->with('success','Banner updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $banner                 = MBanners::FindOrFail($id);
        $gettitle               = MBanners::where('id', $id)->first()->b_name;
        $getlinkimg             = MBanners::where('id', $id)->first()->b_image;
        $banner->delete();
        unlink(public_path($getlinkimg));
        $request->session()->flash('alert-success', 'was successful delete!');
        return redirect()->route('posts.list')->with('success','Article created successfully');
    }
}
