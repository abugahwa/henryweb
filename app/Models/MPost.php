<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MPost extends Model
{
    protected $table 	= 'posts';
	  protected $fillabel = ['id','post_title','post_alias','post_lead_in','post_type','post_category','post_img','post_img_caption','post_text','post_file','post_date_added','post_last_modified','post_status','post_author','post_hits','post_tags','post_lang','brand_related','start_publishing','finish_publishing','comment_status'];

    public function PostCategories()
	  {
		    return $this->belongsTo('App\Models\MPostCategory','post_category','id');
	  }

    public function author()
    {
        $res = $this->hasOne('App\Models\MUsers', 'id', 'post_author');
        return $res;
    }
}
