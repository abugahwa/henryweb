<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MScore extends Model
{
    protected $table        = 'table_score';
    protected $fillable     = ['id', 'merek', 'score', 'cat_score_id', 'created_at', 'update_at'];
}
