          <h2 class="category_title">{{ strtoupper('Brand Update') }}</h2>
          <?php
          $n = 0;
          $banner_position = 200;
          foreach($postsRight as $post) { ?>
          <div class="media">
            <div class="media-left">
                <img class="media-object mo-small" src="<?php echo route('cdn', $post['post_img'] . "?w=120&h=120"); ?>" alt="<?php echo $post['post_title']; ?>" />
            </div>
            <div class="media-body mb-small">
                <p><a href="<?php echo url($post['post_alias'].".phtml"); ?>"><?php echo $post['post_title']; ?></a></p>
            </div>
          </div>
          <hr />

          <?php
            $n++;
    				if(($n % 3) === 0) {
    				  echo $banners->getByCatId($banner_position);
              $banner_position++;
    				}
          ?>

          <?php } ?>

          <a href="{{ route('posts.blog', 'brand-update') }}" class="pull-right ahref">Index <i class="fa fa-angle-double-right"></i></a>

          <div class="clearfix"></div>
<!--
          <div class="sticky_column" data-sticky_column="">
            <h2 class="category_title">{{ strtoupper('Government Update') }}</h2>
            <?php
            foreach($postsRight2 as $post) { ?>
              <div class="media">
                <div class="media-left">
                  <img class="media-object mo-small" src="<?php echo route('cdn', $post['post_img'] . "?w=120&h=120"); ?>" alt="<?php echo $post['post_title']; ?>" />
                </div>
                <div class="media-body mb-small">
                  <p><a href="<?php echo url($post['post_alias'].".phtml"); ?>"><?php echo $post['post_title']; ?></a></p>
                </div>
              </div>
              <hr />
            <?php } ?>

            <a href="{{ route('posts.blog', 'government-update') }}" class="pull-right ahref">Index <i class="fa fa-angle-double-right"></i></a>
            <div class="clearfix"></div>
          </div>
            -->