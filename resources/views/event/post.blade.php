@extends('layouts.event')
@section('content')
<style>
#post_terkait {
    background: #F1F1F1;
    border: 1px solid #E1E1E1;
    margin: 40px 0;
    padding-top: 15px;
    padding-bottom: 15px;

}
#post_terkait h3{
  color: #fff;
  font-size: 14pt;
  padding-left: 23px;
  background-color: #CD2027;
  width: 172px;
  margin-top: -29px;
  padding-top: 8px;
  padding-bottom: 8px;
  margin-left: 39px;
}
#post_terkait ul{
    list-style: none;
}
#post_terkait li {
  padding-left: 1.3em;
}
#post_terkait li:before {
  content: "\f00c"; /* FontAwesome Unicode */
  font-family: FontAwesome;
  display: inline-block;
  margin-left: -1.3em; /* same as padding-left set on li */
  width: 1.3em; /* same as padding-left set on li */
}
</style>
<link href="{{ URL::asset('css/event/post.css') }}" rel="stylesheet">
<div class="container">
    <div class="row">
        <div class="col-md-8">
          <br />
            @foreach($post as $show)
              <h2>{{ $show->post_title }}</h2>
              <hr />
                <div class="card-sosial">
                  <a class="resp-sharing-button__link" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo Request::url(); ?>&t=<?php echo $show['post_img']; ?>&t=<?php echo $show['post_title']; ?>');" target="_blank" aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z"/></svg>
                      </div>
                    </div>
                  </a>

                  <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet?text=<?php echo $show['post_title'] . " - infobrand.id"; ?>&source=infobrand&related=infobrand&via=infobrand&url=<?php echo Request::url(); ?>" target="_blank" aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z"/></svg>
                      </div>
                    </div>
                  </a>

                  <a class="resp-sharing-button__link" href="https://plus.google.com/share?url=<?php echo Request::url(); ?>&hl=en-US/');" src="{{ asset($show->post_img)}}" target="_blank" aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--google resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11.37 12.93c-.73-.52-1.4-1.27-1.4-1.5 0-.43.03-.63.98-1.37 1.23-.97 1.9-2.23 1.9-3.57 0-1.22-.36-2.3-1-3.05h.5c.1 0 .2-.04.28-.1l1.36-.98c.16-.12.23-.34.17-.54-.07-.2-.25-.33-.46-.33H7.6c-.66 0-1.34.12-2 .35-2.23.76-3.78 2.66-3.78 4.6 0 2.76 2.13 4.85 5 4.9-.07.23-.1.45-.1.66 0 .43.1.83.33 1.22h-.08c-2.72 0-5.17 1.34-6.1 3.32-.25.52-.37 1.04-.37 1.56 0 .5.13.98.38 1.44.6 1.04 1.84 1.86 3.55 2.28.87.23 1.82.34 2.8.34.88 0 1.7-.1 2.5-.34 2.4-.7 3.97-2.48 3.97-4.54 0-1.97-.63-3.15-2.33-4.35zm-7.7 4.5c0-1.42 1.8-2.68 3.9-2.68h.05c.45 0 .9.07 1.3.2l.42.28c.96.66 1.6 1.1 1.77 1.8.05.16.07.33.07.5 0 1.8-1.33 2.7-3.96 2.7-1.98 0-3.54-1.23-3.54-2.8zM5.54 3.9c.33-.38.75-.58 1.23-.58h.05c1.35.05 2.64 1.55 2.88 3.35.14 1.02-.08 1.97-.6 2.55-.32.37-.74.56-1.23.56h-.03c-1.32-.04-2.63-1.6-2.87-3.4-.13-1 .08-1.92.58-2.5zM23.5 9.5h-3v-3h-2v3h-3v2h3v3h2v-3h3"/></svg>
                      </div>
                    </div>
                  </a>
                  <a class="resp-sharing-button__link" href="whatsapp://send?text=<?php echo Request::url(); ?>" data-text="<?php echo $show['post_title']; ?>" data-images="<?php echo $show['post_img']; ?>" target="_blank" aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.1 3.9C17.9 1.7 15 .5 12 .5 5.8.5.7 5.6.7 11.9c0 2 .5 3.9 1.5 5.6L.6 23.4l6-1.6c1.6.9 3.5 1.3 5.4 1.3 6.3 0 11.4-5.1 11.4-11.4-.1-2.8-1.2-5.7-3.3-7.8zM12 21.4c-1.7 0-3.3-.5-4.8-1.3l-.4-.2-3.5 1 1-3.4L4 17c-1-1.5-1.4-3.2-1.4-5.1 0-5.2 4.2-9.4 9.4-9.4 2.5 0 4.9 1 6.7 2.8 1.8 1.8 2.8 4.2 2.8 6.7-.1 5.2-4.3 9.4-9.5 9.4zm5.1-7.1c-.3-.1-1.7-.9-1.9-1-.3-.1-.5-.1-.7.1-.2.3-.8 1-.9 1.1-.2.2-.3.2-.6.1s-1.2-.5-2.3-1.4c-.9-.8-1.4-1.7-1.6-2-.2-.3 0-.5.1-.6s.3-.3.4-.5c.2-.1.3-.3.4-.5.1-.2 0-.4 0-.5C10 9 9.3 7.6 9 7c-.1-.4-.4-.3-.5-.3h-.6s-.4.1-.7.3c-.3.3-1 1-1 2.4s1 2.8 1.1 3c.1.2 2 3.1 4.9 4.3.7.3 1.2.5 1.6.6.7.2 1.3.2 1.8.1.6-.1 1.7-.7 1.9-1.3.2-.7.2-1.2.2-1.3-.1-.3-.3-.4-.6-.5z"/></svg>
                      </div>
                    </div>
                  </a>

                </div>
                <img src="<?php echo route('cdn', $show['post_img'] ); ?>" class="img-responsive center-block" alt="{{ $show->post_img_caption }}" width="100%">
                <span class="bridge">{{ $show->post_img_caption }}</span>
                <div class="row">
                    <div class="col-md-1">
                      <div class="sticky_column card-sosial_desktop" data-sticky_column="">

                          <a class="resp-sharing-button__link" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo Request::url(); ?>&t=<?php echo $show['post_img']; ?>&t=<?php echo $show['post_title']; ?>');" target="_blank" aria-label="">
                            <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z"/></svg>
                              </div>
                            </div>
                          </a>
                          <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet?text=<?php echo $show['post_title'] . " - infobrand.id"; ?>&source=infobrand&related=infobrand&via=infobrand&url=<?php echo Request::url(); ?>" target="_blank" aria-label="">
                            <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z"/></svg>
                              </div>
                            </div>
                          </a>
                          <a class="resp-sharing-button__link" href="https://plus.google.com/share?url=<?php echo Request::url(); ?>&hl=en-US/');" src="{{ asset($show->post_img)}}" target="_blank" aria-label="">
                            <div class="resp-sharing-button resp-sharing-button--google resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11.37 12.93c-.73-.52-1.4-1.27-1.4-1.5 0-.43.03-.63.98-1.37 1.23-.97 1.9-2.23 1.9-3.57 0-1.22-.36-2.3-1-3.05h.5c.1 0 .2-.04.28-.1l1.36-.98c.16-.12.23-.34.17-.54-.07-.2-.25-.33-.46-.33H7.6c-.66 0-1.34.12-2 .35-2.23.76-3.78 2.66-3.78 4.6 0 2.76 2.13 4.85 5 4.9-.07.23-.1.45-.1.66 0 .43.1.83.33 1.22h-.08c-2.72 0-5.17 1.34-6.1 3.32-.25.52-.37 1.04-.37 1.56 0 .5.13.98.38 1.44.6 1.04 1.84 1.86 3.55 2.28.87.23 1.82.34 2.8.34.88 0 1.7-.1 2.5-.34 2.4-.7 3.97-2.48 3.97-4.54 0-1.97-.63-3.15-2.33-4.35zm-7.7 4.5c0-1.42 1.8-2.68 3.9-2.68h.05c.45 0 .9.07 1.3.2l.42.28c.96.66 1.6 1.1 1.77 1.8.05.16.07.33.07.5 0 1.8-1.33 2.7-3.96 2.7-1.98 0-3.54-1.23-3.54-2.8zM5.54 3.9c.33-.38.75-.58 1.23-.58h.05c1.35.05 2.64 1.55 2.88 3.35.14 1.02-.08 1.97-.6 2.55-.32.37-.74.56-1.23.56h-.03c-1.32-.04-2.63-1.6-2.87-3.4-.13-1 .08-1.92.58-2.5zM23.5 9.5h-3v-3h-2v3h-3v2h3v3h2v-3h3"/></svg>
                              </div>
                            </div>
                          </a>
                      </div>
                    </div>
                    <div class="col-md-11">
                      <p>
                        {!! $show->post_text !!}
                      </p>
                    
                      <br />
                      @endforeach
                    </div>
                </div>
                Tags : <?php foreach(explode(',', $show->post_tags) as $tag) { ?>
                        <span class="badge badge-secondary"><?php echo ucwords($tag); ?></span>
                        <?php } ?>
                      </span>
                <div style="clear: both;"></div>
                <!-- mobile sosmed -->
                <div class="card-sosial_mobile">
                  <a class="resp-sharing-button__link" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo Request::url(); ?>&t=<?php echo $show['post_img']; ?>&t=<?php echo $show['post_title']; ?>');" target="_blank" aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z"/></svg>
                      </div>
                    </div>
                  </a>

                  <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet?text=<?php echo $show['post_title'] . " - infobrand.id"; ?>&source=infobrand&related=infobrand&via=infobrand&url=<?php echo Request::url(); ?>" target="_blank" aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z"/></svg>
                      </div>
                    </div>
                  </a>

                  <a class="resp-sharing-button__link" href="https://plus.google.com/share?url=<?php echo Request::url(); ?>&hl=en-US/');" src="{{ asset($show->post_img)}}" target="_blank" aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--google resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11.37 12.93c-.73-.52-1.4-1.27-1.4-1.5 0-.43.03-.63.98-1.37 1.23-.97 1.9-2.23 1.9-3.57 0-1.22-.36-2.3-1-3.05h.5c.1 0 .2-.04.28-.1l1.36-.98c.16-.12.23-.34.17-.54-.07-.2-.25-.33-.46-.33H7.6c-.66 0-1.34.12-2 .35-2.23.76-3.78 2.66-3.78 4.6 0 2.76 2.13 4.85 5 4.9-.07.23-.1.45-.1.66 0 .43.1.83.33 1.22h-.08c-2.72 0-5.17 1.34-6.1 3.32-.25.52-.37 1.04-.37 1.56 0 .5.13.98.38 1.44.6 1.04 1.84 1.86 3.55 2.28.87.23 1.82.34 2.8.34.88 0 1.7-.1 2.5-.34 2.4-.7 3.97-2.48 3.97-4.54 0-1.97-.63-3.15-2.33-4.35zm-7.7 4.5c0-1.42 1.8-2.68 3.9-2.68h.05c.45 0 .9.07 1.3.2l.42.28c.96.66 1.6 1.1 1.77 1.8.05.16.07.33.07.5 0 1.8-1.33 2.7-3.96 2.7-1.98 0-3.54-1.23-3.54-2.8zM5.54 3.9c.33-.38.75-.58 1.23-.58h.05c1.35.05 2.64 1.55 2.88 3.35.14 1.02-.08 1.97-.6 2.55-.32.37-.74.56-1.23.56h-.03c-1.32-.04-2.63-1.6-2.87-3.4-.13-1 .08-1.92.58-2.5zM23.5 9.5h-3v-3h-2v3h-3v2h3v3h2v-3h3"/></svg>
                      </div>
                    </div>
                  </a>
                  <a class="resp-sharing-button__link" href="whatsapp://send?text=<?php echo Request::url(); ?>" data-text="<?php echo $show['post_title']; ?>" data-images="<?php echo $show['post_img']; ?>" target="_blank" aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M20.1 3.9C17.9 1.7 15 .5 12 .5 5.8.5.7 5.6.7 11.9c0 2 .5 3.9 1.5 5.6L.6 23.4l6-1.6c1.6.9 3.5 1.3 5.4 1.3 6.3 0 11.4-5.1 11.4-11.4-.1-2.8-1.2-5.7-3.3-7.8zM12 21.4c-1.7 0-3.3-.5-4.8-1.3l-.4-.2-3.5 1 1-3.4L4 17c-1-1.5-1.4-3.2-1.4-5.1 0-5.2 4.2-9.4 9.4-9.4 2.5 0 4.9 1 6.7 2.8 1.8 1.8 2.8 4.2 2.8 6.7-.1 5.2-4.3 9.4-9.5 9.4zm5.1-7.1c-.3-.1-1.7-.9-1.9-1-.3-.1-.5-.1-.7.1-.2.3-.8 1-.9 1.1-.2.2-.3.2-.6.1s-1.2-.5-2.3-1.4c-.9-.8-1.4-1.7-1.6-2-.2-.3 0-.5.1-.6s.3-.3.4-.5c.2-.1.3-.3.4-.5.1-.2 0-.4 0-.5C10 9 9.3 7.6 9 7c-.1-.4-.4-.3-.5-.3h-.6s-.4.1-.7.3c-.3.3-1 1-1 2.4s1 2.8 1.1 3c.1.2 2 3.1 4.9 4.3.7.3 1.2.5 1.6.6.7.2 1.3.2 1.8.1.6-.1 1.7-.7 1.9-1.3.2-.7.2-1.2.2-1.3-.1-.3-.3-.4-.6-.5z"/></svg>
                      </div>
                    </div>
                  </a>

                </div>
                <!-- end mobile sosmed -->
            <div style="clear:both; margin: 30px;"></div>

        </div>
        <div class="col-md-4">
          <div class="sticky_column" data-sticky_column="">
          <div class="row">
            <?php
              if($alias == 'anugerah-brand-indonesia'){
                  $css_post_name_cat  = 'card_cat_gories_abi';
              }
              if ($alias == 'indonesia-digital-popular-brand-award' || $alias == 'indonesia-top-digital-public-relation-award') {
                  $css_post_name_cat  = 'card_cat_gories_idpba';
              }
              if ($alias == 'pertama-di-indonesia') {
                $css_post_name_cat  = 'card_cat_gories_perdi';
              }
              if ($alias == 'mobile-application-choice-award') {
                $css_post_name_cat  = 'card_cat_gories_imac';
              }
             ?>
                <div class="col-md-12">
                  <br />
                  <div class="{{$css_post_name_cat}}">
                    <h5 class="card-title">{{ ucwords($categories_name) }} <a href="{{ route('postcategories', [$alias,$categories_name1])}}"><i class="fas fa-plus float-right"></i></a></h5>
                  </div>
                    @foreach($categories as $cat)
                    <div class="lib-panel">
                            <div class="row box-shadow">
                                <div class="col-md-6">
                                    <img class="lib-img" src="<?php echo route('cdn', $cat['post_img'] ); ?>">
                                </div>
                                <div class="col-md-6">
                                    <div class="lib-row lib-header">
                                    {!! str_limit(strip_tags($cat->post_text), $limit = 25, $end = '...') !!}
                                        <div class="lib-header-seperator"></div>
                                    </div>
                                    <div class="lib-row lib-desc">
                                    <a href="{{ route('detailPost', [ $alias, $cat->category_alias, $cat->post_alias] ) }}">{!! str_limit(strip_tags($cat->post_text), $limit = 80, $end = '...') !!}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                  </div>

            <div class="col-md-12">
                <h4 class="float-left">Most Popular</h4>
                <div class="form-group pull-right" id="pagination_achievment">
                <a href="#carouselExample" data-slide="prev"><i class="fas fa-chevron-circle-left"></i></a>
                <a href="#carouselExample" data-slide="next"><i class="fas fa-chevron-circle-right"></i></a>
            </div>
            <div style="clear:both;"></div>
            <div class="list_div"></div>
              <div style="clear:both;margin: 10px;"></div>

              <div class="row">
                <div id="carouselExample" class="carousel slide" data-ride="carousel" data-interval="9000">
                    <div class="carousel-inner row w-100 mx-auto" role="listbox">
                      <?php $n = 0;?>
                      @foreach($cat_id as $idCat)
                        @foreach($postPopuler->GetPosByHitstByCategory($idCat->id) as $achiev)
                        <div class="carousel-item col-md-12  <?php if($n == 0) echo "active"; $n++; ?>">
                            <div class="card">
                                <a class="card_achiev" href="{{ route('detailPost', [ $alias, $achiev->category_alias, $cat->post_alias] ) }}">
                                  @if(route('cdn', $achiev['post_img'] ) < 1)
                                    <img class="card-img-top" src="<?php echo route('cdn', $achiev['post_img'] ); ?>" alt="{{ $achiev->post_im_caption }}">
                                  @else
                                    <img class="img-fluid mx-auto d-block" src="//via.placeholder.com/600x400?text=infobrand" alt="{{ $achiev->post_im_caption }}">
                                  @endif
                                </a>
                                <div class="card-body">
                                    <p class="card-text"><a href="{{ route('detailPost', [ $alias, $categories_name1, $cat->post_alias] ) }}">{!! str_limit(strip_tags($achiev->post_text), $limit = 80, $end = '...') !!}</a></p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                      @endforeach
                    </div>

                </div>
              </div>
            </div>
          </div>
          </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Article",
  "headline": "{{ $tags->post_title }}",
  "image": {
    "@type": "ImageObject",
    "url": "<?php echo route('cdn', $achiev['post_img'] ); ?>",
    "width": 250,
    "height": 250
  },
  "datePublished": "{{ $tags->created_at }}",
  "dateModified": "{{ $tags->created_at }}",
  "author": {
    "@type": "Person",
    "name": "{{ $tags->author->name }}"
  },
  "publisher": {
    "@type": "Organization",
    "name": "infobrand",
    "logo": {
      "@type": "ImageObject",
      "url": "http://infobrand.id/img/icons/favicon.ico",
      "width": 50,
      "height": 50
    }
  },
      "mainEntityOfPage": {
         "@type": "WebPage",
         "@id": "http://infobrand.id/"
      }
}
</script>
<script>
$(document).ready(function () {
				$.ajax({
					url:"{{ route('detailPost', [$alias,$category_link, $post_alias]) }}",
					type:'get',
					dataType: "html",
					beforeSend:function () {
						$(".loader").show();
					},
					success:function (data) {

						$("#post_terkait").html(data);
					}
				});
    });
</script>
@endsection
