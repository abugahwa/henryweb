@extends('layouts.admin')
@section('content')
<style>
.stylish-input-group .input-group-addon{
    background: white !important; 
	padding: 4px;
}
.stylish-input-group .form-control{
	border-right:0; 
	box-shadow:0 0 0; 
	border-color:#ccc;
}
.stylish-input-group button{
    border:0;
    background:transparent;
}
#LoadingDulu {
		position: fixed;
		top:0px;
		width: 100%;
		z-index: 1;
	}
#LoadingContent {
		height: 30px;
		margin: auto;
		width: 180px;
		background: #ff005e;
		text-align: center;
		line-height: 29px;
		font-weight: bold;
		color: #fff;
	}
</style>
<div id="LoadingDulu"></div>
<div class="" role="main">
	<div class="page-title">
		<div class="title_left">
			<h3>Report Article</h3>
		</div>
			
    </div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2>List Posts </h2>
					<ul class="nav navbar-right panel_toolbox">
						<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Settings 1</a></li>
								<li><a href="#">Settings 2</a></li>
							</ul>
						</li>
						<li><a class="close-link"><i class="fa fa-close"></i></a></li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					{!! Form::open([ 'class' => 'form-horizontal form-label-left', 'id' => 'report', 'method' => 'get']) !!}
					{{ csrf_field() }}
					<div class="form-group">
						<div class="col-md-5">
							<input type="text" id="start_publishing" name="start" placeholder="Start Date" class="form-control">
						</div>
						<div class="col-md-1">
							<div class="text-center">s/d</div>
						</div>
						<div class="col-md-5">
							<div class="input-group stylish-input-group">
								<input type="text" class="form-control" name="end" id="finish_publishing" placeholder="End Date" >
								<span class="input-group-addon">
									<button type="submit" id="report_offer">
										<span class="glyphicon glyphicon-search"></span>
									</button>  
								</span>
							</div>
						</div>
					</div>
					{!! Form::close() !!}
					<div class="clearfix"></div>
					<br />
					<div id="t_id" class="table-responsive">
						<table class="table table-striped">
							<thead bgcolor="#9853bb" style="color: #fff;">
								<tr>
									<th>No.</th>
									<th>Title</th>
									<th>Category</th>
									<th>Viewer</th>
									<th>Date</th>
								</tr>
							</thead>
							<tbody>
								@foreach($report as  $key => $b)
								<tr>
									<td>{{ $report->firstItem() + $key }}</td>
									<td>{{ $b->post_title }}</td>
									<td>{{ $b->PostCategories->category_name }}</td>
									<td>{{ $b->post_hits }}</td>
									<td>{{ Carbon\Carbon::parse($b->created_at)->format('D M Y i H:i:s') }}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@if($report->count() === 0)
							<div class="alert alert-danger">
								No data found.
							</div>
						@endif
						{!! $report->links() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{{ asset('assets/bower_components/gentelella/vendors/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/bower_components/gentelella/vendors/moment/min/moment.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
    $('#start_publishing').datetimepicker({
        format: 'YYYY-MM-DD'
    }); 
    $('#finish_publishing').datetimepicker({
        format: 'YYYY-MM-DD'
    });



$(document).ready(function () {
        $("#report_offer").click(function() {
		var offerForm = $("#report"); 
		 offerForm.submit(function(e){
			 e.preventDefault();
			var formData = offerForm.serialize();
				$.ajax({
					url:"{{ route('search.report') }}",
					type:'GET',
					dataType: "html",
					data:formData,
					beforeSend:function () {
						$("#LoadingDulu").html("<div id='LoadingContent'><i class='fa fa-spinner fa-spin'></i> Mohon tunggu ....</div>");
						$("#LoadingDulu").show();
					},					
					success:function (data) {
						//$("table").html(data);
						$("#t_id").html(data);
						$("#LoadingDulu").fadeOut();
					}
				});
        });
        });
    });
</script>
@endsection