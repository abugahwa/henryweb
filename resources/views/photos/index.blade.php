@extends('henryweb.layouts.frontend')
@section('pageTitle', 'Albums Henry Indraguna')
@section('content')

    <div class="bg-navbar"></div>

    <div class="home-videos pb-4">
    <div class="container">
      <div class="row py-4">
        <div class="col-md-12 text-center">
          <h1>Albums Henry Indraguna</h1>
          <!--<h5>Pengusaha, Pengacara dan Nasionalis Sejati</h5>-->
        </div>
      </div>
      <div class="row">
    @foreach ($photos as $p)
        <div class="col-md-4 py-3 photos">
          <img style="width:100%;height:250px;object-fit: cover;" class="img-fluid mb-2" src="{{ url($p->thumbnail) }}" />
          <h5><a style="color:#000000;" href="{{ route('photos.view', $p->id) }}">{{ $p->title }}</a></h5>
        </div>
    @endforeach
      </div>
      <nav>{{ $photos->links("pagination::bootstrap-4") }}</nav>
    </div>
    </div>
@endsection
