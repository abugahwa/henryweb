<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\MMenuEvents;
use App\Models\MEvents;
use App\Models\EventSurver;

use App\Classes\Event;
use App\Classes\StringClass;

use DB;
use Carbon;

class EventController extends Controller
{
	public function __construct()
    {
		$this->events			= new Event;
	}
    public function create()
	{
		$categories 			= $this->events->byParentMenu(0);
		$cat					= 0;
		$child					= $this->events;
		return view('admin.event.menu.add', compact('categories', 'cat','dropdown', 'child'));
	}
	public function store(Request $request)
	{
		$event					= $this->events->InsertMenuEvent($request);

		$request->session()->flash('alert-success', 'was successful Add!');
        return redirect()->route('event.list');
	}
	public function MenuEvent(Request $request)
	{
		$event							= MMenuEvents::where('parent', '=', 0)->get();
		$allCategories 			= MMenuEvents::pluck('menu_name','id')->all();
		$categories 				= $this->events->PluckEventParent(0);
		$nm_menu						= $this->events;

		return view('admin.event.menu.index', compact('event', 'categories', 'nm_menu'));
	}
	public function edit($id)
	{
		$event					= $this->events->findEventByID($id);
		$categories 			= $this->events->byParentMenu(0);
		$cat					= $id;
		$child					= $this->events;

		return view('admin.event.menu.edit', compact('event','categories', 'cat', 'child'));
	}
	public function update(Request $request, $id)
	{
		$event					= $this->events->updateEventMenu($request,$id);
		$request->session()->flash('alert-success', 'was successful Update!');
        return redirect()->route('event.list');
	}
	public function serachTable(Request $request)
	{
		$cat						= $request->cat;

		$event						= $this->events->SearchMenuEvent($cat);

		return view('admin.event.menu.table_event', compact('event'));
	}
	public function EventIndex(Request $request)
	{
		//$event						= $this->events->EventPaginate();
		$event						=  $categories = MMenuEvents::where('parent', '=', 0)->get();
		$allCategories = MMenuEvents::pluck('menu_name','id')->all();
		$categories 				= $this->events->PluckEventParent(0);
		$nm_menu					= $this->events;
		$text						= 0;

		return view('admin.event.index', compact('event', 'categories', 'nm_menu', 'text'));
	}
	public function createEvent()
	{
		$categories 			= $this->events->byParentMenu(0);
		$cat					= 0;
		$child					= $this->events;

		return view('admin.event.add', compact('categories', 'child', 'cat'));
	}
	public function storeEvent(Request $request)
	{
		$event					= $this->events->Insertevents($request);

		$request->session()->flash('alert-success', 'was successful Update!');
        return redirect()->route('event.list2');
	}
	public function ajaxText($id)
	{
		$text					= $this->events->GetTexts($id);

		return $text;
	}
	public function editEvent($id)
	{
		$id_event				= MEvents::where('menu_id', $id)->first()->id;
		$event					= MEvents::find($id_event);
		$categories 			= $this->events->byParentMenu(0);
		//$getMenuId				= MEvents::where('id', $id)->first()->menu_id;
		$cat					= $id;
		$child					= $this->events;
		//return json_encode($event);
		return view('admin.event.edit', compact('event','categories', 'cat', 'child'));
	}
	public function updateEvent(Request $request, $id)
	{
		$event					= $this->events->updateEvents($request,$id);

		$request->session()->flash('alert-success', 'was successful Update!');
        return redirect()->route('event.list2');
	}
	public function serachTableEvent(Request $request)
	{
		$cat					= $request->cat;


		return view('admin.event.menu.table_event', compact('event'))->render();
	}
	public function create_survey()
	{
		$datenow 		= Carbon\Carbon::now();
		$datenow->setTimezone('Asia/Jakarta');
		$fase			= 0;

		return view("admin.event.survey.add", compact('datenow', 'fase'));
	}
	public function storeSurvey(Request $request)
	{
		$survey					= new EventSurver;

		$survey->event_name		= $request->event_name;
		$survey->fase			= $request->fase;
		$survey->event_date		= $request->event_date;
		$survey->survey_results	= $request->survey_results;

		$survey->save();

		$request->session()->flash('alert-success', 'was successful Add!');
        return redirect()->route('surveylist');
	}
	public function IndexSurvey(Request $request)
	{
		$survey					= EventSurver::paginate(10);
		$category				= EventSurver::pluck('event_name', 'id');
		if($request->ajax()){
			return view('admin.event.survey.table', compact('survey', 'category'))->render();
		}
		return view('admin.event.survey.index', compact('survey', 'category'));
	}
	public function EditSurvey($id)
	{
		$suervey				= EventSurver::find($id);
		$datenow 		= $suervey->event_date;
		$fase						= $suervey->fase;

		return view('admin.event.survey.edit', compact('suervey', 'datenow', 'fase'));
	}
	public function Updatesurvey(Request $request,$id)
	{
		$survey					= EventSurver::find($id);
		$survey->event_name		= $request->event_name;
		$survey->fase			= $request->fase;
		$survey->event_date		= $request->event_date;
		$survey->survey_results	= $request->survey_results;

		$survey->save();

		$request->session()->flash('alert-success', 'was successful Add!');
        return redirect()->route('surveylist');

	}
}
