    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
					{{ method_field('post') }}
						{!! Form::model($gallery, ['method' => 'POST', 'files' => true,'class' => 'form-horizontal ', 'enctype' => 'multipart/form-data','id' => 'edit_gallery'])  !!}
						{{ csrf_field() }}
							{!! Form::hidden('id', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Slide Title','class' => 'form-control','id' => 'id')) !!}
						<input type="hidden" name="gallery_file">
						<div class="form-group"> 
							<label for="inputEmail3" class="col-sm-3 control-label">Gallery Name</label> 
							<div class="col-sm-9"> 
								{!! Form::text('gallery_title', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Slide Title','class' => 'form-control','id' => 'ket_gambar')) !!}
							</div> 
						</div> 
						<div class="form-group"> 
							<label for="inputPassword3" class="col-sm-3 control-label">Gallery Caption</label> 
							<div class="col-sm-9"> 
								{!! Form::text('gallery_caption', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Gallery Caption','class' => 'form-control','id' => 'link')) !!}
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-sm-3 control-label">Category Gallery</label>
							<div class="col-sm-9">
								<select id="category" name="category_image" class="form-control"></select>
							</div>
						</div>
						<div class="form-group">
							<label for="b_image" class="col-sm-3 control-label">Image</label>
							<div class="col-sm-9"> 
								<input type="file" name="b_image" id="gambar">
							</div>				
						</div>						
						
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success add" id="btnSave">
                            <span id="" class='glyphicon glyphicon-check'></span> Edit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
				{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
<script>
	/* edit gallery */
	
	function edit_data(id)
	{
		$('#edit_gallery')[0].reset(); 
		$('.form-group').removeClass('has-error'); 
		$('.help-block').empty(); 

		$.ajax({
			url : '{{ url("/edit-gallery")}}/' + id,
			type: "get",
			dataType: "JSON",
			success: function(data)
			{
				/*
				tinymce.get('post_text').setContent(data.post_text);
				*/
				$('[name="id"]').val(data.id);

				$('[name="gallery_title"]').val(data.gallery_title);
				$('[name="gallery_caption"]').val(data.gallery_caption);
				$('[name="text"]').val(data.text);
				$('[name="gallery_file"]').val(data.gallery_file);
				
				$('#editModal').modal('show'); 
				$('.modal-title').text('Edit Data'); 
				 $('#btnSave').text('Edit');
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error get data from ajax');
			}
		
		});	
			$.get('get-galleries/'+id,function(data){	
				$('#category').html(data);
			});
			$.get('get-type-gallery/'+id,function(data){	
				$('#type').html(data);
			});
	}
	
$("form#edit_gallery").submit(function(event){
		event.preventDefault();
		var id	= $("#id").val();
		$('#btnSave').text('Update'); //change button text
		$('#btnSave').text('update..'); //change button text
		$('#btnSave').attr('disabled',true); //set button disable

		var formData = new FormData($(this)[0]);
		  $.ajax({
			url: "{{ url('/update-gallery') }}/"+id,
			type: 'POST',
			data: formData,
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			dataType: "JSON",
			 success: function(data)
				{
					toastr.success('Successfully Update Image!', 'Success Alert', {timeOut: 5000});
					$('.item' + data.id).replaceWith('<div class="thumb_gal item'+data.id+'"><a class="thumbnail fancybox" rel="ligthbox" href="../public/' + data.gallery_file + '"><img class="img-responsive" alt="" src="../public/' + data.gallery_file + '" /><div class="text-center"><label class="text-muted">'+ data.gallery_title +'</label></div></a><button class="show-modal btn btn-success" data-id="' + data.id + '" data-title="'+ data.gallery_title + '" data-content="' + data.gallery_title + '"><span class="glyphicon glyphicon-eye-open"></span> Show</button> <button class="edit_data btn btn-info" data-id="' + data.id + '" data-title="' + data.gallery_title + '" data-content="' + data.gallery_caption + '"><span class="glyphicon glyphicon-edit"></span> Edit</button> <button class="delete-modal btn btn-danger" data-id="' + data.id + '" data-title="' + data.gallery_title + ' "data-content="' + data.gallery_title + '"><span class="glyphicon glyphicon-trash"></span> Delete</button></div>');
					$('#editModal').modal('toggle');

					$('#btnSave').text('save'); //change button text
					$('#btnSave').attr('disabled',false); //set button enable 


				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Error adding / update data');
					$('#btnSave').text('save'); //change button text
					$('#btnSave').attr('disabled',false); //set button enable 

				}
		  });
	 
	  return false;
	});
</script>