@extends('iamadmin.layouts.dashboard')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Posts</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Posts</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">@if(!isset($data)) {{'Add Post'}} @else {{'Edit Post'}} @endif</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
      </div>
      <div class="card-body">

@if(isset($data))
  {!! Form::model($data, ['method' => 'POST','route' => ['admin.posts.update'],'files' => true, 'class' => 'form-horizontal'])  !!}
  <input type="hidden" name="id" value="{{ $data->id }}">
@else
  {!! Form::open(['method' => 'POST','route' => ['admin.posts.store'],'files' => true, 'class' => 'form-horizontal'])  !!}
@endif
<input type="hidden" name="id_user" value="<?php echo \Auth::user()->id;?>">

<div class="col-md-6">
	<div class="form-group{{ $errors->has('post_title') ? ' has-error' : '' }}">
		<label for="post_title">Title</label>

			{!! Form::text('post_title', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Title Post','class' => 'form-control')) !!}
			@if ($errors->has('post_title'))
				<span class="help-block">
					<strong>{{ $errors->first('post_title') }}</strong>
				</span>
			@endif
	</div>
	<div class="form-group{{ $errors->has('post_lead_in') ? ' has-error' : '' }}">
		<label for="post_lead_in">Lead in</label>
		{!! Form::text('post_lead_in', null, array('autofocus' => 'autofocus','placeholder' => 'Lead In','class' => 'form-control')) !!}
		@if ($errors->has('post_lead_in'))
    		<span class="help-block">
    			<strong>{{ $errors->first('post_lead_in') }}</strong>
    		</span>
		@endif
	</div>
	<div class="form-group{{ $errors->has('post_img') ? ' has-error' : '' }}">
		<label for="post_img">Image</label>
		{!! Form::file('post_img', null) !!}
			@if ($errors->has('post_img'))
				<span class="help-block">
					<strong>{{ $errors->first('post_img') }}</strong>
				</span>
			@endif
	</div>
	<div class="form-group{{ $errors->has('post_img_caption') ? ' has-error' : '' }}">
		<label for="post_img_caption">Image Caption</label>
			{!! Form::text('post_img_caption', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Image Caption','class' => 'form-control')) !!}
			@if ($errors->has('post_img_caption'))
				<span class="help-block">
					<strong>{{ $errors->first('post_img_caption') }}</strong>
				</span>
			@endif
	</div>

	<div class="form-group{{ $errors->has('post_category') ? ' has-error' : '' }}">
		<label>Category</label>
		<select class="form-control" name="post_category">
			<option value="0">Select</option>
			@if($catpost->count())
				@foreach($catpost as $role)

					<option value="{{ $role->id }}" {{ $cat == $role->id ? 'selected="selected"' : '' }}><strong>{{ $role->category_name }}</strong></option>
					@foreach($child->GetCatByParentNull($role->id) as $categorys)
						<option class="dropdown-item" value="{{ $categorys->id }}" {{ $cat == $categorys->id ? 'selected="selected"' : '' }}><strong>&nbsp;&nbsp;{{ $categorys->category_name }}</strong></option>
					@endforeach
				@endforeach
			@endif
		</select>
    @if ($errors->has('post_category'))
      <span class="help-block">
        <strong>{{ $errors->first('post_category') }}</strong>
      </span>
    @endif
	</div>

	 <div class="form-group{{ $errors->has('start_publishing') ? ' has-error' : '' }}">
		<label for="start_publishing">Start Publishing</label>
			{!! Form::text('start_publishing', $datenow, ['id' => 'start_publishing','class' => 'form-control', 'placeholder' => 'Please Select']) !!}
			@if ($errors->has('start_publishing'))
				<span class="help-block">
					<strong>{{ $errors->first('start_publishing') }}</strong>
				</span>
			@endif
	</div>
	<!--
	<div class="form-group{{ $errors->has('finish_publishing') ? ' has-error' : '' }}">
		<label for="finish_publishing">Finnish Publishing</label>
			{!! Form::text('finish_publishing', null, ['id' => 'finish_publishing','class' => 'form-control', 'placeholder' => 'Please Select']) !!}
			@if ($errors->has('finish_publishing'))
				<span class="help-block">
					<strong>{{ $errors->first('finish_publishing') }}</strong>
				</span>
			@endif
	</div>
	-->
	<div class="form-group{{ $errors->has('post_tags') ? ' has-error' : '' }}">
		<label for="post_tags">Post Tags</label>
			{!! Form::text('post_tags', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Post Tags','class' => 'form-control')) !!}
			@if ($errors->has('post_tags'))
				<span class="help-block">
					<strong>{{ $errors->first('post_tags') }}</strong>
				</span>
			@endif
	</div>
</div>
<div class="col-md-6">
	{{ Form::textarea('post_text', null, ['class' => 'field','id' => 'ckeditor-el']) }}
</div>
<div style="clear:both;"></div>
<div class="ln_solid"></div>
	<div class="form-group py-3">
		<div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
			<!--<a class="btn btn-primary" href="{{ url()->previous() }}">Cancel</a>-->
      <button type="submit" class="btn btn-lg btn-success">Submit</button>
    </div>
	</div>

{!! Form::close() !!}
</div>
<!-- /.card-body -->
</div>
<!-- /.card -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

  @endsection
