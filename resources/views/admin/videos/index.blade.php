@extends('layouts.admin')
@section('content')

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Video Gallery <small>List</small></h2>
                    <a href="{{ route('admin.videos.add') }}">
                    <button type="button" class="btn btn-primary text-right" style="float:right;" title="Add">
                      <span class="fa fa-plus"></span> Add Video
                    </button>
                    </a>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <p class="text-muted font-13 m-b-30">

                    </p>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No.</th>
                          <th>Title</th>
                          <th>Video ID</th>
                          <th>Description</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php $n=1; foreach ($videos as $vid) { ?>
                        <tr>
                          <td><?php echo $n++; ?></td>
                          <td><?php echo $vid->title; ?></td>
                          <td><?php echo $vid->video_id; ?></td>
                          <td>{{ str_limit(strip_tags($vid->description), $limit = 50, $end = '...') }}</td>
                          <td><?php echo $vid->status(); ?></td>
                          <td>
                            <a href="{{ route('admin.videos.edit', $vid->id) }}">
                            <button type="button" class="btn btn-default" title="Edit">
                              <span class="fa fa-edit"></span>
                            </button>
                            </a>
                            <button type="button" class="btn btn-danger" title="Delete">
                              <span class="fa fa-trash"></span>
                            </button>
                          </td>
                        </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>

@endsection
