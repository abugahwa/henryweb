<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMenuEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('menu_name');
            $table->string('menu_alias');
            $table->integer('parent');
            $table->integer('status');
            $table->string('logo_menu')->nullable();
            $table->string('slide')->nullable();
            $table->string('description');
            $table->integer('sort_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_events');
    }
}
