@extends('layouts.admin')
@section('content')

	<div class="" role="main">
		<div class="page-title">
			<div class="title_left">
					<h3>Edit Event</h3>
			</div>
			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search for...">
						<span class="input-group-btn">
						  <button class="btn btn-default" type="button">Go!</button>
						</span>
                  </div>
                </div>
			</div>
        </div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<div class="row">
							<div class="col-md-6">
								<div class="btn-group btn-breadcrumb">
									<a href="{{ url('/') }}" class="btn btn-default"><i class="glyphicon glyphicon-home"></i></a>
									<a href="{{ url()->previous() }}" class="btn btn-default">Event</a>
									<a href="#" class="btn btn-default">Edit Event</a>
								</div>
							</div>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#">Settings 1</a></li>
										<li><a href="#">Settings 2</a></li>
									</ul>
								</li>
								<li><a class="close-link"><i class="fa fa-close"></i></a></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="x_content">
						{!! Form::model($suervey, ['method' => 'POST','route' => ['updatesurveylist', $suervey->id], 'class' => 'form-horizontal ', 'enctype' => 'multipart/form-data'])  !!}
							<input type="hidden" name="id_user" value="<?php echo \Auth::user()->id;?>">
							@include('admin.event.survey.form')
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection
