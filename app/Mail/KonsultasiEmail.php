<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class KonsultasiEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
      $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

      return $this->from('henryindragunachannel@gmail.com')
                  ->view('henryweb.email.konsultasi')
                  ->with(
                   [
                       'name' => $this->data['name'],
                       'email' => $this->data['email'],
                       'telp' => $this->data['telp'],
                       'jobs' => $this->data['jobs'],
                       'address' => $this->data['address'],
                       'case' => $this->data['case'],
                       'need_lawyers' => ($this->data['advokat'] == 1 ? 'Ya' : 'Tidak'),
                       'question' => $this->data['question'],
                   ]);
    }
}
