@extends('layouts.frontend')
@section('pageTitle', $categoryName)
@section('content')


    <div class="container">
      <a target="_blank" href="https://api.whatsapp.com/send?phone=6285759269117&text=Hai, saya mau ORDER buku katalog INFOBRAND 2019!">
        <img style="width:100%" src="{{asset('assets/images/banner-sales-katalog-infobrand-2019.jpg') }}" alt="INFOBRAND.ID" />
      </a>
      <div class="row">
        <div class="col-md-3 order-2">

        @include('shared.leftColumn')

        </div>

        <div class="col-md-6 order-1 order-md-2">
            <br />
          <h2>Category: <?php echo $categoryName; ?></h2>
          <hr />
          @if($postByalias->count() < 1)
          <div class="alert alert-danger" role="alert">
            No data found
          </div>
          @else
          <?php foreach($postByalias as $post) { ?>
          <div class="media">
            <div class="media-left">
                <img class="media-object" src="<?php echo route('cdn', $post['post_img'] . "?w=120&h=120"); ?>" alt="<?php echo $post['post_title']; ?>" width="120" height="120" />
            </div>
            <div class="media-body">
                <p><a href="<?php echo url($post['post_alias'].".phtml"); ?>"><?php echo $post['post_title']; ?></a><br />
                <small>{{ str_limit(strip_tags($post['post_text']), $limit = 150, $end = '...') }}</small></p>
            </div>
          </div>
          <?php } ?>
          <hr />
          <nav>{{ $postByalias->links("pagination::bootstrap-4") }}</nav>
          @endif
        </div>
        <div class="col-md-3 order-3">
          <div class="sticky_column" data-sticky_column="">
            @include('shared.rightColumn')
          </div>
        </div>
      </div>
    </div>

@endsection
