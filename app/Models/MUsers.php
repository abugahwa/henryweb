<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MUsers extends Model
{
    protected $table 	= 'users';
    protected $fillable = [
        'name', 'email', 'password','status','department_id','is_deleted','social_id','social_provider','avatar',
    ];
	
	public function Department()
    {
        return $this->belongsTo('App\Usercategory','department_id','id');
    }
    public function Profile()
    {
        return $this->hasOne('App\Models\MUserProfile','id','id_user');
    }
}
