@extends('layouts.admin')
@section('content')
	<div class="" role="main">
		<div class="page-title">
			<div class="title_left">
				<h3>List Banner</h3>
            </div>
			<div class="title_right">
				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="{{ url('/query/') }}" method="GET" role="search">
					<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
						<div class="input-group">
							<input type="text" class="form-control" name="search" placeholder="Search for...">
							<span class="input-group-btn">
							  <button class="btn btn-default" type="submit">Go!</button>
							</span>
					  </div>
					</div>
				</form>
			</div>
        </div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2>List Banner </h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Settings 1</a></li>
									<li><a href="#">Settings 2</a></li>
								</ul>
							</li>
							<li><a class="close-link"><i class="fa fa-close"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>

					<div class="x_content">
						<div class="flash-message">
							@foreach (['danger', 'warning', 'success', 'info'] as $msg)
							  @if(Session::has('alert-' . $msg))

							  <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
							  @endif
							@endforeach
						  </div>
						<a href="{{ route('banner.add') }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span> Add</a>
						<div class="table-responsive">
    						<table class="table table-striped">
    							<thead bgcolor="#9853bb" style="color: #fff;">
    								<tr>
    									<th>Banner Name</th>
    									<th>Image</th>
    									<th>Category Banner</th>
    									<th>Publish</th>
    									<th>End Publish</th>
    									<th>Action</th>
    								</tr>
    							</thead>
    							<tbody>
    								@foreach($banner as $b)
    								<tr>
    									<td>{{ $b->b_name }}</td>
    									<td><img src="<?php echo asset( $b->b_image);?>" width="200"></td>
    									<td></td>
    									<td>{{ Carbon\Carbon::parse($b->b_start_publishing)->format('D M Y i H:i:s') }}</td>
    									<td>{{ Carbon\Carbon::parse($b->b_finish_publishing)->format('D M Y i H:i:s') }}</td>
    									<td><a class="btn-sm btn-success" href="{{ route('banner.edit',$b->id) }}" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
    									<a href="{{ route('banner.delete', $b->id) }}" class="btn-sm btn-danger" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="Delete Article" onclick="return confirm('Apakah Anda yakin menghapus post ini?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
    								</tr>
    								@endforeach
    							</tbody>
    						</table>
    					</div>
						@if($banner->count() === 0)
							<div class="alert alert-danger">
								No data found.
							</div>
						@endif
					{!! $banner->render() !!}

					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
