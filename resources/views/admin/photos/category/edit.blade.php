    <div id="editModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
					{{ method_field('post') }}
						{!! Form::model($cats, ['method' => 'POST','class' => 'form-horizontal ','id' => 'edit_catgallery'])  !!}
						{{ csrf_field() }}
							{!! Form::hidden('id', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Slide Title','class' => 'form-control','id' => 'id')) !!}
						@include('admin.gallery.category.form')			
						
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success add" id="EditBtn">
                            <span id="" class='glyphicon glyphicon-check'></span> Edit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
				{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
<script>
	/* edit gallery */
	
	function edit_data(id)
	{
		$('#edit_catgallery')[0].reset(); 

		$.ajax({
			url : '{{ url("/edit-categories-gallery")}}/' + id,
			type: "get",
			dataType: "JSON",
			success: function(data)
			{
				$('[name="id"]').val(data.data.id);
				$('[name="category_image"]').val(data.data.category_image);
				$('[name="gallery_status"]').val(data.data.status);
				
				$('#editModal').modal('show'); 
				$('.modal-title').text('Edit Data'); 
				 $('#btnSave').text('Edit');
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error get data from ajax');
			}
		
		});	
	}
	
$("form#edit_catgallery").submit(function(event){
		event.preventDefault();
		var id	= $("#id").val();
		$('#EditBtn').text('Update'); //change button text
		$('#EditBtn').attr('disabled',true); //set button disable

		var formData = new FormData($(this)[0]);
		  $.ajax({
			url: "{{ url('/update-categories-gallery') }}/"+id,
			type: 'POST',
			data: formData,
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			dataType: "JSON",
			 success: function(data)
				{
					toastr.success('Successfully Update Image!', 'Success Alert', {timeOut: 5000});
					$('.item' + data.id).replaceWith('<tr class="item'+data.id+'"><td>'+data.category_image+'</td><td><a class="btn-sm btn-success" href="javascript:void(0)" onclick="edit_data('+data.id+')" title="Edit"><i class="fa fa-pencil-square-o"></i></a> <a href="javascript:void(0)" class="btn-sm btn-danger" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="Delete Article" onclick="delete_data('+data.id+')"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>  </tr>');
					$('#editModal').modal('toggle');

					$('#EditBtn').text('Update'); //change button text
					$('#EditBtn').attr('disabled',false); //set button enable 


				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Error adding / update data');
					$('#EditBtn').text('Update'); //change button text
					$('#EditBtn').attr('disabled',false); //set button enable 

				}
		  });
	 
	  return false;
	});
</script>