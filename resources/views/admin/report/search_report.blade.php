<div id="t_id" class="table-responsive">

	<table class="table table-striped">
		<thead bgcolor="#9853bb" style="color: #fff;">
			<tr>
				<th>No.</th>
				<th>Title</th>
				<th>Category</th>
				<th>Viewer</th>
				<th>Date</th>
			</tr>
		</thead>
		<tbody>
		@foreach($report as  $key => $b)
			<tr>
				<td>{{ $report->firstItem() + $key }}</td>
				<td>{{ $b->post_title }}</td>
				<td>{{ $b->PostCategories->category_name }}</td>
				<td>{{ $b->post_hits }}</td>
				<td>{{ Carbon\Carbon::parse($b->created_at)->format('D M Y i H:i:s') }}</td>
			</tr>
		@endforeach
		</tbody>
	</table>
	<div class="clearfix"></div>
	@if($report->count() === 0)
	<div class="alert alert-danger">
		No data found.
	</div>
	@endif
		
{{ $report->appends(['start' => $mulai,'end' => $akhir])->links() }}
<div class="clearfix"></div>
		{!! Form::open([ 'route'  => ['print.report'], 'class' => 'form-horizontal form-label-left', 'method' => 'post']) !!}
		{{ csrf_field() }}
		<input type="hidden" name="mulai" value="{{ $mulai }}">
		<input type="hidden" name="akhir" value="{{ $akhir }}">
		<button type="submit" class="btn btn-sm btn-info pull-right"><i class="fa fa-print" aria-hidden="true"></i> Print</button>
		{!! Form::close() !!}
</div>
<script>
$(function() {
		$('#t_id').on('click', '.pagination a', function(e) {
			e.preventDefault();

			$("#LoadingDulu").html("<div id='LoadingContent'><i class='fa fa-spinner fa-spin'></i> Mohon tunggu ....</div>");
			$("#LoadingDulu").show();

			var url = $(this).attr('href');  
			getArticles(url);
			window.history.pushState("", "", url);
		});

		function getArticles(url) {
			$.ajax({
				url : url  
			}).done(function (data) {
				$('#t_id').html(data); 
				$("#LoadingDulu").fadeOut();
			}).fail(function () {
				alert('Articles could not be loaded.');
			});
		}
	});	
</script>

		