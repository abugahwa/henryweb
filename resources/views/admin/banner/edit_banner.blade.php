@extends('layouts.admin')
@section('content')

	<div class="" role="main">
		<div class="page-title">
			<div class="title_left">
					<h3>Edit Companny</h3>
			</div>
			<div class="title_right">
				<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search for...">
						<span class="input-group-btn">
						  <button class="btn btn-default" type="button">Go!</button>
						</span>
                  </div>
                </div>
			</div>
        </div>
		
		<div class="clearfix"></div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<div class="row">
							<div class="col-md-6">
								<div class="btn-group btn-breadcrumb">
									<a href="{{ url('/') }}" class="btn btn-default"><i class="glyphicon glyphicon-home"></i></a>
									<a href="{{ url()->previous() }}" class="btn btn-default">Banner</a>
									<a href="#" class="btn btn-default">Edit Banner</a>
								</div>
							</div>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#">Settings 1</a></li>
										<li><a href="#">Settings 2</a></li>
									</ul>
								</li>
								<li><a class="close-link"><i class="fa fa-close"></i></a></li>
							</ul>
						</div>
						<div class="clearfix"></div>
					</div>
					
					<div class="x_content">
						{!! Form::model($data, ['method' => 'POST','route' => ['banner.update', $data->id], 'class' => 'form-horizontal ', 'enctype' => 'multipart/form-data'])  !!}
							<input type="hidden" name="id_user" value="<?php echo \Auth::user()->id;?>">
						    <input type="hidden" name="post_img" value="<?php echo $data->post_img; ?>" />
							@include('admin.banner.form_banner')
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="{{ asset('assets/bower_components/gentelella/vendors/jquery/dist/jquery.min.js') }}"></script>
	
	<script src="{{ asset('assets/bower_components/gentelella/vendors/moment/min/moment.min.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
	    


<script type="text/javascript">
    $('#start_publishing').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    }); 
    $('#finish_publishing').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
</script>  
	<script type="text/javascript" src="https://www.franchiseglobal.com/ghw-panel/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
	/*
		tinymce.init({
			selector: "#textareaku", width: "100%",
			plugins: [
				"advlist autolink lists link image charmap print preview hr anchor pagebreak",
				"searchreplace wordcount visualblocks visualchars code fullscreen",
				"insertdatetime media nonbreaking save table contextmenu directionality",
				"emoticons template paste textcolor responsivefilemanager"
			],
			toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
			toolbar2: "fontsizeselect fontselect styleselect | preview media | forecolor backcolor pagebreak",
			image_advtab: true,
			relative_urls: false,
			
			 external_filemanager_path:"{!! str_finish(asset('/filemanager'),'/') !!}",
              filemanager_title        :"Responsive File Manager" , // bisa diganti terserah anda
              external_plugins         : { "filemanager" : "{{ asset('/filemanager/plugin.min.js') }}"} 
               }); 
			*/
		
		</script>
@endsection


