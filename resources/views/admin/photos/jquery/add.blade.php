<div id="addModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
				{{ method_field('post') }}
						{!! Form::open([ 'class' => 'form-horizontal','method' => 'POST','files' => true, 'id' => 'offer']) !!}
						{{ csrf_field() }}
						
						<div class="col-md-6">
							<div class="form-group{{ $errors->has('category_image') ? ' has-error' : '' }}">
								<label for="category_image">Category Gallery</label>
								<select class="form-control m-bot15" name="category_image">
								@if($CateImg->count())
									@foreach($CateImg as $role)
										<option value="{{ $role->id }}" {{ $catid == $role->id ? 'selected="selected"' : '' }}>{{ $role->category_image }}</option>    
									@endforeach
								@endif

								</select>
									@if ($errors->has('category_image'))
										<span class="help-block">
											<strong>{{ $errors->first('category_image') }}</strong>
										</span>
									@endif
							</div>
							
							<div class="form-group{{ $errors->has('gallery_title') ? ' has-error' : '' }}">
								<label for="gallery_title">Gallery Name</label>
								
									{!! Form::text('gallery_title', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Gallery Name','class' => 'form-control')) !!}
									@if ($errors->has('gallery_title'))
										<span class="help-block">
											<strong>{{ $errors->first('gallery_title') }}</strong>
										</span>
									@endif
							</div>
							<div class="form-group{{ $errors->has('gallery_caption') ? ' has-error' : '' }}">
								<label for="gallery_caption">Gallery Caption</label>
								
									{!! Form::text('gallery_caption', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Gallery Caption','class' => 'form-control')) !!}
									@if ($errors->has('gallery_caption'))
										<span class="help-block">
											<strong>{{ $errors->first('gallery_caption') }}</strong>
										</span>
									@endif
							</div>
							
							<div class="form-group{{ $errors->has('b_image') ? ' has-error' : '' }}">
								<label for="b_image">Banner Image</label>
								<input type="file" name="b_image" id="uploadFile">
									@if ($errors->has('b_image'))
										<span class="help-block">
											<strong>{{ $errors->first('b_image') }}</strong>
										</span>
									@endif
							</div>
								 
						</div>
						<div class="col-md-6">
							<div class="form-group{{ $errors->has('gallery_status') ? ' has-error' : '' }}">
								<label for="gallery_status">Gallery Status</label>
									{{ Form::selectRange('gallery_status',null, 1,$category_status,['class' => 'form-control']) }}
									@if ($errors->has('gallery_status'))
										<span class="help-block">
											<strong>{{ $errors->first('gallery_status') }}</strong>
										</span>
									@endif
							</div>
							
							
							
							
							
						</div>

						
                   
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success add" id="btnSave">
                            <span id="" class='glyphicon glyphicon-check'></span> Add
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
				{!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

<!-- ***** -->
<script>
$("form#offer").submit(function(event){
		event.preventDefault();
		var formData = new FormData($(this)[0]);
		$('#btnSave').text('saving...');
		$('#btnSave').attr('disabled',true);
		$.ajax({
			url: "{{ url('/save-gallery') }}",
			type: 'POST',
			data: formData,
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			dataType: "JSON",
			success: function(data)
			{
				toastr.success('Successfully Add Video!', 'Success Alert', {timeOut: 5000});
				
				$('#tabs').prepend('<div class="thumb_gal"><a class="thumbnail fancybox" rel="ligthbox" href="../public/' + data.gallery_file + '"><img class="img-responsive" alt="" src="../public/' + data.gallery_file + '" /><div class="text-center"><label class="text-muted">'+ data.gallery_title +'</label></div></a><button class="show-modal btn btn-success" data-id="' + data.id + '" data-title="'+ data.gallery_title + '" data-content="' + data.gallery_title + '"><span class="glyphicon glyphicon-eye-open"></span> Show</button> <button class="edit_data btn btn-info" data-id="' + data.id + '" data-title="' + data.gallery_title + '" data-content="' + data.gallery_caption + '"><span class="glyphicon glyphicon-edit"></span> Edit</button> <button class="delete-modal btn btn-danger" data-id="' + data.id + '" data-title="' + data.gallery_title + ' "data-content="' + data.gallery_title + '"><span class="glyphicon glyphicon-trash"></span> Delete</button></div>');
				
				$('#btnSave').text('save'); //change button text
				$('#btnSave').attr('disabled',false); //set button enable
				$('#addModal').modal('toggle');
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error adding / update data');
				$('#btnSave').text('save'); //change button text
				$('#btnSave').attr('disabled',false); //set button enable 

			}
		
		});
		return false;
	});	
</script>