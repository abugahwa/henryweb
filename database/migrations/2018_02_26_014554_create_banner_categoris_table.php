<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBannerCategorisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banner_categoris', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('category_name');
			$table->integer('parent_id');
			$table->integer('sort_order');
			$table->integer('category_status');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banner_categoris');
	}

}
