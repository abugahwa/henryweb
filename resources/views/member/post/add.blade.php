@extends('member.layouts.member')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="main">
			{{ csrf_field() }}
			{{ method_field('post') }}
			{!! Form::open(['method' => 'POST','route' => ['insertpostmember'],'files' => true, 'class' => 'form-horizontal'])  !!}
				<input type="hidden" name="id_user" value="<?php echo \Auth::user()->id;?>">
				@include('admin.posts.form_post')
			{!! Form::close() !!}
		</div>
	</div>
</div>
	<script src="{{ asset('assets/bower_components/gentelella/vendors/jquery/dist/jquery.min.js') }}"></script>
	
	<script src="{{ asset('assets/bower_components/gentelella/vendors/moment/min/moment.min.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
	    
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
  <script type="text/javascript">  

     CKEDITOR.replace( 'technig' );  

  </script>

<script type="text/javascript">
    $('#start_publishing').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    }); 
    $('#finish_publishing').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss'
    });
</script> 
@endsection