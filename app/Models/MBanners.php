<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MBanners extends Model
{
     protected $table       = 'banners';
     protected $fillabel    = ['id','brand_id','b_image','b_link','b_name','b_category','b_status','b_sort_order','b_start_publishing','b_finish_publishing'];
     
    public function BannerCategory()
    {
        return $this->belongsTo('App\Models\MBannerCategory','b_category','id');
    }
}
