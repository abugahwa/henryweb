@extends('member.layouts.member')
@section('content')
<style>
.btn-edit{
	float: left;
    display: inline-flex;
}
.btn-edit .btn-danger{
	height: 33px;
    padding: 11px;
    margin: 0 3px;
}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="main">
			<div class="flash-message">
				@foreach (['danger', 'warning', 'success', 'info'] as $msg)
					@if(Session::has('alert-' . $msg))
						<p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
					@endif
				@endforeach
			</div> 
			<div class="col-md-8">
			
			</div>
			<div class="col-md-4">
				<input type="text" id="search_post" placeholder="search data" class="form-control col-md-4 pull-right">
			</div>
			<div style="clear:both;margin: 30px;"></div>
				<br />
			<div class="table-responsive">
				<table class="table table-striped">
					<thead bgcolor="#9853bb" style="color: #fff;">
						<tr>
							<th>No.</th>
    						<th width="400px">Post Title</th>
    						<th>Post Category</th>
    						<th>Viewer</th>
    						<th>Publish</th>
    						<th>Action</th>
    					</tr>
    				</thead>
    				<tbody>
    				@foreach($post as $key => $b)
    				<tr>
						<td>{{ $post->firstItem() + $key }}</td>
    					<td>{{ $b->post_title }}</td>
    					<td>{{ $b->PostCategories->category_name }}</td>
    					<td>{{ $b->post_hits }}</td>
    					<td>{{ Carbon\Carbon::parse($b->created_at)->format('D M Y i H:i:s') }}</td>
    					<td> 
							<div class="btn-edit">
								<a href="{{ route('removepostMember', $b->id) }}" class="btn-sm btn-danger" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="Delete Article" onclick="return confirm('Apakah Anda yakin menghapus post ini?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
								<a target="_blank" href="{{ url($b->post_alias.'.phtml')}}" class="btn-sm btn-success"><i class="fa fa-eye"></i></a> &nbsp;
									{!! Form::open(['method' => 'POST','route' => ['postsedit']])  !!}
									<input type="hidden" name="id" value="{{ $b->id }}">
									<button type="submit" class="btn-sm btn-info "><i class="fa fa-pencil-square-o"></i></button>
									{!! Form::close() !!}
							</div>
						</td>
    				</tr>
    				@endforeach
					</tbody>
    			</table>
			</div>
			@if($post->count() === 0)
				<div class="alert alert-danger">
					No data found.
				</div>
			@endif
			{!! $post->render() !!}
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

    $(document).ready(function () {
        $("#search_post").on('keyup',function () {
            var key = $(this).val();
            $.ajax({
                url:"{{ route('listpostmember') }}",
                type:'GET',
                data:'keyword='+key,
                beforeSend:function () {
					//alert('ubu');
					$('.loading').show();
                   // $("table").slideUp('fast');
                },
                success:function (data) {
                    $("table").html(data);
                   $('.load-bar').fadeOut("slow");
                }
            });
        });
    });

</script>
@endsection