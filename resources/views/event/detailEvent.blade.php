@extends('layouts.event')
@section('content')
<style>
.no_style{
  padding: 0;
  border-top: none;
  list-style: none;
  cursor: pointer
}



#datanya_a h1,h2,h3{
  clear: both;
}
#datanya_a .top{
  position: absolute;
  z-index: 9999;
  background: #fff;
  width: 100%;
}
@media only screen and (max-width: 599px) {
  #datanya_a table{
    width: 100% !important;
    overflow: hidden;
    padding: 10px;
  }
}
#back-to-top {
    position: fixed;
    bottom: 40px;
    right: 40px;
    z-index: 9999;
    cursor: pointer;
    transition: opacity 0.2s ease-out;
}
.table-bordered td, .table-bordered th {
    border: 1px solid #dee2e6;
    font-size: 10pt;
}
</style>
<link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
<img src="<?php echo route('cdn', $images_slide ); ?>" class="img-responsive center-block" width="100%">
<?php
    $data_c   = '';
    $even_css = '';
    if($alias == 'indonesia-digital-popular-brand-award' || $alias == 'indonesia-top-digital-public-relation-award'){
        $even_css    = 'ipdba';
        $data_c       = 'datanya_a';
    }elseif($alias == 'anugerah-brand-indonesia'){
        $even_css    = 'abi';
      }
?>
<div class="container">
    <div class="col-sm-12">
	<br />
	@if($name_event == 'hasil-survey')

  <?php
      $even_css = '';
      if($alias == 'indonesia-digital-popular-brand-award' || $alias == 'indonesia-top-digital-public-relation-award'){
          $even_css    = 'ipdba';
      }elseif($alias == 'anugerah-brand-indonesia'){
          $even_css    = 'abi';
      }
   ?>
    <div class="row">
    <!-- sidebar menu -->
      <div class="col-sm-3">
        <div class="card" style="width: 16rem;">
          <div class="card-header text-white bg-primary mb-3 {{ $even_css }}">
            {{ str_replace('-', ' ', ucwords($alias)) }}
          </div>
          <ul class="list-group list-group-flush">

            <?php
              $begin = '2015';
              $det_n  = \Carbon\Carbon::now();
              $now = date('Y', strtotime($det_n));

              for ($i = $begin; $i<=$now;$i++){
             ?>
            <li class="list-group-item">
              <a href="#submenu{{$i}}" data-toggle="collapse">{{ $i}}</a>
              <ul id="submenu{{$i}}" class="collapse">
                <?php
                  $start = 1;
                  $end  = 3;

                  for ($no = $start; $no<=$end;$no++){
                 ?>
                  <li class="no_style">
                      <a OnClick="dataEvent(<?php echo $no;?>,<?php echo $i;?>)" class="list-group-item list-group-item-action">Fase {{ $no }}</a>
                  </li>
                <?php } ?>
              </ul>
            </li>
          <?php } ?>
          </ul>
        </div>
      </div>
        <div class="col-sm-9">
            <div id="{{$data_c}}" class="scroll">
                @include('event.home.survey_result')
            </div>
        </div>
    </div>

    <!-- end sidebar -->
	@else

	<div class="card">
		<div class="card-header text-white bg-primary mb-3 {{ $even_css }}">
		{{ str_replace('-', ' ', ucwords($name_event)) }} - {{ $txt_events->GetName($idmenu)}}
		</div>
		<div class="card-body">

				@foreach($txt_events->GetTextsByMenuID($id) as $txtEvent)
					{!! $txtEvent->css !!}
					{!! $txtEvent->text_event !!}
					@endforeach

		</div>
	</div>
	<br />
	@endif
	</div>
</div>
<button id="back-to-top" type="button" class="btn btn-success"><i class="fas fa-chevron-up"></i></button>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
function dataEvent(fase,year){
     $.ajax({
          type: "get",
          url: "{{ route('edtailevents', $alias) }}",
          data: "fase=" + fase + "&year=" + year + "&alias={{ $alias }}" ,
      success: function (html) {
             $('#{{$data_c}}').html(html);
             $('#firs_idpba').hide();
      }
     });
  return false;
}
$('.no_style a').click(function () {
    $('.collapse').collapse('hide');
});
</script>
 <script type="text/javascript">

$(document).ready(function () {

$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('#back-to-top').fadeIn();
    } else {
        $('#back-to-top').fadeOut();
    }
});

$('#back-to-top').click(function () {
    $("html, body").animate({
        scrollTop: 0
    }, 600);
    return false;
});

});
</script>
@endsection
