@extends('henryweb.layouts.frontend')
@section('pageTitle', $categoryName)
@section('content')

    <div class="bg-navbar"></div>

    <div class="container">
      <div class="row">

        <div class="col-md-8 order-1 order-md-2">
            <br />
          <h2><?php echo $categoryName; ?></h2>
          <hr />
          <?php foreach($postsBlog as $post) { ?>
          <div class="media pb-3 blog-post-list">
            <div class="media-left">
                <img class="media-object" src="{{ url($post->post_img) }}" alt="<?php echo $post['post_title']; ?>" width="250" height="200" />
            </div>
            <div style="padding:0px 20px" class="media-body">
                <span class="badge badge-secondary"><?php echo $post->PostCategories->category_name; ?></span>
                <p><a style="color:#000000;font-size:18pt;" href="<?php echo url($post['post_alias'].".phtml"); ?>"><?php echo $post['post_title']; ?></a><br />
                <small class="d-none d-md-block">{{ str_limit(strip_tags($post['post_text']), $limit = 150, $end = '...') }}</small></p>
            </div>
          </div>
          <?php } ?>
          <hr />
          <nav>{{ $postsBlog->links("pagination::bootstrap-4") }}</nav>
        </div>

        <div class="col-md-4 order-3 py-4">
          <div>
             @include('shared.rightColumn')
          </div>
        </div>
      </div>
    </div>

@endsection
