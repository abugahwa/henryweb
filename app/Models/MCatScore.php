<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MCatScore extends Model
{
    protected   $table            = 'cat_score';
    protected   $fillable         = ['id', 'cat_name', 'menu_id', 'parent_id', 'created_at', 'update_at', 'fase', 'years'];
}
