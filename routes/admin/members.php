<?php
Route::get('social/login/redirect/{provider}', ['uses' => 'Auth\AuthController@redirectToProvider', 'as' => 'social.login']);
Route::get('social/login/{provider}', 'Auth\AuthController@handleProviderCallback');

Route::get('/redirect/google', ['uses' => 'Auth\SocialiteController@redirectToProvider', 'as' => 'social.google']);
Route::get('login/google', 'Auth\SocialiteController@handleProviderCallback')->name('google');

Route::get('redirect/twitter', ['uses' => 'Auth\TwitterLogin@redirectToProvider', 'as' => 'social.twitter']);
Route::get('access/twitter', 'Auth\TwitterLogin@handleProviderCallback')->name('twitter');

Route::get('login-member', 'Member\HomeController@login')->name('loginmember');
Route::get('home-member', 'Member\HomeController@HomeMember')->name('homemember');
Route::get('reg-member', 'Member\RegMemberController@index');
Route::post('create-member', 'Member\RegMemberController@store')->name('insertsmember');

Route::post('insert-member/{id}', 'Member\auth\MembersController@InsertProfile')->name('insertmember');
Route::get('members-infobrand','Member\auth\MembersController@index')->name('memberinfo');
/*post */
Route::get('add-post-member','Member\post\PostController@create')->name('addpostmember');
Route::post('insert-post-member','Member\post\PostController@store')->name('insertpostmember');
Route::get('list-post-member','Member\post\PostController@index')->name('listpostmember');
Route::post('edit-post-member', 'Member\post\PostController@edit')->name('postsedit');
Route::post('update-post-member/{id}', 'Member\post\PostController@update')->name('postsupdate');
Route::get('delete-post-member/{id}', 'Member\post\PostController@destroy')->name('removepostMember');

Route::post('logout-member', 'Member\auth\MembersController@getLogout')->name('logoutmember');
Route::post('loginmember', 'Auth\AuthController@attempt')->name('signinmember');

/* chart */
Route::get('report-viewer', 'Member\report\ReportMemberController@ReportViewer');

/* profile */
Route::get('profile-member', 'Member\HomeController@MemberProfile');
Route::post('update-profile-member', 'Member\auth\MembersController@update')->name('updateprofile');

Route::get('get-profile-member/{id}', 'Member\auth\MembersController@GetProfile');
Route::get('get-gender-member/{id}', 'Member\auth\MembersController@combo_data');

Route::post('update-profiles', 'Member\auth\MembersController@updateProfile')->name('updateprofilemember');
Route::get('reset-password/{token}', function ($token) {
    return view('auth.passwords.reset', compact('token'));
})->name('resetpass');
Route::post('change-password', 'Auth\UpdatePasswordController@update')->name('password.update');
