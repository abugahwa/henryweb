@extends('layouts.frontend')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img class="d-block w-100" src="{{ asset('images/slide-1.png') }}" alt="First slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('images/slide-1.png') }}" alt="Second slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="{{ asset('images/slide-1.png') }}" alt="Third slide">
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>

        </div>
    </div>
</div>


<div class="row-home-2">
    <div class="row">
        <div class="col-md-6 p-0">
          <img class="img-responsive h-100 float-right" src="{{ asset('images/cbezt-home-1.png') }}" alt="CBezt Fried Chicken">
        </div>
        <div class="col-md-6 p-0">
          <img class="img-responsive h-100 float-left" src="{{ asset('images/cbezt-home-2.png') }}" alt="CBezt Fried Chicken">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 p-0">
          <img class="img-responsive h-100 float-right" src="{{ asset('images/cbezt-home-3.png') }}" alt="CBezt Fried Chicken">
        </div>
        <div class="col-md-6 p-0">
          <img class="img-responsive h-100 float-left" src="{{ asset('images/cbezt-home-4.png') }}" alt="CBezt Fried Chicken">
        </div>
    </div>
</div>

<div class="container">
    <div class="row text-center">
        <div class="col-md-12 py-4">
          <h1>C'Bezt Fried Chicken</h1>
        </div>
        <div class="col-md-2">

        </div>
        <div class="col-md-8">
          <h4>CBEZT_friedchicken merupakan restoran cepat saji. Produk utama kami adalah "Ayam Goreng Crispy" atau yang lebih dikenal dengan FRIED CHICKEN. Pemilihan kami akan produk tersebut dikarenakan fried chicken merupakan makanan alternatif masyarakat Indonesia.</h4>
        </div>
        <div class="col-md-2">

        </div>
    </div>
    <div class="row py-4">
        <div class="col-md-6 pb-3">
          <button type="button" class="btn btn-cyellow btn-w300 pull-right">PROFILE SELENGKAPNYA</button></p>
        </div>
        <div class="col-md-6 pb-3">
          <button type="button" class="btn btn-cyellow btn-w300">HUBUNGI KAMI</button></p>
        </div>
    </div>
</div>

<div class="row row-yellow bottom-style-1">
  <div class="container">
    <div class="row text-center">
        <div class="col-md-12 py-4">
          <h2>Promo Menarik</h2>
        </div>
    </div>
    <div class="row">
      <?php foreach($postsPromo as $p) { ?>
        <div class="col-md-4">
          <img width="100%" height="200" class="img-responsive rounded py-2" src="{{ $p->post_img }}" />
          <h5 class="textTitle"><a href="{{ url($p->post_alias . '.phtml') }}">{{ $p->post_title }}</a></h5>
          <p>{{ str_limit(strip_tags($p->post_text), $limit = 350, $end = '...') }}</p>
        </div>
      <?php } ?>
    </div>
  </div>
          <img class="image-1" src="{{ asset('images/bottom-style-1.png') }}" alt="CBezt Fried Chicken">
</div>



<div class="container">
  <div class="row text-center">
      <div class="col-md-12 py-4">
        <h2>Berita CBezt Fried Chicken</h2>
      </div>
  </div>
  <div class="row pb-5">

<?php foreach($postsNews as $p) { ?>
      <div class="col-md-4 pb-3">
        <div class="newsBorder">
          <img width="100%" height="200" class="img-responsive pb-2 px-0" src="{{ $p->post_img }}" />
          <h5 class="textTitle"><a href="{{ url($p->post_alias . '.phtml') }}"><?php echo $p['post_title']; ?></a></h5>
          <p>{{ str_limit(strip_tags($p->post_text), $limit = 350, $end = '...') }}</p>
        </div>
      </div>
<?php } ?>

  </div>
  <div class="row">
    <div class="col-md-12 text-center pb-2">
      <button type="button" class="btn btn-cyellow btn-w300"><a href="{{ route('posts.blog', 'latest-news') }}">Baca Selengkapnya</button></p>
    </div>
  </div>
</div>

<div class="container">
  <div class="row p-5">
      <div class="col-md-6 p-5 my-auto">
        <h2 class="pb-2"><strong>Semua Hal Seru Hanya ada di C'Bezt Fried Chicken</strong></h2>
        <button type="button" class="btn btn-cyellow btn-w300">Baca Selengkapnya</button></p>
      </div>
      <div class="col-md-6">
          <img class="img-responsive w-100 float-left" src="{{ asset('images/cbezt-gallery.png') }}" alt="CBezt Fried Chicken">
      </div>
  </div>
</div>



<!-- SnapWidget -->
<script src="https://snapwidget.com/js/snapwidget.js"></script>
<iframe src="https://snapwidget.com/embed/776855" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:100%; "></iframe>

<div class="bg-red">
<div class="">
  <div class="row py-3">
      <div class="col-md-6 my-auto">
        <h3 class="mx-0 float-right">Follow Instagram C'Bezt Fried Chicken</h3>
      </div>
      <div class="col-md-6 p-0">
        <a href="https://instagram.com/cbeztfriedchicken.official" target="_blank">
          <img class="img-responsive float-right" src="{{ asset('images/cbezt-instagram-line.png') }}" alt="CBezt Fried Chicken">
        </a>
      </div>
  </div>
</div>
</div>

@endsection
