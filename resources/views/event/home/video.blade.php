<div class="container">
        <div class="row row-space">
          <div class="col-md-12">
              <h3>VIDEO</h3>
              <div class="list"></div>
              <br />
          </div>
        </div>

<style>
#video-player {
  background-color: #000000;
  color: #FFFFFF;
  margin-bottom: 10px;
}

#video-player .videoarea {
  list-style:none; width:100%; padding: 20px;
}

#video-player .videoarea iframe {
  width:100%;
}

@media (max-width: 767px) {
  #video-player .videoarea iframe {
    width:100%;
    height:auto;
    min-height:200px;
  }
}

#video-player .playlist {
  margin: 0px;
  padding: 10px;
  list-style: none;
  position: relative;
}

#video-player .playlist img {
  width: 80px;
  height: 80px;
  margin-right: 10px;
}

#video-player .playlist a {
  color: #FFFFFF;
}

#video-player .playlist a:hover {
  text-decoration: none;
}

#video-player .playlist li {
  cursor: pointer;
  overflow-y: hidden !important;
  height: 95px;
  background-color: #000000;
  line-height: 15pt;
  border-bottom: 1px dotted #FFFFFF;
  margin-bottom: 5px;
  color: #fff;
}
#video-player .index {
  padding: 10px;
  color: #FFFFFF;
  font-weight: bold;
}
</style>

    <div id="video-player">

      <div class="row">
        <div class="col-md-8">
          <div class="videoarea">
          <iframe frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
          </div>
          <!--<video class="videoarea" id="videoarea" poster="" src="" proload="auto" controls></video>-->
        </div>

        <div class="col-md-4">
          <ul class="playlist">

            <?php foreach($videolist as $video) { ?>
             <a class="list" videoid="{{ $video->video_id }}"><li>
               <img src="https://img.youtube.com/vi/{{ $video->video_id }}/0.jpg" class="rounded float-left" alt="{{ $video->title }}">
                 {{ ucwords(strtolower($video->title)) }}
             </li></a>
             <?php } ?>


           </ul>
           <a href="{{ route('videos.index') }}" class="text-right pull-right index">Index Video <i class="fas fa-angle-double-right"></i></a>
         </div>
      </div>
      <div class="float-right">
        <a href="{{ route('video_event',$alias) }}"><u>Index</u></a>
      </div>
     </div>

    </div>
