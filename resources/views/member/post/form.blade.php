<div class="col-md-6">
	<div class="form-group">
		<label for="exampleFormControlInput1">Post Title</label>
		{!! Form::text('post_title', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Banner Name','class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		<label for="exampleFormControlInput1">Post Lead In</label>
		{!! Form::text('post_lead_ind', null, array('autofocus' => 'autofocus','placeholder' => 'Banner Name','class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		<label for="post_lead_in">Image</label>
		{!! Form::file('post_img', null) !!}
	</div>
	<div class="form-group">
		<label for="post_img_caption">Image Caption</label>
		{!! Form::text('post_img_caption', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Image Caption','class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		<label for="name">Category Post</label>
		{!! Form::select('post_category', $catpost, $catid, ['id' => 'post_category','class' => 'form-control', 'placeholder' => 'Please Select']) !!}
	</div>
</div>