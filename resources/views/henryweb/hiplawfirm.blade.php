@extends('henryweb.layouts.frontend')
@section('pageTitle', 'Henry Indraguna & Partners')

@section('content')


<div class="bg-navbar"></div>

@include('henryweb.shared.hiplawyers')
@include('henryweb.shared.ourpracticearea')

<div class="container">
  <h2 class="text-center py-4">Our People</h2>
  <div class="row">

    @foreach ($postsLawyers as $p)
    <div class="col-md-4 text-center p-2">
      <a href="{{ route('hip.profiles', $p->post_alias) }}">
        <img class="rounded" style="width:200px;height:250px;object-fit:cover;" src="{{ url($p->post_img) }}">
      </a>
      <p><strong>{{ $p->post_title }}</strong></p>
    </div>
    @endforeach
  </div>
</div>
<!--
<div class="container">
  <h2 class="text-center py-4">Our People</h2>
  <div class="row">
    <div class="col-md-4 text-center p-2">
      <img class="rounded" style="width:200px;height:250px;object-fit:cover;" src="{{ url('images/lawyers/adi-sutrisno-simanjutak.jpg') }}">
      <p><strong>Adi Sutrisno Simanjutak</strong></p>
    </div>
    <div class="col-md-4 text-center p-2">
      <img class="rounded" style="width:200px;height:250px;object-fit:cover;" src="{{ url('images/lawyers/apriwanto-manik.jpg') }}">
      <p><strong>Apriwanto Manik</strong></p>
    </div>
    <div class="col-md-4 text-center p-2">
      <img class="rounded" style="width:200px;height:250px;object-fit:cover;" src="{{ url('images/lawyers/hendry-sangapta-sitepu.jpg') }}">
      <p><strong>Hendry Sangapta Sitepu</strong></p>
    </div>
    <div class="col-md-4 text-center p-2">
      <img class="rounded" style="width:200px;height:250px;object-fit:cover;" src="{{ url('images/lawyers/kayaruddin-hasibuan.jpg') }}">
      <p><strong>Kayaruddin Hasibuan</strong></p>
    </div>
    <div class="col-md-4 text-center p-2">
      <img class="rounded" style="width:200px;height:250px;object-fit:cover;" src="{{ url('images/lawyers/siti-aminah-zuhria.jpg') }}">
      <p><strong>Siti Aminah Zuhria</strong></p>
    </div>
  </div>
</div>
-->
@include('henryweb.shared.konsultasi-hukum')

<div class="container">
  <div class="row">
    <div class="col-md-6 pb-4">
      <h2>Henry Indraguna & Partners</h2>

      TREASURY TOWER<br />
      9th Floor<br />
      Jl. Jend. Sudirman Kav. 52-53,<br />
      Lot 28 - District 8 SCBD,<br />
      Jakarta, 12190.<br />
      info@henryindraguna.com<br />
      021 50111278 (Hunting)<br />

      <div class="py-4"><a href="https://goo.gl/maps/kwZWRny3bA5GURSi9" target="_blank"><button class="btn btn-md btn-secondary">Get Directions</button></a></div>
    </div>
    <div class="col-md-6 pb-4">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d63460.403749018486!2d106.806904!3d-6.227397!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb4e00e59a970abc9!2sDistrict%208%20SCBD!5e0!3m2!1sid!2sid!4v1606207606471!5m2!1sid!2sid" style="width:100%;min-height:450px;" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
  </div>
</div>

@endsection
