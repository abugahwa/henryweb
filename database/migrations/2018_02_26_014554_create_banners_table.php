<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBannersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('banners', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('brand_id');
			$table->string('b_image');
			$table->string('b_link');
			$table->string('b_name');
			$table->integer('b_category');
			$table->integer('b_status');
			$table->integer('b_sort_order');
			$table->dateTime('b_start_publishing');
			$table->dateTime('b_finish_publishing');
			$table->integer('user_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('banners');
	}

}
