@extends('iamadmin.layouts.dashboard')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Posts</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Videos</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
          @if(Session::has('alert-' . $msg))
          <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
          @endif
        @endforeach
      </div>

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Video List</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>

        <div class="card-body table-responsive p-0">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th>ID</th>
                <th>Title</th>
                <!--<th>Category</th>-->
                <th>Publish</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($videos as $key => $b)
              <tr>
                <td>{{ $videos->firstItem() + $key }}</td>
                <td>{{ $b->title }}</td>
                <!--<td>@if(!empty($b->VideoCategories->category_name)) {{ $b->VideoCategories->category_name }} @else Uncategorized @endif</td>-->
                <td>{{ Carbon\Carbon::parse($b->created_at)->format('d-m-Y H:i') }}</td>
                <td>
                  <div class="btn-group">
                    <button onClick="window.open('{{ route('videos.view', $b->id) }}')" type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="View">
                    <i class="fas fa-eye"></i></button>
                    <button onClick="window.location='{{ route('admin.videos.edit', $b->id) }}'" type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Edit">
                    <i class="fas fa-edit"></i></button>
                    <a onclick="return confirm('Apakah Anda yakin menghapus video ini?')" href="{{ route('admin.videos.delete', $b->id) }}" type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-container="body" title="Delete">
                    <i class="far fa-trash-alt"></i></a>
                  </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          {{ $videos->render("pagination::bootstrap-4") }}
        </div>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
