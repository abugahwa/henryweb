<?php

namespace App\Http\Controllers\Member\report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Classes\Report;
use DB;
use Carbon;

class ReportMemberController extends Controller
{
	protected $report;
	
	public function __construct()
    {
        parent::__construct();
        $this->report = new Report;
       
    }
    public function ReportViewer()
	{
		$dateS 					=  Carbon\Carbon::now()->startOfMonth()->subMonth(12);
		$dateE 					=  Carbon\Carbon::now()->startOfMonth(); 
		
		$startMonth				= $dateS->subMonth()->format('F Y');
		$endMonth				= $dateE->format('F Y');
		
		$chrt					= $this->report->ViewerByuser();
		
		return view('member.report.viewer', compact('chrt'));
    }
	
}
