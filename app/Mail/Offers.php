<?php

namespace App\Mail;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Offers extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(Request $request)
    {
        $pdf_name = $request->pdf_name;
        return $this->markdown('emails.orders.shipped')
					->attach('create/pdf/'.$pdf_name, [
						'as' => $pdf_name.'.pdf',
						'mime' => 'application/pdf',
					])
                        ->with('content',$this->content);
    }
}
