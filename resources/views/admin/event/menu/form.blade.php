<div class="form-group">
	<label>Menu Name</label>
	{!! Form::text('menu_name', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Menu Title','class' => 'form-control')) !!}
</div>
<div class="form-group">
	<label>Sort Order</label>
	{!! Form::number('sort_order', null, array('autofocus' => 'autofocus','placeholder' => 'Sort Order','class' => 'form-control')) !!}
</div>
<div class="form-group">
	<label>Logo</label>
	<input type="file" name="logo_menu">
</div>
<div class="form-group">
	<label>Slide</label>
	<input type="file" name="slide">
</div>
<div class="form-group">
	<label>Parent Menu</label>
	<select class="form-control itemName" name="parent">
		<option value="0">Pilih</option>
		@if($categories->count())
			@foreach($categories as $role)
				
				<option value="{{ $role->id }}" {{ $cat == $role->id ? 'selected="selected"' : '' }}><strong>{{ $role->menu_name }}</strong></option>
				@foreach($child->byParentMenu($role->id) as $categorys)
					<option disabled class="dropdown-item disabled" value="{{ $role->id }}"><strong>&nbsp;&nbsp;{{ $categorys->menu_name }}</strong></option>
				@endforeach
			@endforeach
		@endif
	</select>

</div>
<div class="form-group">
	<label>Description</label>
	{!! Form::textarea('description', null, array( 'autofocus' => 'autofocus','placeholder' => 'Description','class' => 'form-control')) !!}
</div>
<div class="form-group">
	<label>Sytle FIle</label>
	{!! Form::textarea('css', null, array('autofocus' => 'autofocus','placeholder' => 'Description','class' => 'form-control')) !!}
</div>
<div class="form-group">
	<button type="submit" class="btn btn-success cbutton">Add</button>
</div>