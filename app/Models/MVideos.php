<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MVideos extends Model
{
    protected $table        = 'videos';
    protected $fillabel     = [ 'id','video_id','title','description','hits','is_active','is_deleted','created_at','updated_at','deleted_at', 'video_category'];
}
