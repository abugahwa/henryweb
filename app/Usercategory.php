<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Usercategory extends Model
{
	protected $table = 'departments';
    public $fillable = ['id','name'];
	
	public function CategoryUser()
	{
		return $this->belongsTo('App\User','department_id','id');
	}
}
