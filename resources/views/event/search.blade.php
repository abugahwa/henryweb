@extends('layouts.event')
@section('content')
<style>
    .card{
        width: 100%;
        margin-bottom: 20px;
    }
    .card img{
        width: 100%;
    }
    .lib-panel {
    margin-bottom: 20Px;
}
.lib-panel img {
    background-color: transparent;
    height: 163px;
}

.lib-panel .row,
.lib-panel .col-md-6 {
    padding: 0;
    background-color: #FFFFFF;
    height: 139px;
    overflow: hidden;
    width:  258px;
}


.lib-panel .lib-row {
    padding: 0 11px 0 8px;
}

.lib-panel .lib-row.lib-header {
    background-color: #FFFFFF;
    font-size: 15px;
    font-weight: bold;
   /* padding: 10px 20px 0 20px; */
}

.lib-panel .lib-row.lib-header .lib-header-seperator {
    height: 2px;
    width: 26px;
    background-color: #d9d9d9;
    margin: 7px 0 7px 0;
}

.lib-panel .lib-row.lib-desc {
    position: relative;
    /*height: 100%;*/
    display: block;
    font-size: 13px;
}
.lib-panel .lib-row.lib-desc a{
    position: relative;
    width: 100%;
    bottom: 0;
    left: 0;
}

.row-margin-bottom {
    margin-bottom: 20px;
}

.box-shadow {
    -webkit-box-shadow: 0 0 10px 0 rgba(0,0,0,.10);
    box-shadow: 0 0 10px 0 rgba(0,0,0,.10);
    height: 163px;
    overflow: hidden;
}

.no-padding {
    padding: 0;
}
.col-sm-3 h3, hr{
    margin-left: -17px;
}
#categories_card{
    height: 189px;
    overflow:hidden;
    border: none;
    margin-left: -17px;
}
#categories_card img{
    height: 189px;
}
#categories_card .card-block{
    width: 100%;
    padding: 0 !important;
    margin-left: -18px;
}
@media only screen and (max-width: 599px) {
    #categories_card {
        width: 100%;
        height: auto;
    }
    #categories_card .card-block {
        width: 100%;
        padding: 10px !important;
        margin-left: 0;
    }
}
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-3" style="padding: 21px">
        <h3>Most Popular</h3>
        <hr />
        @foreach($cat_id as $idCat)
          @foreach($postPopuler->GetPosByHitstByCategory($idCat->id) as $cat)
        <div class="lib-panel">
            <div class="row box-shadow">
                <div class="col-md-6">
                    <img class="lib-img" src="<?php echo route('cdn', $cat['post_img'] ); ?>">
                </div>
                <div class="col-md-6">
                    <div class="lib-row lib-header">
                        {!! str_limit(strip_tags($cat->post_title), $limit = 25, $end = '...') !!}
                        <div class="lib-header-seperator"></div>
                    </div>
                    <div class="lib-row lib-desc">
                        <a href="{{ route('detailPost', [ $alias, $categories_name->FirstAliasByID($cat->post_category), $cat->post_alias] ) }}">{!! str_limit(strip_tags($cat->post_text), $limit = 80, $end = '...') !!}</a>
                    </div>
                </div>
            </div>
        </div>
          @endforeach
        @endforeach
        </div>
        <div class="col-sm-9" style="padding: 21px">
        <h2>{{ ucwords($title_cat) }}</h2>
        <hr />
            <div id="categories">
                @include('event.includes.card_categories')
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">

$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        getArticles(url);
        window.history.pushState("", "", url);
    });

    function getArticles(url) {
        $.ajax({
            url : url
        }).done(function (data) {
            $('#categories').html(data);
        }).fail(function () {
            alert('Articles could not be loaded.');
        });
    }
});
</script>
@endsection
