<div class="form-group pull-right" id="images_rows">
	@if ($images->onFirstPage())
		<button class="btn btn-success disabled">First</button>
	@else
		<a href="{{ $images->previousPageUrl() }}" rel="prev" class="btn btn-success image_i"><i class='fa fa-angle-left'></i></a>
	@endif

	@if ($images->hasMorePages())
		<a href="{{ $images->nextPageUrl() }}" rel="prev" class="btn btn-success"><i class='fa fa-angle-right'></i></a>
	@else
		<button class="btn btn-success disabled">End</button>
	@endif
</div>
<div style="clear:both;"></div>
<div class="row">
@foreach($images as $c_image)
	<div class="col-md-3">
		<a href="{{ asset($c_image->gallery_file) }}" class="thumbnail" data-lightbox="example-1">
			<img src="{{ asset($c_image->gallery_file) }}" class="img-responsive img-thumbnail" width="100%">
		</a>
	</div>
@endforeach
</div>
<script src="{{ asset('css/event/js/lightbox-plus-jquery.min.js') }}"></script>
<script>
$(function(){
	$('body').on('click', '#images_rows a', function(e) {
		e.preventDefault();
		$("#LoadingDulu").html("<div id='LoadingContent'><i class='fa fa-spinner fa-spin'></i> Mohon tunggu ....</div>");
		$("#LoadingDulu").show();
		$( "#images_rows a" ).attr( "disabled", true );
		var url = $(this).attr('href');  
		getArticles(url);
		window.history.pushState("", "", url);
		return false;
	});

	function getArticles(url) {
		$.ajax({
			url : url  
		}).done(function (data) {
			$('#images_rows2').html(data);  
			$("#LoadingDulu").fadeOut();
		}).fail(function () {
			alert('Articles could not be loaded.');
		});
	}
});
</script>