<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Models\MUserProfile;
use App\Models\MPost;

use Socialite;
use Auth;
use Exception;
use Session;
use Carbon;
use DB;

class HomeController extends Controller
{
    public function login()
	{
		if(Auth::check()){
			return redirect()->intended('/members-infobrand');
		}else{
		return view('member.auth.login');
		}
	}
	public function HomeMember()
	{
		$id	= Session::get('id');
		$name	= Session::get('name');
		$first_name	= Session::get('first_name');
		$last_name	= Session::get('last_name');
		$email	= Session::get('email');
		$gender	= Session::get('gender');
		$nickname	= Session::get('nickname');
		if(!empty($email)){
			$idprofile				= User::where('email',$email)->first()->id; 
		}else{
			$idprofile				= Null;
		}
		$check				= \Auth::user()->password;
		if (!isset($check[0])){
			$data			= MUserProfile::find($idprofile);
			return view('member.index', compact('name','first_name', 'last_name', 'email', 'gender','id','data','idprofile','nickname'));
		}else{
			return redirect()->route('memberinfo');
		}
	}
	public function index()
	{
		
		$check				= \Auth::user()->password;
		if ($check < 1){
			return view('member.index', compact('name','first_name', 'last_name', 'email', 'gender','id'));
		}else{
			return redirect()->route('memberinfo');
		}
	}
	public function MemberProfile()
	{
		//$user				= MUserProfile::join('users', 'user_profile.id_user', 'users.id')->where('id_user', \Auth::user()->id)->get();
		$user				= User::join('user_profile', 'users.id', 'user_profile.id_user')->find(\Auth::user()->id);
		//return json_encode($user);
		return view('member.user_profile', compact('user'));
	}
	
}
