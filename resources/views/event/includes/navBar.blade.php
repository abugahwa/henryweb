<?php
    $even_css = '';
    if($alias == 'indonesia-digital-popular-brand-award' || $alias == 'indonesia-top-digital-public-relation-award'){
        $even_css    = 'ipdba';
    }elseif($alias == 'anugerah-brand-indonesia'){
        $even_css    = 'abi';
    }elseif($alias == 'mobile-application-choice-award'){
        $even_css ='imac';
    }elseif($alias == 'pertama-di-indonesia'){
        $even_css  = 'perdi';
    }
 ?>
<nav class="navbar navbar-expand-lg navbar-light bg-light {{ $even_css }}">
  <div class="container">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon" style="color: #fff;"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto mr-oto">
        <li class="nav-item">
            <!--<a class="nav-link" href="{{route('events', $alias)}}">Home </a>-->
            <a class="nav-link" href="{{route('home')}}">Home </a>
        </li>
        @foreach($menu as $b)
        <li class="nav-item">
            <a class="nav-link"  href="{{ route('detail_event',[$alias,$b->id,$b->menu_alias]) }}">{{ $b->menu_name }}</a>
        </li>
        @endforeach


          <form class="form-inline my-2 my-lg-0 search_form" method="get" action="{{ route('searching_event', $alias) }}">
          <div class="opens">
          <li class="nav-item dropdown ml-auto search_f">
            <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-search" style="color:#fff;"></i>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <div style="padding: 10px;">
                <input type="text" name="search" class="form-control" placeholder="keyword" aria-label="keyword" aria-describedby="basic-addon2">

              </div>
            </div>
          </li>
        </div>
      </form>
    </ul>

  </div>

</nav>
