<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MPhotos extends Model
{
    protected $table        = 'photos';
    protected $fillabel     = [ 'id',
                                'title',
                                'slug',
                                'file',
                                'description',
                                'album_id',
                                'sort_order',
                                'user_id',
                                'is_deleted',
                                'created_at',
                                'updated_at',
                                'deleted_at',
                              ];

    public function albums()
    {
        return $this->belongsTo('App\Models\MPhotosAlbums','album_id','id');
    }
}
