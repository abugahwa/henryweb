    <nav class="navbar navbar-toggleable-md navbar-dark navbar-infobrand" style="background-color: #FF0000;">
    <!-- <nav class="navbar navbar-toggleable-md navbar-inverse bg-inverse navbar-infobrand">-->
      <div class="container">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="clearfix"></div>

      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('home') }}"><span style="font-size:15pt;" class="fa fa-home"></span> <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('posts.blog', 'brand-update') }}">{{ strtoupper('Brand Update') }}</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('posts.blog', 'brand-headline') }}">{{ strtoupper('Brand Headline') }}</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('posts.blog', 'business-economy') }}">{{ strtoupper('Business & Economy') }}</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('videos.index') }}">{{ strtoupper('InfoBrand.tv') }}</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="https://infobrand.id/event/indonesia-digital-popular-brand-award.html">{{ strtoupper('Data Riset') }}</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="{{ route('posts.blog', 'brand-achievement') }}">{{ strtoupper('Brand Award') }}</a>
          </li>



          <li class="nav-item active">
            <a class="nav-link" href="{{ route('posts.blog', 'opinions') }}">{{ strtoupper('Opini') }}</a>
          </li>
<!--
          <li class="nav-item active">
            <form class="navbar-form" method="get" action="{{ route('posts.search') }}">
             <div class="input-group">
               <input style="width:150px!important" type="text" name="query" class="form-control" placeholder="Search">
             </div>
            </form>
          </li>
-->

          <!--<li class="nav-item active">
          <form class="navbar-form" method="get" action="{{ route('posts.search') }}">
           <div class="input-group">
             <input type="text" name="query" class="form-control" placeholder="Search">
           </div>
          </form>
          </li>-->

        </ul>

      </div>
      </div>
    </nav>
