<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Photos;
use App\Classes\Galleries;
use App\Classes\GalleriesCategory;

class PhotosController extends Controller
{

  protected $photos;

  public function __construct()
  {
      parent::__construct();
      $this->photos = new Photos;
      $this->galleries = new Galleries;
      $this->galleriescategory = new GalleriesCategory;
  }

  public function index()
  {
    $photos = $this->galleries->getByGroup(['paginate' => 30]);
    //$thumbnail = $this->galleries->GetAll();
    //$photos = $this->photos->getAll(['sortby'=>'group','paginate' => 10]);
    //$slides = $this->photos->getAll(['paginate' => 10]);

    return view('photos.index', compact('photos'));
  }

  public function view($id)
  {
/*
    $photo    = $this->photos->getById($id);
    $addHits  = $this->photos->addHits($photo->id);
*/

    $slides = $this->galleries->getByAlbumID($id, array());
    $title = $slides[0]->Galleries->category_image;
    //$another = $this->photos->getAll(['limit' => 4, 'sortby'=> 'group']);

    return view('photos.view', compact('slides', 'title'));
  }

}
