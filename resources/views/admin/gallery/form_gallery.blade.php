<div class="col-md-6">
    <div class="form-group{{ $errors->has('category_image') ? ' has-error' : '' }}">
		<label for="category_image">Category Gallery</label>
		<select class="form-control m-bot15" name="category_image">
        @if($CateImg->count())
            @foreach($CateImg as $role)
                <option value="{{ $role->id }}" {{ $catid == $role->id ? 'selected="selected"' : '' }}>{{ $role->category_image }}</option>    
            @endforeach
        @endif

        </select>
			@if ($errors->has('category_image'))
				<span class="help-block">
					<strong>{{ $errors->first('category_image') }}</strong>
				</span>
			@endif
	</div>
	
	<div class="form-group{{ $errors->has('gallery_title') ? ' has-error' : '' }}">
		<label for="gallery_title">Gallery Name</label>
		
			{!! Form::text('gallery_title', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Gallery Name','class' => 'form-control')) !!}
			@if ($errors->has('gallery_title'))
				<span class="help-block">
					<strong>{{ $errors->first('gallery_title') }}</strong>
				</span>
			@endif
	</div>
	<div class="form-group{{ $errors->has('gallery_caption') ? ' has-error' : '' }}">
		<label for="gallery_caption">Gallery Caption</label>
		
			{!! Form::text('gallery_caption', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Gallery Caption','class' => 'form-control')) !!}
			@if ($errors->has('gallery_caption'))
				<span class="help-block">
					<strong>{{ $errors->first('gallery_caption') }}</strong>
				</span>
			@endif
	</div>
	
	<div class="form-group{{ $errors->has('b_image') ? ' has-error' : '' }}">
		<label for="b_image">Banner Image</label>
		<input type="file" name="b_image" id="uploadFile">
			@if ($errors->has('b_image'))
				<span class="help-block">
					<strong>{{ $errors->first('b_image') }}</strong>
				</span>
			@endif
	</div>
	     
</div>
<div class="col-md-6">
	<div class="form-group{{ $errors->has('gallery_status') ? ' has-error' : '' }}">
		<label for="gallery_status">Gallery Status</label>
			{{ Form::selectRange('gallery_status',null, 1,$category_status,['class' => 'form-control']) }}
			@if ($errors->has('gallery_status'))
				<span class="help-block">
					<strong>{{ $errors->first('gallery_status') }}</strong>
				</span>
			@endif
	</div>
	
	
	
	
	
</div>
<div style="clear:both;"></div>
<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
			<a class="btn btn-primary cbutton2" href="{{ url()->previous() }}">Cancel</a>
			<?php 
				if(Request::is('edit-address/*')){;?>
            <button type="submit" class="btn btn-success cbutton">Update</button>
			<?php }else{ ?>
            <button type="submit" class="btn btn-success cbutton" name='submitImage'>Submit</button>
			<?php } ?>
        </div>
	</div>