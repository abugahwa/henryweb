<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\MPost;

use DB;
use Auth;
use Carbon;
class Chartbar extends Controller
{
    public function index()
	{
		$dateS = Carbon\Carbon::now()->startOfMonth()->subMonth(12);
		$dateE = Carbon\Carbon::now()->startOfMonth(); 
		
		$startMonth			= $dateS->subMonth()->format('F Y');
		$endMonth			= $dateE->format('F Y');
		
		$hits			 	= MPost::select(DB::raw("DATE_FORMAT(post_date_added,'%M') as months"),DB::raw('sum(post_hits) as sums'))
								->whereBetween('post_date_added',[$dateS,$dateE])
								->orderBy('post_date_added')
								->groupBy('months')
								->get();
		
		$charthits			= '';
		foreach($hits as $b){
			$charthits	.="
			{
					name: '".$b->months ."',
					y: ".$b->sums ."
			},
			";
		}
		echo json_encode($hits);
		return view('admin.chartbar.index', compact('charthits'));
	}
}
