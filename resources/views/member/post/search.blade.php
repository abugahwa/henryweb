<div class="table-responsive">
				<table class="table table-striped">
					<thead bgcolor="#9853bb" style="color: #fff;">
						<tr>
							<th>No.</th>
    						<th width="400px">Post Title</th>
    						<th>Post Category</th>
							<th>Viewer</th>
    						<th>Publish</th>
    						<th>Action</th>
    					</tr>
    				</thead>
    				<tbody>
    				@foreach($post as $key => $b)
    				<tr>
						<td>{{ $post->firstItem() + $key }}</td>
    					<td>{{ $b->post_title }}</td>
    					<td>{{ $b->PostCategories->category_name }}</td>
						<td>{{ $b->post_hits }}</td>
    					<td>{{ Carbon\Carbon::parse($b->created_at)->format('D M Y i H:i:s') }}</td>
    					<td> 
							<div class="btn-edit">
								<a href="{{ route('removepostMember', $b->id) }}" class="btn-sm btn-danger" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="Delete Article" onclick="return confirm('Apakah Anda yakin menghapus post ini?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									<a target="_blank" href="{{ url($b->post_alias.'.phtml')}}" class="btn-sm btn-success"><i class="fa fa-eye"></i></a> &nbsp;
									{!! Form::open(['method' => 'POST','route' => ['postsedit']])  !!}
									<input type="hidden" name="id" value="{{ $b->id }}">
									<button type="submit" class="btn-sm btn-info "><i class="fa fa-pencil-square-o"></i></button>
									{!! Form::close() !!}
							</div>
						</td>
    				</tr>
    				@endforeach
					</tbody>
    			</table>
			</div>