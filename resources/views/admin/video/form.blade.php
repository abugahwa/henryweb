<div class="form-group">
	<label for="title">Video Title</label>
		{!! Form::text('title', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Video Title','class' => 'form-control')) !!}
</div>
<div class="form-group{{ $errors->has('video_category') ? ' has-error' : '' }}">
		<label for="video_category">Category Gallery</label>
		<select class="form-control m-bot15" name="video_category">
        @if($CateImg->count())
            @foreach($CateImg as $role)
                <option value="{{ $role->id }}">{{ $role->name }}</option>    
            @endforeach
        @endif

        </select>
			@if ($errors->has('video_category'))
				<span class="help-block">
					<strong>{{ $errors->first('video_category') }}</strong>
				</span>
			@endif
	</div>
<div class="form-group">
	<label for="title">Video Code On Youtube</label>
		{!! Form::text('video_id', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Video Code On Youtube','class' => 'form-control')) !!}
</div>
<div class="form-group">
	<label for="description_video">Video Description</label>
	{!! Form::textarea('description', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Video Description','class' => 'form-control')) !!}
</div>

<div style="clear:both;"></div>
<div class="ln_solid"></div>
 