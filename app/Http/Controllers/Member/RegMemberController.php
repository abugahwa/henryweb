<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use App\Models\MUserProfile;
use DB;

class RegMemberController extends Controller
{
    public function index()
    {
        return view('member.register');
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'email' => 'required|email|max:100|unique:users,email',
            'password' => 'required|min:6',
            'password' => 'required|string|min:6|confirmed',
        ]);
        DB::transaction(function () use ($request) {
            $users = User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => bcrypt($request['password']),
                'department_id' => 2
            ]);

           
            MUserProfile::create([
                'first_name'        => $request['first_name'],
                'last_name'         => $request['last_name'],
                'brand'             => $request['brand'],
                'mobile_phone'      => $request['mobile_phone'],
                'phone'             => $request['phone'],
                'address'           => $request['address'],
                'id_user'           => $users->id,
                'gender'            => $request['gender']
            ]);
        });

        $request->session()->flash('alert-success', 'was successful Add! please login');
        return redirect()->route('reg-member');
    }
}
