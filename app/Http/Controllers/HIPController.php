<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MPost;
use App\Classes\Posts;
use App\Classes\Videos;
use App\Classes\Photos;

class HIPController extends Controller
{

    protected $posts;
    protected $videos;
    protected $photos;

    public function __construct()
    {
        parent::__construct();
        $this->posts = new Posts;
        $this->videos = new Videos;
        $this->photos = new Photos;
    }

    public function index()
    {
        $scripts = 'home/hiplawfirm';

        $postsLawyers = $this->posts->getAll(['type' => 'profiles', 'sortby' => 'asc']);
        return view('henryweb.hiplawfirm', compact('scripts', 'postsLawyers'));
    }

    public function profile(Request $request, $alias)
    {
        $scripts    = 'home/profile';
        //return $alias;
        $postview   = MPost::where('post_alias', $alias)->where('post_status', 1)->where('post_type', 'profiles')->firstOrFail();
        $addHits    = $this->posts->addHits($postview->id);
        $postsBlog  = $this->posts->getAll(['limit' => 4, 'category' => $postview->post_category]);

        return view('henryweb.hip.profile', compact('postsBlog', 'postview', 'scripts'))->with(['postsLeft'=>$this->postsLeft, 'postsRight'=>$this->postsRight]);
    }
}
