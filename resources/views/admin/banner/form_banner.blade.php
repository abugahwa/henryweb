<div class="col-md-6">
    <div class="form-group{{ $errors->has('b_category') ? ' has-error' : '' }}">
		<label for="b_category">Category Banner</label>
		<select class="form-control m-bot15" name="b_category">
        @if($catbanner->count())
            @foreach($catbanner as $role)
                <option value="{{ $role->id }}" {{ $catid == $role->id ? 'selected="selected"' : '' }}>{{ $role->category_name }}</option>    
            @endforeach
        @endif

        </select>
			@if ($errors->has('b_category'))
				<span class="help-block">
					<strong>{{ $errors->first('b_category') }}</strong>
				</span>
			@endif
	</div>
	
	<div class="form-group{{ $errors->has('b_name') ? ' has-error' : '' }}">
		<label for="b_name">Banner Name</label>
		
			{!! Form::text('b_name', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Banner Name','class' => 'form-control')) !!}
			@if ($errors->has('b_name'))
				<span class="help-block">
					<strong>{{ $errors->first('b_name') }}</strong>
				</span>
			@endif
	</div>
	<div class="form-group{{ $errors->has('b_link') ? ' has-error' : '' }}">
		<label for="b_link">Link Banner</label>
		{!! Form::text('b_link', null, array('autofocus' => 'autofocus','placeholder' => 'Link Banner','class' => 'form-control')) !!}
		@if ($errors->has('b_link'))
    		<span class="help-block">
    			<strong>{{ $errors->first('b_link') }}</strong>
    		</span>
		@endif
	</div>
	<div class="form-group{{ $errors->has('b_image') ? ' has-error' : '' }}">
		<label for="b_image">Banner Image</label>
		{!! Form::file('b_image', null) !!}
			@if ($errors->has('b_image'))
				<span class="help-block">
					<strong>{{ $errors->first('b_image') }}</strong>
				</span>
			@endif
	</div>

</div>
<div class="col-md-6">
	<div class="form-group{{ $errors->has('b_status') ? ' has-error' : '' }}">
		<label for="b_status">Banner Status</label>
			{{ Form::selectRange('b_status',null, 1,$b_status,['class' => 'form-control']) }}
			@if ($errors->has('b_status'))
				<span class="help-block">
					<strong>{{ $errors->first('b_status') }}</strong>
				</span>
			@endif
	</div>
	<div class="form-group{{ $errors->has('b_sort_order') ? ' has-error' : '' }}">
		<label for="b_sort_order">Banner Sort Order</label>
			{{ Form::selectRange('b_sort_order',null, 20, $b_sort_order,['class' => 'form-control']) }}
			@if ($errors->has('b_sort_order'))
				<span class="help-block">
					<strong>{{ $errors->first('b_sort_order') }}</strong>
				</span>
			@endif
	</div>	
	
	 <div class="form-group{{ $errors->has('b_start_publishing') ? ' has-error' : '' }}">
		<label for="b_start_publishing">Start Publishing</label>
			{!! Form::text('b_start_publishing', null, ['id' => 'start_publishing','class' => 'form-control', 'placeholder' => 'Please Select']) !!}
			@if ($errors->has('b_start_publishing'))
				<span class="help-block">
					<strong>{{ $errors->first('b_start_publishing') }}</strong>
				</span>
			@endif
	</div>
	
	<div class="form-group{{ $errors->has('b_finish_publishing') ? ' has-error' : '' }}">
		<label for="b_finish_publishing">Finnish Publishing</label>
			{!! Form::text('b_finish_publishing', null, ['id' => 'finish_publishing','class' => 'form-control', 'placeholder' => 'Please Select']) !!}
			@if ($errors->has('b_finish_publishing'))
				<span class="help-block">
					<strong>{{ $errors->first('b_finish_publishing') }}</strong>
				</span>
			@endif
	</div>   
</div>
<div style="clear:both;"></div>
<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
			<a class="btn btn-primary cbutton2" href="{{ url()->previous() }}">Cancel</a>
			<?php 
				if(Request::is('edit-address/*')){;?>
            <button type="submit" class="btn btn-success cbutton">Update</button>
			<?php }else{ ?>
            <button type="submit" class="btn btn-success cbutton">Submit</button>
			<?php } ?>
        </div>
	</div>