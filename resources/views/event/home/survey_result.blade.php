
@if(!empty($survey))

	<div class="top">
	<h3>* {{ ucwords($event_name) }} {{ $years }} Fase {{ $fase }} *</h3>
	<hr />
</div>
	<br />
		<div style="margin-top: 50px;"></div>
			<div class="row">
				<?php
					foreach($survey as $c){
						$cat_names	= $c['cat_name'];
						$parent		= $c['id'];

						?>
				<div class="col-sm-12">
					@if($c['cat_name'] === 'Uncategory')

					@else
						<?php
							$cek_data = $scores->getCatScore($parent,$fase, $years);
						?>
						@if($cek_data->count() > 0)
					<br />
						<h3><?php echo $c['cat_name'];?></h3>
					<br />
						@endif
					@endif
				</div>
				<div style="clear:both;"></div>
				<?php
						$catcores		= $scores->getCatScore($parent,$fase, $years);
						foreach($catcores as $cc){
							$idcat		= $cc->id;
					?>
				<div class="col-sm-4">

					<table class="table table-bordered">
						<thead class="table-primary">
							<tr>
								<th class="text-center" colspan="3"><?php echo $cc['cat_name'];?></th>
								<tr>
								<th>Merek</th>
								<th>DPBI</th>
								<th>Popular</th>
							</tr>
						</thead>
						<?php
							$dscores		= $scores->getScore($idcat);
							foreach($dscores as $sc){
						?>
						<tr>
							<td>{{ $sc->merek }}</td>
							<td>{{ $sc->score }}%</td>
							<td class="text-danger">{{ $sc->top }}</td>
						</tr>
						<?php } ?>
					</table>
				</div>
					<?php } ?>
				<?php } ?>

			</div>

@else
    <div class="alert alert-primary" role="alert">
    No data Found !
    </div>
@endif
