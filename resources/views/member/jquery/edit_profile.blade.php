<script>
function edit_profile(id){
	$('#edit_profile')[0].reset();
	
	$.ajax({
			url : '{{ url("/get-profile-member")}}/' + id,
			type: "get",
			dataType: "JSON",
			success: function(data)
			{
				/*
				tinymce.get('post_text').setContent(data.post_text);
				*/
				$('[name="id"]').val(data.id);

				$('[name="first_name"]').val(data.first_name);
				$('[name="last_name"]').val(data.last_name);
				$('[name="company"]').val(data.company);
				$('[name="brand"]').val(data.brand);
				$('[name="mobile_phone"]').val(data.mobile_phone);
				$('[name="phone"]').val(data.phone);
				$('[name="address"]').val(data.address);
				
				$('#editProfile').modal('show'); 
				$('.modal-title').text('Edit '+data.title); 
				 $('#Edits').text('Edit');
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error get data from ajax');
			}
		
		});	
		
	$.get('get-gender-member/'+id,function(data){	
				$('#gender').html(data);
			});
}
</script>
<div id="editProfile" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
		{{ method_field('post') }}
		{!! Form::open(['method' => 'POST','class' => 'form-horizontal', 'id' => 'edit_profile'])  !!}
		{{ csrf_field() }}
      <div class="modal-body">
			<input type="hidden" name="id">
			<div class="col-md-6">
				<div class="form-group">
					<label>First Name</label>
					<input type="text" name="first_name" class="form-control">
				</div>
				<div class="form-group">
					<label>Last Name</label>
					<input type="text" name="last_name" class="form-control">
				</div>
				<div class="form-group">
					<label>Company</label>
					<input type="text" name="company" class="form-control">
				</div>
				<div class="form-group">
					<label>Brand</label>
					<input type="text" name="brand" class="form-control">
				</div>
				<div class="form-group">
					<label>Mobile Phone</label>
					<input type="text" name="mobile_phone" class="form-control">
				</div>
				<div class="form-group">
					<label>Phone</label>
					<input type="text" name="phone" class="form-control">
				</div>
			</div>
			<div class="col-md-6">
				<div class="form-group">
					<label>Address</label>
					<textarea class="form-control" name="address"></textarea>
				</div>
				<div class="form-group">
					<label>Gender</label>
					<select class="form-control" id="gender" name="gender"></select>
				</div>
			</div>
      </div>
	  <div style="clear:both;"></div>
      <div class="modal-footer">
        <button type="submti" class="btn btn-danger" id="Edits">Save</button>
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
      </div>
		{!! Form::close() !!}
    </div>

  </div>
</div>
<script>
$("form#edit_profile").submit(function(event){
		event.preventDefault();
		var formData = new FormData($(this)[0]);
		$('#Edits').text('saving...');
		$('#Edits').attr('disabled',true);
		$.ajax({
			url: "{{ url('/update-profiles') }}",
			type: 'POST',
			data: formData,
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			dataType: "JSON",
			success: function(data)
			{
				toastr.success('Successfully Edit Your Profile!', 'Success Alert', {timeOut: 5000});
				
				$('.group_user'+ data.id).replaceWith('<div class="group_user'+ data.id +'"><div class="form-group"><label class="col-md-4 control-label">First Name <span class="pull-right">:</span></label><label class="col-md-8 control-label">'+ data.first_name +'</label></div><div class="form-group"><label class="col-md-4">Last Name <span class="pull-right">:</span></label><label class="col-md-8">' + data.last_name + '</label></div><div class="form-group"><label class="col-md-4 control-label">Company <span class="pull-right">:</span></label><label class="col-md-8">' + data.company + '</label></div><div class="form-group"><label class="col-md-4 control-label">Brand Name <span class="pull-right">:</span></label><label class="col-md-8">' + data.brand + '</label></div><div class="form-group"><label class="col-md-4 control-label">Mobile Phone <span class="pull-right">:</span></label><label class="col-md-8">' + data.mobile_phone + '</label></div><div class="form-group"><label class="col-md-4 control-label">Phone <span class="pull-right">:</span></label><label class="col-md-8">' + data.phone + '</label></div><div class="form-group"><label class="col-md-4 control-label">Address<span class="pull-right">:</span></label><label class="col-md-8">' + data.address + '</label></div><a class="btn-sm btn-info" onclick="edit_profile(' + data.id + ')">Edit Profile</a></div>');
				
				$('#Edits').text('save'); //change button text
				$('#Edits').attr('disabled',false); //set button enable
				$('#editProfile').modal('toggle');
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error adding / update data');
				$('#Edits').text('save'); //change button text
				$('#Edits').attr('disabled',false); //set button enable 

			}
		
		});
		return false;
	});	
</script>