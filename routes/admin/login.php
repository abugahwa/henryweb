<?php

Route::get('login', [ 'as' => 'login', 'uses' => 'SigninController@form']);
Route::post('attempt', 'SigninController@attempt')->name('signin.attempt');
Route::get('signup', 'SignupController@form')->name('signup.form');
Route::post('store', 'SignupController@store')->name('signup.store');
Route::get('verify', 'SignupController@verify')->name('signup.verify');
Route::post('logouts', 'SignupController@getLogout')->name('logout');
