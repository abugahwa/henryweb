<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="https://www.franchiseglobal.com/images/favicon.ico"/>

    <title>Login Area</title>

    <!-- Bootstrap -->
    <link href="{{ asset('assets/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('assets/bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('assets/bower_components/gentelella/vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="{{ asset('assets/bower_components/gentelella/vendors/animate.css/animate.min.css') }}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{ asset('assets/bower_components/gentelella/build/css/custom.min.css') }}" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
			<form class="form-horizontal" role="form" method="POST" action="{{ route('signin.attempt') }}">
              {{ csrf_field() }}
              <h1>Login Form</h1>
              <div>
                <input name="email" type="email" class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input name="password" type="password" class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <button class="btn btn-default submit" type="submit">Log in</button>
                <a class="reset_pass" href="#">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">New to site?
                  <a href="#signup" class="to_register"> Create Account </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><img src="{{ asset('img/LOGO_INFOBRAND_TRANSPARANT-04.png') }}" width="100"> InfoBrand</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
			<form class="form-horizontal" role="form" method="POST" action="{{ route('signup.store') }}">
				<h1>Create Account</h1>
				{{ csrf_field() }}
				{{ method_field('post') }}
				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
					<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"  placeholder="Username" autofocus>
					@if ($errors->has('name'))
						<span class="help-block">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
					<input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}"  placeholder="Email">
					@if ($errors->has('email'))
						<span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
					<input id="password" type="password" class="form-control" name="password" placeholder="Password">
					@if ($errors->has('password'))
						<span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
					<input id="password-confirm" type="password" class="form-control" name="confirm_password" placeholder="Confirm Password">
					@if ($errors->has('confirm_password'))
						<span class="help-block">
							<strong>{{ $errors->first('confirm_password') }}</strong>
						</span>
					@endif
				</div>
				<div class="form-group">
					{!! Form::select('department_id', $cat, null, ['class' => 'form-control']) !!}
				</div>
				<button class="btn btn-default submit" type="submit">Submit</button>
				<div class="clearfix"></div>

				<div class="separator">
					<p class="change_link">Already a member ?
					  <a href="#signin" class="to_register"> Log in </a>
					</p>

					<div class="clearfix"></div>
					<br />

					<div>
					  <h1><img src="{{ asset('img/LOGO_INFOBRAND_TRANSPARANT-04.png') }}" width="100"> InfoBrand</h1>
					  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
					</div>
				</div>
			</form>
		  
		  
		  
            
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
