<?php

Route::get('add-menu-event', 'Admin\EventController@create')->name('event.add');
Route::get('get-parent', 'Admin\EventController@parents')->name('event.parent');
Route::post('save-menu', 'Admin\EventController@store')->name('event.save');
Route::get('list-menu-event', 'Admin\EventController@MenuEvent')->name('event.list');
Route::get('edit-menu-event/{id}', 'Admin\EventController@edit')->name('event.edit');
Route::post('update-menu-event/{id}', 'Admin\EventController@update')->name('event.update');
Route::get('search-menu-event', 'Admin\EventController@serachTable')->name('event.search');

Route::get('add-event', 'Admin\EventController@createEvent')->name('event.create');
Route::post('insert-event', 'Admin\EventController@storeEvent')->name('event.insert');
Route::get('event-list', 'Admin\EventController@EventIndex')->name('event.list2');
Route::get('get-text-event/{id}', 'Admin\EventController@ajaxText')->name('event.text');
Route::get('edit-event/{id}', 'Admin\EventController@editEvent')->name('eventedit');
Route::post('update-event/{id}', 'Admin\EventController@updateEvent')->name('eventupdate');

Route::get('add-survey', 'Admin\EventController@create_survey')->name('add_survey');
Route::post('save-survey-event', 'Admin\EventController@storeSurvey')->name('save_survey_event');
Route::get('list-survey-event', 'Admin\EventController@IndexSurvey')->name('surveylist');
Route::get('edit-survey-event/{id}', 'Admin\EventController@EditSurvey')->name('editsurveylist');
Route::post('update-survey-event/{id}', 'Admin\EventController@UpdateSurvey')->name('updatesurveylist');
