<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Classes\Posts;
use App\Classes\Videos;
use App\Classes\Banners;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $posts;
    protected $postsLeft;
    protected $postsLeft2;
    protected $postsRightEditorNotes;
    protected $postsRight;
    protected $postsRight1;
    protected $postsRight2;

    public function __construct()
    {
        $this->posts = new Posts;
        $this->videos = new Videos;
        $this->banners = new Banners;

        $this->postsLeft                = $this->posts->getAll(['limit' => 5, 'offset' => 15, 'category' => 3]);
        $this->postsLeft2               = $this->posts->getAll(['limit' => 5, 'category' => 7]);
        //$this->postsRightEditorNotes    = $this->posts->getAll(['limit' => 3, 'category' => 5]);
        //$this->postsRight2              = $this->posts->getAll(['limit' => 6, 'category' => 7]); // opini
        $this->postsRight               = $this->posts->getAll(['limit' => 5, 'category' => array(1,3,4,5)]); // brand update
        $this->postsRight1              = $this->videos->getAll(['limit' => 3]);
        $this->postsRight2              = $this->posts->getAll(['limit' => 3, 'category' => 29]); // Opportunities Update
        //$this->postsRightHome           = $this->posts->getAll(['limit' => 4, 'category' => 3, 'offset' => 6]); // bisnis update

        View::share('postsLeft', $this->postsLeft);
        View::share('postsLeft2', $this->postsLeft2);
        View::share('postsRight', $this->postsRight);
        View::share('postsRight1', $this->postsRight1);
        View::share('postsRight2', $this->postsRight2);

        View::share('banners', $this->banners);
    }

}
