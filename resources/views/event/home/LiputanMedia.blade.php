@if($liputanMedia->count() > 0)
<div class="row">
  <div class="col-md-6">
      <h4><strong>LIPUTAN</u></strong> MEDIA</h4>
      <div class="list1"></div>
      <br />
  </div>

  <div class="col-md-6">
    <div class="form-group pull-right" id="pagination_achievment">
      <a href="#LMedia" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
      <a href="#LMedia" data-slide="next"><i class="fas fa-chevron-right"></i></a>
    </div>
  </div>
</div>

<div style="clear:both;"></div>



<div class="row">
    <div id="LMedia" class="carousel slide" data-ride="carousel" data-interval="9000">
        <div class="carousel-inner row w-100 mx-auto" role="listbox">
          <?php $n = 0;?>
          @foreach($liputanMedia as $lMedia)
          <div class="carousel-item col-md-4  <?php if($n == 0) echo "active"; $n++; ?>">
              <div class="card">
                  <a class="card_achiev" href="{{ route('detailPost', [$alias,$lMedia->category_alias,$lMedia->post_alias])}}">
                      <img class="card-img-top" src="<?php echo route('cdn', $lMedia['post_img'] ); ?>" alt="Card image cap">
                  </a>
                  <div class="card-body">
                      <a href="{{ route('detailPost', [$alias,$lMedia->category_alias,$lMedia->post_alias])}}">{!! str_limit(strip_tags($lMedia->post_text), $limit = 80, $end = '...') !!}</a>
                  </div>
              </div>
          </div>
          @endforeach
        </div>

    </div>
    <div class="col-md-12">
      <br />
      @if($liputanMedia->count() > 0)
      <div class="form-group pull-right">
        <a href="{{ route('postcategories', [$alias,$lMedia->category_alias]) }}" class="float-right"><u>Index</u></a>
      </div>
      @endif
    </div>
</div>
@endif
