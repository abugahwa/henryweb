@extends('layouts.admin')
@section('content')
    <div class="page-title">
      <div class="title_left">
        <h3>Add Video</h3>
      </div>

      <div class="title_right">
        .
      </div>
    </div>
    <div class="clearfix"></div>

    @if(isset($video))
        {{ Form::model($video, ['route' => ['admin.videos.update', $video->id], 'method' => 'post', 'class' => 'form-horizontal form-label-left input_mask']) }}
    @else
        {{ Form::open(['route' => 'admin.videos.store', 'class' => 'form-horizontal form-label-left input_mask']) }}
    @endif

    <div class="row">
      <div class="col-md-8 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Video Detail</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />

              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Title</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {{ Form::text('title', null, array('class' => 'form-control')) }}
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Video ID</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {{ Form::text('video_id', null, array('class' => 'form-control')) }}
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {{ Form::textarea('description', null, array('class' => 'form-control')) }}
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">is Active?
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  {{ Form::checkbox('is_active', 1, null, array('class' => 'js-switch')) }}
                </div>
              </div>

          </div>
        </div>
      </div>

      <div class="col-md-4 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Publish</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

            Status: Draft <strong>Edit</strong> <br />
            Visibility: Public <strong>Edit</strong> <br />
            Publish: immediately <strong>Edit</strong> <br />

            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-12">
                <button type="button" class="btn btn-danger">Cancel</button>
                <button class="btn btn-primary" type="reset">Reset</button>
                <button type="submit" class="btn btn-success">Publish</button>
              </div>
            </div>

          </div>
        </div>
      </div>

  </div>
  {{ Form::close() }}

@endsection
