<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\MVideos;
use App\Models\MVideoCategory;
use App\Classes\Videos;

class VideosController extends Controller
{
  protected $videos;

  public function __construct()
  {
      $this->videos = new Videos;
  }

  public function index()

    {
        $videos = $this->videos->getAll(['paginate' => 10]);
        $data    = 0;
        $CateImg        				= $this->videos->catVideoAll();
        $catid						    = 0;

        return view('admin.video.videos', compact('videos','data', 'CateImg', 'catid'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.video.add_videos');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $video				= $this->videos->InsertVideos($request);
        $video->save();
        
		return response()->json($video); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data1                   = $this->videos->FindsVideosByid($id);
        $catid                  = $this->videos->GetVideoCategoryByid($id);

        $data = [
            'data1'     => $data1,
            'catid'     => $catid
        ];
        
		return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $video					= $this->videos->UpdateVideos($request, $id);
        
        $video->save();
        
		return response()->json($video);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
		$video					= $this->videos->DeleteVideos($request, $id);
		$data		= array('id' => $id);
        return response()->json($data);
    }
    public function combo_data($id)
	{
        $catid          				= $this->videos->GetVideoCategoryByid($id);
        $CateImg        				= $this->videos->catVideoAll();
        $json							= '';

		if($CateImg->count()){
            foreach($CateImg as $role){
				echo '<option value="'.$role->id.'" '.($catid == $role->id ?'selected="selected"':"").'>'.$role->name.'</option>';
            }
		}
	}

}
