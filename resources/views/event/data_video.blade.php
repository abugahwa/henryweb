<div class="form-group pull-right" id="video_rows">
	@if ($videos->onFirstPage())
		<button class="btn btn-success disabled">First</button>
	@else
		<a href="{{ $videos->previousPageUrl() }}" rel="prev" class="btn btn-success video_a"><i class='fa fa-angle-left'></i></a>
	@endif

	@if ($videos->hasMorePages())
		<a href="{{ $videos->nextPageUrl() }}" rel="prev" class="btn btn-success video_a"><i class='fa fa-angle-right'></i></a>
	@else
		<button class="btn btn-success disabled">End</button>
	@endif
</div>
<div style="clear:both;"></div>
<div class="row">
	@foreach($videos as $v)
	<div class="col-md-3">
		<a class="various fancybox fancybox.iframe" data-fancybox-type="iframe" href="https://www.youtube.com/embed/{{ $v->video_id }}" rel="gallery" title="{{ $v->caption_video }}">
			<img class="img-responsive img-thumbnail" src="https://img.youtube.com/vi/{{ $v->video_id }}/0.jpg" alt="" />
		</a>
	</div>
	@endforeach
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
<script>
$(document).ready(function(){
	$(".fancybox").fancybox();
	$("#nav").stick_in_parent();
	$(".sidebar").stick_in_parent({offset_top: 70});

});
</script>