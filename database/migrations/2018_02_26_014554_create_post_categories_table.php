<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('post_categories', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('category_name');
			$table->string('category_alias');
			$table->integer('parent_id');
			$table->integer('sort_order');
			$table->integer('category_status');
			$table->dateTime('date_added');
			$table->dateTime('last_modified')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('post_categories');
	}

}
