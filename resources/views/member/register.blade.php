@extends('member.layouts.member')

@section('content')
<div class="">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                <div class="flash-message">
							@foreach (['danger', 'warning', 'success', 'info'] as $msg)
							  @if(Session::has('alert-' . $msg))

							  <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
							  @endif
							@endforeach
						  </div>
                    <form class="form-horizontal" method="POST" action="{{ route('insertsmember') }}">
                    {{ csrf_field() }}
				    {{ method_field('post') }}
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
					<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
						<label class="control-label required" for="name"> First Name<sup style="color:red">*</sup></label>
						<input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                        @if ($errors->has('name'))
                            span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
					<div class="form-group">
						<label class="control-label required" for="name"> Last Name<sup style="color:red">*</sup></label>
						{!! Form::text('last_name', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Enter Your Last Name','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
					<div class="form-group">
						<label class="control-label required" for="name"> Company<sup style="color:red">*</sup></label>
						{!! Form::text('company', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Enter Your Company Name','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
					<div class="form-group">
						<label class="control-label required" for="name"> Brand<sup style="color:red">*</sup></label>
						{!! Form::text('brand', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Enter Your Brand Name','class' => 'form-control')) !!}
					</div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
					<div class="form-group">
						<label class="control-label required" for="name"> Phone<sup style="color:red">*</sup></label>
						{!! Form::text('phone', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Enter Your Phone','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
					<div class="form-group">
						<label class="control-label required" for="name"> Mobile Phone<sup style="color:red">*</sup></label>
						{!! Form::text('mobile_phone', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Enter Your Mobile Phone','class' => 'form-control')) !!}
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
					<div class="form-group">
						<label class="control-label required" for="phone">Address<sup style="color:red">*</sup></label>
						{!! Form::textarea('address', null, array('required' => 'required', 'rows' => '5', 'autofocus' => 'autofocus','placeholder' => 'Enter Your Address','class' => 'form-control')) !!}
                    </div>
                </div>
				@if (empty($gender))
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
					<div class="form-group">
						<label class="control-label required" for="phone">Address<sup style="color:red">*</sup></label>
                        <select class="form-control" name="gender">
							<option value="male">Male</option>
							<option value="female">Female</option>
						
						</select>
                    </div>
                </div>
				@else
				<input type="hidden" name="gender" value="{{$gender}}">
				@endif
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
					<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
						<label class="control-label required" for="email">Email<sup style="color:red">*</sup></label>
						<input id="email" name="email" value="{{ old('email') }}" type="email" class="form-control" placeholder="Enter Email Address" required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
					<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						<label class="control-label required" for="password">Password<sup style="color:red">*</sup></label>
						<input id="password" name="password" type="password" class="form-control" placeholder="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
					<div class="form-group">
						<label class="control-label required" for="password-confirm">Confirm Password<sup style="color:red">*</sup></label>
						<input id="password-confirm" name="password_confirmation" type="password" class="form-control" placeholder="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Register
                    </button>
                </div>
            </div>
            </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
