<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Usercategory;

class SigninController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function form()
    {
		if(Auth::check()){
			return redirect()->intended('/home');
		}else{
			$cat		 	= Usercategory::all()->pluck('name');
        return view('admin.login', compact('cat'));
		}
    }
    public function attempt(Request $request)
    {
        $this->validate($request, [
            'email' => 'email|exists:users,email',
            'password' => 'required',
        ]);
        $attempts = [
            'email' => $request->email,
            'password' => $request->password,
            'status' => 'activated',
        ];
        if (Auth::attempt($attempts, (bool) $request->remember)) {
            return redirect()->intended('/admin');
        }
        return redirect()->back();
    }
    public function getLogout()
    {
         Auth::logout();
         return Redirect::route('/');
    }
}
