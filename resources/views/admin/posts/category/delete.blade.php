<script>
	function delete_data(id)
	{
		
		$.ajax({
			url : '{{ url("/delete-category-posts") }}/' + id,
			type: "get",
			dataType: "JSON",
			success: function(data)
			{
				toastr.success('Successfully Delete Data!', 'Success Alert', {timeOut: 5000});
				$('.item' + data['id']).remove();
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error get data from ajax');
			}
		
		});	
	}
</script>