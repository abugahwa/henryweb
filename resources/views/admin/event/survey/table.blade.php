<div class="table-responsive">
	<table class="table table-striped">
		<thead bgcolor="#9853bb" style="color: #fff;">
			<tr>
				<th>No.</th>
    			<th width="400px">Event Name</th>
    			<th>Years</th>
				<th>Fase</th>
    			<th>Action</th>
    		</tr>
    	</thead>
		<tbody>
    	@foreach($survey as $key => $b)
			<tr>
				<td>{{ $survey->firstItem() + $key }}</td>
    			<td>{{ $b->event_name }}</td>
    			<td>{{ Carbon\Carbon::parse($b->event_date)->format('D M Y i H:i:s') }}</td>
				<td>{{ $b->fase }}</td>
    			<td>
					<a href="{{ route('editsurveylist', $b->id)}}">Edit</a>
				</td>
    		</tr>
    	@endforeach
    	</tbody>
    </table>
</div>
@if($survey->count() === 0)
	<div class="alert alert-danger">
		No data found.
	</div>
@endif
{{ $survey->appends(\Request::except('_token'))->render() }}
