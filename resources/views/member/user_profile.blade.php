@extends('member.layouts.member')
@section('content')
<style>
.user_group label{
	padding: 2px !important;
	font-weight: normal;
}
.main .col-md-4 img{
    width: 71%;
}
.main .col-md-4 a{
	cursor: pointer;
}
.main .btn-sm{
	cursor: pointer;
}
#editProfile .form-group{
	padding-left: 10px;
	padding-right: 10px;
}
.small-box {
    position: relative;
    display: block;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    margin-bottom: 15px;
}
.bg-aqua {
    background-color: #00c0ef !important;
}
.small-box > .inner {
    padding: 10px;
}
.small-box .icon {
    position: absolute;
    top: auto;
    bottom: 5px;
    right: 5px;
    z-index: 0;
    font-size: 90px;
    color: rgba(0, 0, 0, 0.15);
}
.small-box > .small-box-footer {
    position: relative;
    text-align: center;
    padding: 3px 0;
    color: #fff;
    color: rgba(255, 255, 255, 0.8);
    display: block;
    z-index: 10;
    background: rgba(0, 0, 0, 0.1);
    text-decoration: none;
}
.small-box h3, .small-box p {
    z-index: 5px;
}
.small-box p {
    font-size: 15px;
}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="main">
			<div class="row">
				
				<div class="col-md-4">
					<div id="tabs">
						<div class="item{{ $user->id }}">
							<a onclick="edit_image({{ \Auth::user()->id }})">
								<img src="{{ asset($user->avatar)}}" class="img-circle center-block">
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="user_group group_user{{ $user->id }}">
						<div class="form-group">
							<label class="col-md-4 control-label">First Name <span class="pull-right">:</span></label>
							<label class="col-md-8 control-label">{{ $user->first_name }}</label>
						</div>
						<div class="form-group">
							<label class="col-md-4">Last Name <span class="pull-right">:</span></label>
							<label class="col-md-8">{{ $user->last_name }}</label>
						</div>
						<div class="form-group">
							<label class="col-md-4">Company <span class="pull-right">:</span></label>
							<label class="col-md-8">@if(!empty($user->company)){ {{ $user->company }} @else - @endif</label>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Brand Name <span class="pull-right">:</span></label>
							<label class="col-md-8">{{ $user->brand }}</label>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Mobile Phone <span class="pull-right">:</span></label>
							<label class="col-md-8">{{ $user->mobile_phone }}</label>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Phone <span class="pull-right">:</span></label>
							<label class="col-md-8">{{ $user->phone }}</label>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Address <span class="pull-right">:</span></label>
							<label class="col-md-8">{{ $user->address }}</label>
						</div>
						<div class="form-group">
						<a class="btn-sm btn-info" onclick="edit_profile({{ $user->id }})">Edit Profile</a>
						<a href="{{ route('resetpass', csrf_token()) }}" class="btn-sm btn-info">Edit Password</a>
						</div>
					</div>
					
				</div>

				
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@include('member.jquery.edit_image')
@include('member.jquery.edit_profile')
@endsection