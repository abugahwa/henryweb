@foreach($categories as $category)
<div class="card" id="categories_card">
    <div class="row ">
        <div class="col-md-4">
            <a href="{{ route('detailPost', [$alias,$category->category_alias,$category->post_alias])}}">
                <img src="<?php echo route('cdn', $category['post_img'] ); ?>" class="w-100">
            </a>
        </div>
        <div class="col-md-8 px-3">
            <div class="card-block px-3">
                <h4 class="card-title"><a href="{{ route('detailPost', [$alias,$category->category_alias,$category->post_alias])}}">{{ $category->post_title }}</a></h4>
                <p class="card-text">{!! str_limit(strip_tags($category->post_text), $limit = 280, $end = '...') !!}</p>
            </div>
        </div>
    </div>
  </div>
  @endforeach
  {{ $categories->appends(\Request::except('_token'))->render("pagination::bootstrap-4") }}
