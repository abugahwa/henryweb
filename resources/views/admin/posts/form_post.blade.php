<div class="col-md-6">
	<div class="form-group{{ $errors->has('post_title') ? ' has-error' : '' }}">
		<label for="post_title">Title</label>
		
			{!! Form::text('post_title', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Title Post','class' => 'form-control')) !!}
			@if ($errors->has('post_title'))
				<span class="help-block">
					<strong>{{ $errors->first('post_title') }}</strong>
				</span>
			@endif
	</div>
	<div class="form-group{{ $errors->has('post_lead_in') ? ' has-error' : '' }}">
		<label for="post_lead_in">Lead in</label>
		{!! Form::text('post_lead_in', null, array('autofocus' => 'autofocus','placeholder' => 'Lead In','class' => 'form-control')) !!}
		@if ($errors->has('post_lead_in'))
    		<span class="help-block">
    			<strong>{{ $errors->first('post_lead_in') }}</strong>
    		</span>
		@endif
	</div>
	<div class="form-group{{ $errors->has('post_lead_in') ? ' has-error' : '' }}">
		<label for="post_lead_in">Image</label>
		{!! Form::file('post_img', null) !!}
			@if ($errors->has('post_lead_in'))
				<span class="help-block">
					<strong>{{ $errors->first('post_lead_in') }}</strong>
				</span>
			@endif
	</div>
	<div class="form-group{{ $errors->has('post_img_caption') ? ' has-error' : '' }}">
		<label for="post_img_caption">Image Caption</label>
			{!! Form::text('post_img_caption', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Image Caption','class' => 'form-control')) !!}
			@if ($errors->has('post_img_caption'))
				<span class="help-block">
					<strong>{{ $errors->first('post_img_caption') }}</strong>
				</span>
			@endif
	</div>
	<!--
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		<label for="name">Category Post</label>
			{!! Form::select('post_category', $catpost, $catid, ['id' => 'post_category','class' => 'form-control', 'placeholder' => 'Please Select']) !!}
			@if ($errors->has('name'))
				<span class="help-block">
					<strong>{{ $errors->first('name') }}</strong>
				</span>
			@endif
	</div>
	-->
	<div class="form-group">
		<label>Parent Menu</label>
		<select class="form-control" name="post_category">
			<option value="0">Pilih</option>
			@if($catpost->count())
				@foreach($catpost as $role)
					
					<option value="{{ $role->id }}" {{ $cat == $role->id ? 'selected="selected"' : '' }}><strong>{{ $role->category_name }}</strong></option>
					@foreach($child->GetCatByParentNull($role->id) as $categorys)
						<option class="dropdown-item" value="{{ $categorys->id }}" {{ $cat == $categorys->id ? 'selected="selected"' : '' }}><strong>&nbsp;&nbsp;{{ $categorys->category_name }}</strong></option>
					@endforeach
				@endforeach
			@endif
		</select> 

	</div>
	 <div class="form-group{{ $errors->has('start_publishing') ? ' has-error' : '' }}">
		<label for="start_publishing">Start Publishing</label>
			{!! Form::text('start_publishing', $datenow, ['id' => 'start_publishing','class' => 'form-control', 'placeholder' => 'Please Select']) !!}
			@if ($errors->has('name'))
				<span class="help-block">
					<strong>{{ $errors->first('start_publishing') }}</strong>
				</span>
			@endif
	</div>
	<!--
	<div class="form-group{{ $errors->has('finish_publishing') ? ' has-error' : '' }}">
		<label for="finish_publishing">Finnish Publishing</label>
			{!! Form::text('finish_publishing', null, ['id' => 'finish_publishing','class' => 'form-control', 'placeholder' => 'Please Select']) !!}
			@if ($errors->has('finish_publishing'))
				<span class="help-block">
					<strong>{{ $errors->first('finish_publishing') }}</strong>
				</span>
			@endif
	</div>
	-->
	<div class="form-group{{ $errors->has('post_tags') ? ' has-error' : '' }}">
		<label for="post_tags">Post Tags</label>
			{!! Form::text('post_tags', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Post Tags','class' => 'form-control')) !!}
			@if ($errors->has('post_tags'))
				<span class="help-block">
					<strong>{{ $errors->first('post_tags') }}</strong>
				</span>
			@endif
	</div>
</div>
<div class="col-md-6">
   
	{{ Form::textarea('post_text', null, ['class' => 'field','id' => 'technig']) }}
</div>
<div style="clear:both;"></div>
<div class="ln_solid"></div>
	<div class="form-group">
		<div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
			<a class="btn btn-primary cbutton2" href="{{ url()->previous() }}">Cancel</a>
			<?php 
				if(Request::is('edit-address/*')){;?>
            <button type="submit" class="btn btn-success cbutton">Update</button>
			<?php }else{ ?>
            <button type="submit" class="btn btn-success cbutton">Submit</button>
			<?php } ?>
        </div>
	</div>