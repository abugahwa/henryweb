<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Social\FacebookServiceProvider;
use GuzzleHttp\Exception\ClientException;
use League\OAuth1\Client\Credentials\CredentialsException;

use Socialite;
use Auth;
use Exception;

class AuthController extends Controller
{
	protected $providers;

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
	public function handleProviderCallback($provider)
    {
        try {
            //$user = Socialite::driver($provider)->User();
            $users = Socialite::driver($provider)
                ->fields([
					'id',
                    'name', 
                    'first_name', 
                    'last_name', 
                    'email', 
                    'gender', 
                    'verified'
                ]);
				$user = $users->user();
        } catch (Exception $e) {
            return redirect('/login-member');
			
        }
 
        $authUser = $this->findOrCreateUser($user,$provider);
 
        Auth::login($authUser, true);
		
			$id		= $user->user['id'];
			$name		= $user->user['name'];
			$first_name		= $user->user['first_name'];
			$last_name		= $user->user['last_name'];
			$email		= $user->user['email'];
			$gender		= $user->user['gender'];

        return redirect()->route('homemember')
						->with('id', $id)
						->with('name', $name)
						->with('last_name', $last_name)
						->with('email', $email)
						->with('gender', $gender)
						->with('first_name', $first_name);
    }
	private function findOrCreateUser($provideruser,$provider)
    {
        $authUser = User::where('email', $provideruser->email)->first();
 
        if ($authUser){
            return $authUser;
        }
        return User::create([
            'name' => $provideruser->name,
            'email' => $provideruser->getEmail(),
            'department_id' => 3,
            'status' => 'activated',
            'social_id' => $provideruser->getId(),
			'social_provider' => $provider,
            'avatar' => $provideruser->getAvatar()
        ]);
    }
	public function attempt(Request $request)
    {
        $this->validate($request, [
            'email' => 'email|exists:users,email',
            'password' => 'required',
        ]);
        $attempts = [
            'email' => $request->email,
            'password' => $request->password,
            //'department_id' => 3,
            'status' => 'activated',
        ];
        if (Auth::attempt($attempts, (bool) $request->remember)) {
            return redirect()->intended('/members-infobrand');
        }
        return redirect()->back();
    }
}