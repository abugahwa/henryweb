
<div class="form-group">
	<label>Parent Menu</label>
	<select class="form-control" name="menu_id">
		<option value="0">Pilih</option>
		@if($categories->count())
			@foreach($categories as $role)
				
				<option disabled value="{{ $role->id }}" {{ $cat == $role->id ? 'selected="selected"' : '' }}><strong>{{ $role->menu_name }}</strong></option>
				@foreach($child->byParentMenu($role->id) as $categorys)
					<option class="dropdown-item" value="{{ $categorys->id }}" {{ $cat == $categorys->id ? 'selected="selected"' : '' }}><strong>&nbsp;&nbsp;{{ $categorys->menu_name }}</strong></option>
				@endforeach
			@endforeach
		@endif
	</select>

</div>
<div class="form-group">
	<label>Description</label>
	{{ Form::textarea('text_event', null, ['class' => 'field','id' => 'technig']) }}
</div>
<div class="form-group">
	<label>Sytle FIle</label>
	{!! Form::textarea('css', null, array('autofocus' => 'autofocus','placeholder' => 'Description','class' => 'form-control')) !!}
</div>
<div class="form-group">
	<button type="submit" class="btn btn-success cbutton">Add</button>
</div>