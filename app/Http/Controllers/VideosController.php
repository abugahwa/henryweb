<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classes\Videos;

class VideosController extends Controller
{

  protected $videos;

  public function __construct()
  {
      parent::__construct();
      $this->videos = new Videos;
  }

  public function index()
  {
    //$videos = $this->videos->getAll(['paginate' => 10]);
    $catvideo   = $this->videos->catVideoAlljoin();
    $videos     = $this->videos->getAll(['sortby' => 'desc', 'limit' => 30, 'paginate' => 30]);
    $most       = $this->videos->getAll(['sortby' => 'hits', 'limit' => 8]);

    return view('videos.index', compact('videos', 'catvideo', 'most'));
  }

  public function view($id)
  {
    $video    = $this->videos->getById($id);
    $addHits  = $this->videos->addHits($video->id);
    $videos   = $this->videos->getAll(['sortby' => 'rand','limit' => 8]);
    $most     = $this->videos->getAll(['sortby' => 'hits', 'limit' => 8]);

    return view('videos.view', compact('videos', 'video', 'most'));
  }
  public function detail($id)
  {
      $videos   = $this->videos->videobycat($id,0,20);
      $nm_cat   = $this->videos->GetCatname($id);
      $most     = $this->videos->getAll(['sortby' => 'hits', 'limit' => 9]);
      return view('videos.category', compact('videos', 'nm_cat', 'most'));
  }

}
