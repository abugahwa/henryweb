<?php

namespace App\Classes;

use Illuminate\Http\Request;
use App\Models\MPost;
use DB;
use Carbon;

class Report extends BaseClass {
	
	public function ViewerByuser()
	{
		$dateS 					=  Carbon\Carbon::now()->startOfMonth()->subMonth(12);
		$dateE 					=  Carbon\Carbon::now()->startOfMonth(); 
		
		$startMonth				= $dateS->subMonth()->format('F Y');
		$endMonth				= $dateE->format('F Y');
		
		$newprospeks 			= MPost::select(DB::raw("DATE_FORMAT(created_at,'%M') as months"),DB::raw('sum(post_hits) as sums'))
								->where('post_status','1')
								->where('post_author', \Auth::user()->id)
								->whereBetween('created_at',[$dateS,$dateE])
								->orderBy('created_at')
								->groupBy('months')
								->get();
		
		$chrt				= '';
		
		foreach($newprospeks as $b){
			$chrt	.="
			{
					name: '".$b->months ."',
					y: ".$b->sums ."
			},
			";
		}
		return $chrt;
	}
}