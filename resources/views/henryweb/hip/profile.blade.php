@extends('henryweb.layouts.frontend')
@section('pageTitle', $postview->post_title)

@section('metaTags')
<!-- Gahwa SEO Attack!! -->
<meta name="description" content="{{ str_limit(strip_tags($postview->post_text), $limit = 150, $end = '...') }}"/>
<meta name="keywords" content="{{ $postview->post_tags }}"/>
<meta name="author" content="{{ $postview->post_author }}"/>
<link rel="canonical" href="<?php echo env('APP_URL') . "/" . $postview->post_alias .".phtml"; ?>"/>
<link rel="publisher" href="{{ env('APP_NAME') }}"/>
<meta property="og:locale" content="id_ID"/>
<meta property="og:type" content="article"/>
<meta property="og:title" content="{{ $postview->post_title }}"/>
<meta property="og:description" content="{{ str_limit(strip_tags($postview->post_text), $limit = 150, $end = '...') }}"/>
<meta property="og:url" content="<?php echo env('APP_URL') . "/" . $postview->post_alias .".phtml"; ?>"/>
<meta property="article:publisher" content="https://www.facebook.com/officialhenryindraguna/"/>
<meta property="article:author" content="https://www.facebook.com/officialhenryindraguna/"/>

<?php
$tags = explode(",", $postview->post_tags);
foreach($tags as $tag) { ?>
<meta property="article:tag" content="<?php echo trim($tag); ?>"/>
<?php } ?>

<meta property="article:section" content="article"/>
<meta property="article:published_time" content="<?php echo date("c", strtotime($postview->start_publishing)); ?>"/>
<meta property="article:modified_time" content="<?php echo date("c", strtotime($postview->finish_publishing)); ?>"/>
<meta property="og:updated_time" content="<?php echo date("c", strtotime($postview->finish_publishing)); ?>"/>
<meta property="fb:admins" content=""/>
<meta property="og:image" content="<?php echo url($postview->post_img); ?>" alt="<?php echo $postview->post_title; ?>"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:description" content="{{ str_limit(strip_tags($postview->post_text), $limit = 150, $end = '...') }}"/>
<meta name="twitter:title" content="{{ $postview->post_title }}"/>
<meta name="twitter:site" content="@henryindraguna2"/>
<meta name="twitter:image" content="<?php echo url($postview->post_img); ?>" alt="<?php echo $postview->post_title; ?>"/>
<meta name="twitter:creator" content="@henryindraguna2"/>
<!-- End :) -->
@endsection

@section('content')

    <div class="bg-navbar"></div>

    <div class="container">
      <div class="row">

        <div class="col-md-8 order-1 order-md-2 postView">
            <br />
            <h1 class="title" style="text-align:center;"><?php echo $postview->post_title; ?></h1>
            <!--<p class="detail">Posted by: <i class="fa fa-user"></i>
              <span itemprop="author" itemscope="" itemtype="http://schema.org/Person">
								<strong itemprop="name" style="margin-right: 10px;"><?php echo $postview->author->name; ?></strong>
							</span>
              <span style="margin-right: 10px;"><i class="fa fa-clock"></i> <?php echo date("d-m-Y H:i", strtotime($postview->start_publishing)); ?> WIB</span>
              <span style="margin-right: 10px;"><i class="fa fa-eye"></i> <?php echo $postview->post_hits; ?> viewer</span>
            </p>-->

            <img class="img-responsive" style="height:300px;text-align:center;margin:0px auto; display:block;" src="<?php echo url($postview->post_img); ?>" alt="<?php echo $postview->post_title; ?>" />
            <br />
            <!--<small style="float:right;"><?php echo $postview->post_img_caption; ?></small>-->

            <div class="clearfix row-space"></div>

            <?php echo $postview->post_text; ?>

            <hr />

            <div class="container">
                <div class="row row-space">
                    <p>
                        <?php foreach(explode(',', $postview->post_tags) as $tag) { ?>
                        <span class="badge badge-dark"><a style="color:#FFFFFF;" href="{{ route('posts.tag', str_slug($tag, '-'))}}"><?php echo $tag; ?></a></span>
                        <?php } ?>
                    </p>
                </div>
            </div>

            <hr />

            <div class="containerr">
            <div class="row row-space text-center">
                <!-- AddToAny BEGIN -->
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style" style="display:block;margin:0px auto;">
                <a class="a2a_button_facebook"></a>
                <a class="a2a_button_twitter"></a>
                <a class="a2a_button_whatsapp"></a>
                <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                </div>
                <script async src="https://static.addtoany.com/menu/page.js"></script>
                <!-- AddToAny END -->
            </div>
            </div>

            <div class="container my-4">
              <div class="row">
              <div class="col-md-12">
                <div class="alert alert-secondary text-center" role="alert">
                  Untuk informasi layanan konsultasi hukum: <br /><strong>0815-7300-0000</strong>
                </div>
              </div>
              </div>
            </div>

<!--            <br />
            <h2>Article Related</h2>
            <br />

            <?php foreach($postsBlog as $post) { ?>
            <div class="media">
              <div class="media-left">
                  <img class="media-object" src="<?php echo $post['post_img']; ?>" alt="<?php echo $post['post_title']; ?>" width="120" height="120" />
              </div>
              <div class="media-body">
                  <p><a href="<?php echo url($post['post_alias'].".phtml"); ?>"><?php echo $post['post_title']; ?></a><br />
                  <small>{{ str_limit(strip_tags($post['post_text']), $limit = 150, $end = '...') }}</small></p>
              </div>
            </div>
            <hr />
            <?php } ?>
-->
        </div>

        <div class="col-md-4 order-3 py-4">
          <div class="sticky_column" data-sticky_column="">
              @include('shared.rightColumn')
          </div>
        </div>
      </div>
    </div>

@endsection
