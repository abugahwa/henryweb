<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventSurver extends Model
{
    protected $table    = 'event_survey';
    protected $fillabel = ['id','event_name','fase','event_date','survey_results','created_at','updated_at'];
}
