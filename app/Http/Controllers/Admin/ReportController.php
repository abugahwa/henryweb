<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;

use App\Models\MPost;
use App\User;
use App\Usercategory;

use App\Classes\Posts;

use DB;
use Carbon;
use PDF;

class ReportController extends Controller
{
	protected $posts;
	
	public function __construct()
    {
        $this->posts = new Posts;
		
    }
    public function index(Request $request)
	{
		$datenow 			= Carbon\Carbon::now();
		$report				= $this->posts->PostsByuser();
		
		
		return view('admin.report.index', compact('report'));
	}
	public function searchPost(Request $request)
	{
		if($request->ajax() ){
			$mulai			= $request->start;
			$akhir			= $request->end;
			$report			= $this->posts->SearchPostBydate($request);
			return view('admin.report.search_report', compact('report','mulai','akhir'));
		}
	}
	public function printOut(Request $request)
	{
		$mulai				= $request->mulai;
		$akhir				= $request->akhir;
		$post				= MPost::where('post_author', \Auth::user()->id)
							->whereBetween('created_at', array($mulai, $akhir))
							->paginate(30);
		$col_0 = "";
		$col_1 = "";
		$col_2 = "";
		$col_3 = "";
		$col_4 = "";
		
		$user				= User::where('id', \Auth::user()->id)->get();
		foreach($user as $us){
			$names				= $us->name;
			$jabatan			= $us->department_id;
		}
		//return json_encode($jabatan);
		$dept					= Usercategory::where('id', $jabatan)->first()->name;
		foreach($post as $key => $b){
			$no					= $post->firstItem() + $key;
			$txt				= $b->post_title;
			$title 				= Str::words($txt, $words = 5, $end = '...');
			
			$category			= $b->PostCategories->category_name;
			$hits				= $b->post_hits;
			$dates				= Carbon\Carbon::parse($b->created_at)->format('D M Y i H:i:s');
			
			$col_0		 		= $col_0.$no."\n";
			$col_1		 		= $col_1.$title."\n";
			$col_2			 	= $col_2.$category."\n";
			$col_3		 		= $col_3.$hits."\n";
			$col_4		 		= $col_4.$dates."\n";
		}
		
		PDF::SetHeaderData(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		PDF::SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 034', PDF_HEADER_STRING);
		
		PDF::setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
		PDF::SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		PDF::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		PDF::setImageScale(PDF_IMAGE_SCALE_RATIO);
		PDF::AddPage();
		
		PDF::Image(asset('assets/images/logo2.jpg'),156,10,50,'');
		PDF::Cell(1);
		PDF::SetFont('Times','B','12');
		PDF::Image(asset('assets/images/logo-infobrand.png'),5,10,60,'L');

		PDF::SetLineWidth(1);
		PDF::Line(10,30,202,30);
		PDF::SetLineWidth(0);
		PDF::Line(10,31,202,31);
		
		PDF::ln(10);
		PDF::SetFont('Times','B','14');
		PDF::Cell(0,5,'Report Article',0,1,'C');
		PDF::ln();
		
		PDF::SetFont('Times','B','10');
		PDF::MultiCell(30, 5, 'Name', 0, 'L', 0, 0, '', '', true, 0, false, true, 0);
		PDF::MultiCell(50, 5, ': ' .$names, 0, 'L', 0, 0, '', '', true, 0, false, true, 0);
		PDF::ln();
		PDF::MultiCell(30, 5, 'Department', 0, 'L', 0, 0, '', '', true, 0, false, true, 0);
		PDF::MultiCell(50, 5, ': ' .$dept, 0, 'L', 0, 0, '', '', true, 0, false, true, 0);
		
		PDF::ln();
		
		PDF::SetFont('Times','','10');
		PDF::Cell(10,10.5,'No',1,0,'C');
		PDF::Cell(70,10.5,'Title',1,0,'C');
		PDF::Cell(25,10.5,'Category',1,0,'C');
		PDF::Cell(15,10.5,'Viewer',1,0,'C');
		PDF::Cell(55,10.5,'Date',1,0,'C');
		
		PDF::ln();
		PDF::SetFont('Times','','9');
		
		PDF::MultiCell(10, 10, $col_0, 1, 'C', 0, 0, '', '', true, 0, false, true, 0);
		PDF::MultiCell(70, 10, $col_1, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
		PDF::MultiCell(25, 10, $col_2, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
		PDF::MultiCell(15, 10, $col_3, 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
		PDF::MultiCell(55, 10, $col_4 , 1, 'L', 0, 0, '', '', true, 0, false, true, 0);
		
		PDF::Output('SamplePDF.pdf', 'I');
	}
}
