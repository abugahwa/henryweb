<div class="table-responsive">
	<table class="table table-striped">
		<thead bgcolor="#9853bb" style="color: #fff;">
			<tr>
				<th>No.</th>
    			<th width="400px">Post Title</th>
    			<th>Post Category</th>
				<th>Viewer</th>
    			<th>Publish</th>
    			<th>Action</th>
    		</tr>
    	</thead>
		<tbody>
    	@foreach($post as $key => $b)
			<tr>
				<td>{{ $post->firstItem() + $key }}</td>
    			<td>{{ $b->post_title }}</td>
    			<td>@if(!empty($b->PostCategories->category_name)) {{ $b->PostCategories->category_name }} @else Uncategorized @endif</td>
				<td>{{ $b->post_hits }}</td>
    			<td>{{ Carbon\Carbon::parse($b->created_at)->format('D M Y i H:i:s') }}</td>
    			<td>
					<div class="btn-edit">

						<form id="edit-post" action="{{ route('posts.edit') }}" method="POST" style="display: block;">
							{{ csrf_field() }}
								<input type="hidden" name="id_post" value="{{ $b->id }}">
								<button class="btn btn-primary" href="#" role="button" type="submit">
								<i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                        </form>

						<a href="{{ route('posts.delete', $b->id) }}" class="btn-sm btn-danger" data-toggle="tooltip" data-placement="top" data-trigger="hover" title="Delete Article" onclick="return confirm('Apakah Anda yakin menghapus post ini?')"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
						<!--<a href="{{ route('posts.edit', $b->id) }}" class="btn-sm btn-info" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>-->
					</div>
				</td>
    		</tr>
    	@endforeach
    	</tbody>
    </table>
</div>
@if($post->count() === 0)
	<div class="alert alert-danger">
		No data found.
	</div>
@endif
{{ $post->appends(\Request::except('_token'))->render() }}
