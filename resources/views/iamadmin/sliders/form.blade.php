@extends('iamadmin.layouts.dashboard')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Sliders</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Sliders</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">@if(!isset($data)) {{'Add Post'}} @else {{'Edit Post'}} @endif</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
      </div>
      <div class="card-body">

@if(isset($data))
  {!! Form::model($data, ['method' => 'POST','route' => ['admin.sliders.update'],'files' => true, 'class' => 'form-horizontal'])  !!}
  <input type="hidden" name="id" value="{{ $data->id }}">
@else
  {!! Form::open(['method' => 'POST','route' => ['admin.sliders.store'],'files' => true, 'class' => 'form-horizontal'])  !!}
@endif
<input type="hidden" name="id_user" value="<?php echo \Auth::user()->id;?>">
<input type="hidden" name="post_type" value="sliders">

<div class="col-md-6">
	<div class="form-group{{ $errors->has('post_title') ? ' has-error' : '' }}">
		<label for="post_title">Title</label>

			{!! Form::text('post_title', null, array('required' => 'required', 'autofocus' => 'autofocus','placeholder' => 'Title Post','class' => 'form-control')) !!}
			@if ($errors->has('post_title'))
				<span class="help-block">
					<strong>{{ $errors->first('post_title') }}</strong>
				</span>
			@endif
	</div>

	<div class="form-group{{ $errors->has('post_img') ? ' has-error' : '' }}">
		<label for="post_img">Image</label>
		{!! Form::file('post_img', null) !!}
			@if ($errors->has('post_img'))
				<span class="help-block">
					<strong>{{ $errors->first('post_img') }}</strong>
				</span>
			@endif
	</div>
</div>
<div class="col-md-6">
<label for="description">Image Caption</label>
	{{ Form::textarea('post_text', null, ['class' => 'field','id' => 'ckeditor-el']) }}
</div>
<div style="clear:both;"></div>
<div class="ln_solid"></div>
	<div class="form-group py-3">
		<div class="col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
			<!--<a class="btn btn-primary" href="{{ url()->previous() }}">Cancel</a>-->
      <button type="submit" class="btn btn-lg btn-success">Submit</button>
    </div>
	</div>

{!! Form::close() !!}
</div>
<!-- /.card-body -->
</div>
<!-- /.card -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

  @endsection
