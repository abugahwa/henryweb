<?php

namespace App\Classes;

use Illuminate\Http\Request;

use App\Models\MVideos;
use App\Models\MVideoCategory;

class Videos extends BaseClass {

	public function getById($id) {
		return MVideos::findOrFail($id);
	}

	public function getAll($params) {
		$res = MVideos::where('is_active', 1)->where('is_deleted', 0);

		if(isset($params['limit'])) {
		    $res->limit($params['limit']);
		}
		if(isset($params['category'])) {
			$res->where('video_category', 'not like', '$'.$params['category'] .'%');
		}
		if(isset($params['query'])) {
		    $res->where('title', 'LIKE', '%'.$params['query'].'%')->orWhere('description', 'LIKE', '%'.$params['query'].'%');
		}
		if(isset($params['sortby'])) {
			if($params['sortby'] == "hits") {
	    	$res->orderBy('hits', 'DESC');
			} elseif($params['sortby'] == "rand") {
	    	$res->inRandomOrder();
			} else {
	    	$res->orderBy('id', 'DESC');
			}
		} else {
    	$res->orderBy('id', 'DESC');
		}
		if(isset($params['paginate'])) {
	    	return $res->paginate($params['paginate']);
		} else {
	    	return $res->get();
		}
	}
	public function InsertVideos(Request $request)
	{
		$video              		= new MVideos;

        $video->video_id		    = $request->video_id;
        $video->title		        = $request->title;
        $video->description			= $request->description;
        $video->hits	        	= 0;
        $video->is_active	        = 1;
        $video->is_deleted	        = 0;
        $video->deleted_at	        = 0;

		return $video;
	}
	public function FindsVideosByid($id)
	{
		return Mvideos::find($id);
	}
	public function UpdateVideos(Request $request, $id)
	{
		$video                      = MVideos::find($id);
        $video->video_id		    = $request->video_id;
        $video->title		        = $request->title;
        $video->description			= $request->description;
        $video->hits	        	= $request->hits;
        $video->is_active	        = 1;
        $video->is_deleted	        = 0;
		$video->deleted_at	        = 0;
		$video->video_category		= $request->video_category;

		return $video;
	}
	public function DeleteVideos(Request $request, $id)
	{
		return \DB::update('update videos set is_deleted = ? where id = ?',[1,$id]);
	}

	public function create($data)
	{
		$video 									= new MVideos;
    $video->title		        = $data->title;
    $video->video_id        = $data->video_id;
    $video->is_active	      = isset($data->is_active) ? 1 : 0;
    $video->description     = $data->description;
    $video->save();

		return [true, 'Video has been created!'];
	}

	public function update($data)
	{
		$video 									= $this->getById($data->id);
    $video->title		        = $data->title;
    $video->video_id        = $data->video_id;
    $video->is_active	      = isset($data->is_active) ? 1 : 0;
    $video->description     = $data->description;
    $video->update();

		return [true, 'Video has been updated!'];
	}

	public function addHits($id) {
		return MVideos::whereId($id)->increment('hits');
	}
	public function VideoByTitle($title, $paginate)
	{
		$video									= MVideos::where('title', 'LIKE', '%'.$title.'%');
		if($paginate > 0){
			$videos								= $video->orderBy('id', 'DESC')->paginate($paginate);
		}

		if(!empty($videos)){
			return $videos;
		}
	}
	public function VideoRand($title, $limit)
	{
		$video								= MVideos::where('title', 'LIKE', '%' .$title.'%')->inRandomOrder()->limit($limit)->get();
		return $video;
	}
	public function catVideoAll()
	{
		return MVideoCategory::all();
	}
	public function catVideoAlljoin()
	{
		return MVideoCategory::join('videos', 'video_category.id', 'videos.video_category')
							->select('video_category.id', 'video_category.name')
							->groupBy('video_category.id')->orderBy('video_category.sort_order', 'asc')->paginate(3);
	}
	public function videobycat($vidcat, $limit, $paginate)
	{
		$video		= MVideos::where('is_deleted', 0)->where('video_category', $vidcat);

		if($limit > 0){
			return $video->limit(10)->get();
		}
		if($paginate > 0){
			return $video->paginate($paginate);
		}
	}
	public function GetCatname($cat)
	{
		$cat		= MVideoCategory::where('id', $cat)->first();
		if(!empty($cat)){
			return $cat->name;
		}
	}
	public function GetCatId($id)
	{
		$cat		= MVideoCategory::where('id', $id)->first();
		if(!empty($cat)){
			return $cat->id;
		}
	}
	public function GetVideoCategoryByid($id)
	{
		$cat		= MVideos::where('id', $id)->first();
		if(!empty($cat)){
			return $cat->video_category;
		}
	}
}
