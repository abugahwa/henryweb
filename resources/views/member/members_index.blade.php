@extends('member.layouts.member')
@section('content')
<style>
.small-box {
    position: relative;
    display: block;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    margin-bottom: 15px;
}
.bg-aqua {
    background-color: #00c0ef !important;
}
.small-box > .inner {
    padding: 10px;
}
.small-box .icon {
    position: absolute;
    top: auto;
    bottom: 5px;
    right: 5px;
    z-index: 0;
    font-size: 90px;
    color: rgba(0, 0, 0, 0.15);
}
.small-box > .small-box-footer {
    position: relative;
    text-align: center;
    padding: 3px 0;
    color: #fff;
    color: rgba(255, 255, 255, 0.8);
    display: block;
    z-index: 10;
    background: rgba(0, 0, 0, 0.1);
    text-decoration: none;
}
.small-box h3, .small-box p {
    z-index: 5px;
}
.small-box p {
    font-size: 15px;
}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="main">
			<div class="row">
				<div class="col-lg-3 col-xs-6">
					<div class="small-box bg-aqua">
						<div class="inner">
							<h3 style="color: #fff;">{{ $total_hits }}</h3>
							<p style="color: #fff;">Viewer</p>
						</div>
						<div class="icon">
							<i class="ion fa fa-eye"></i>
						</div>
						<div href="#" class="small-box-footer">
							Viewer for a month <i class="fa fa-eye"></i>
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="panel panel-default">
						<div class="panel-heading">Your Article</div>
						<div class="panel-body">
							@foreach($post as $pos)
								<p><a target="_blank" href="{{ url($pos->post_alias.'.phtml')}}">{{ $pos->post_title }}</a> </p>
								<p><i class="fa fa-eye"></i> {{ $pos->post_hits }} </p>
							@endforeach
						
						</div>
					</div>
				</div>
            </div>
		</div>
	</div>
</div>
@endsection