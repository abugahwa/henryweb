<?php
namespace App\Classes;

use Illuminate\Http\Request;
use App\Models\MGallery;
use App\Models\MImageCategory;

use App\Classes\StringClass;

use DB;
use Carbon;

class Galleries extends BaseClass {

	public function getByGroup($params) {

		$res = MImageCategory::join('gallery', 'image_category.id', 'gallery.category_image');

		$res->select('image_category.id as id', 'gallery.gallery_file as thumbnail', 'image_category.category_image as title');

		$res->groupBy('image_category.id')->orderBy('image_category.id', 'DESC');

		if(isset($params['paginate'])) {
	    	return $res->paginate($params['paginate']);
		} else {
	    	return $res->get();
		}
	}

	public function getByAlbumID($id, $params) {

		$res = MGallery::where('category_image', $id);

		$res->orderBy('id', 'DESC');

		if(isset($params['paginate'])) {
	    	return $res->paginate($params['paginate']);
		} else {
	    	return $res->get();
		}
	}

	public function GetAll()
	{
		return MGallery::paginate(12);
	}

	public function InsertGalleries(Request $request)
	{
				$strings                        = new StringClass();
        $gallery_title                  = $strings->str2alias($request->gallery_title);

        $now                            = \Carbon\Carbon::now();
				$year                           = date('Y', strtotime($now));
				$month                          = date('m', strtotime($now));
        $days                           = date('d', strtotime($now));

        $bs                             = $request->file('b_image')->getClientOriginalExtension();
        $nombreCarpeta                  = preg_replace('/\s+/', '.', $year . "/" . $month . "/" . $days);
        $fileimg                        = $gallery_title . "-" . rand(0,1000) . '.' .$bs;
        $b_image                        = 'img/gallery/'.$nombreCarpeta .'/' .$fileimg;
        $path                           = base_path() .'/public/img/gallery/'.$nombreCarpeta;

        $gal                           = new MGallery;
        $gal->gallery_title             = $request->gallery_title;
        $gal->gallery_caption           = $request->gallery_caption;
        $gal->gallery_status            = $request->gallery_status;
        $gal->category_image            = $request->category_image;
        $gal->gallery_file              = $b_image;

        $gal->save();

        $imageName                      = $strings->str2alias($gal->gallery_title);
        $nmImg                          = strtolower($imageName) . '.' .

        $request->file('b_image')->getClientOriginalExtension();
        $request->file('b_image')->move($path, $fileimg);

		return $gal;
	}
	public function FindById($id)
	{
		return MGallery::find($id);
	}
	public function PutCatImg($id)
	{
		return MGallery::where('id', $id)->first()->category_image;
	}
	public function PutStatus($id)
	{
		return MGallery::where('id', $id)->first()->category_status;
	}
	public function UpdateGalleries(Request $request, $id)
	{
		$strings                		= new StringClass();
        $gallery_title          		= $strings->str2alias($request->gallery_title);

        $b_title             			= strtolower($gallery_title);

        if($request->hasFile('b_image')) {
        $strings                        = new StringClass();
        $gallery_title                  = $strings->str2alias($request->gallery_title);

        $now                            = \Carbon\Carbon::now();
		$year                           = date('Y', strtotime($now));
		$month                          = date('m', strtotime($now));
        $days                           = date('d', strtotime($now));

        $bs                             = $request->file('b_image')->getClientOriginalExtension();
        $nombreCarpeta                  = preg_replace('/\s+/', '.', $year . "/" . $month . "/" . $days);
        $fileimg                        = $gallery_title . '.' .$bs;
        $b_image                        = 'img/gallery/'.$nombreCarpeta .'/' .$fileimg;
        $path                           = base_path() .'/public/img/gallery/'.$nombreCarpeta;
        }else{
            $b_image                = $request->gallery_file;
        }

        $gal                        = MGallery::find($id);
        $gal->gallery_title         = $request->gallery_title;
        $gal->gallery_caption       = $request->gallery_caption;
        $gal->gallery_status        = 1;
        $gal->category_image        = $request->category_image;
        $gal->gallery_file          = $b_image;



        if($request->hasFile('b_image')) {
            $imageName = $strings->str2alias($gal->gallery_title);
            $nmImg      = strtolower($imageName) . '.' .
            $request->file('b_image')->getClientOriginalExtension();
            $request->file('b_image')->move($path, $nmImg);
        }
        $gal->save();

		return $gal;
	}
	public function FindGalleriesId(Request $request, $id)
	{
		return MGallery::FindOrFail($id);
	}
	public function PutTitleGalleries($id)
	{
		return MGallery::where('id', $id)->first()->gallery_title;
	}
	public function PutGalleriesFile($id)
	{
		return MGallery::where('id', $id)->first()->gallery_file;
	}
	public function IDbyALias($alias, $limit, $paginate)
	{
		$galleries			= MImageCategory::join('gallery', 'image_category.id', 'gallery.category_image')
										->where('image_category.category_image', 'LIKE', '%'.$alias.'%');

		if($limit > 0){
			$galleriesP	= $galleries->orderBy('gallery.created_at', 'desc')->limit($limit)->get();
		}
		if($paginate > 0){
			$galleriesP	= $galleries->orderBy('gallery.created_at', 'desc')->paginate($paginate);
		}
		if(!empty($galleriesP)){
				return $galleriesP;
		}
	}
	public function ImageBYCatID($cat_id, $limit, $paginate)
	{
			$gallery				= MGallery::where('category_image', $cat_id);
			if($limit > 0){
					$galleries	= $gallery->orderBy('created_at', 'desc')->limit($limit)->get();
			}
			if($paginate > 0){
					$galleries	= $gallery->orderBy('created_at', 'desc')->paginate($paginate);
			}
			if(!empty($galleries)){
					return $galleries;
			}
	}
	public function GalleriByCatIdNTitle($title, $cat)
	{

			$galleri				= MGallery::where('gallery_title', 'LIKE', '%' . $title . '%')
											->where(function($query) use ($cat){
												$query->where('category_image', $cat);
											})
											->paginate(12);

			if(!empty($galleri))
			{
				return $galleri;
			}
	}
}
