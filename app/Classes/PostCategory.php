<?php

namespace App\Classes;

use Illuminate\Http\Request;

use Auth;
use App\Models\MPost;
use App\Models\MPostCategory;

use App\Classes\StringClass;
use App\Classes\Posts;
use App\Classes\PostCategory;

use DB;
use Carbon;

class PostCategory extends BaseClass {

	public function getByAlias($alias) {
		return MPostCategory::where('category_alias', $alias)->firstOrFail();
	}
	public function GetAll()
	{
		return MPostCategory::where('category_status', 1)->paginate(10);
	}
	public function FindById($id)
	{
		return MPostCategory::find($id);
	}
  public function FirstAliasByID($id)
  {
    $alias   = MPostCategory::where('id', $id)->first();
    if(!empty($alias)){
      return $alias->category_alias;
    }
  }
	public function PluckCat()
	{
		return MPostCategory::pluck('category_name', 'id');
	}
  public function NameCatByAlias($alias)
  {
    $cat      = MPostCategory::where('category_alias','LIKE', $alias)->first();
    if(!empty($cat)){
      return $cat->category_name;
    }
  }
	public function InsertCategory(Request $request)
	{
		$strings                	= new StringClass();
        $alias		             	= $strings->str2alias($request->category_name);

		$cats						= new MPostCategory;
		$cats->category_name		= $request->category_name;
		$cats->category_alias		= $alias;
		$cats->parent_id			= $request->parent_id;
		$cats->sort_order			= $request->sort_order;
		$cats->category_status		= $request->category_status;

		$cats->save();

		return $cats;
	}
	public function UpdateCategory(Request $request, $id)
	{
		$strings                	= new StringClass();
        $alias		             	= $strings->str2alias($request->category_name);

		$cats						= MPostCategory::find($id);
		$cats->category_name		= $request->category_name;
		$cats->category_alias		= $alias;
		$cats->parent_id			= $request->parent_id;
		$cats->sort_order			= $request->sort_order;
		$cats->category_status		= $request->category_status;

		$cats->save();

		return $cats;
	}
	public function DeleteCategory(Request $request, $id)
	{
		return \DB::update('update post_categories set category_status = ? where id = ?',[0,$id]);
	}
	public function GetCatByParentNull($parent)
	{
		return MPostCategory::where('parent_id', $parent)->get();
	}
  public function GetIDByParent($id)
  {
    $parent   = MPostCategory::where('parent_id', $id)->get();
    return $parent;
  }
	public function GetIdByAlias($alias)
	{
		$cat			= MPostCategory::where('category_name', 'LIKE', '%'. $alias .'%')->first();
		if(!empty($cat)){
				return $cat->id;
		}
	}
	public function GetNameByAlias($alias)
	{
			$cat		= MPostCategory::where('category_alias', 'LIKE', '%'. $alias .'%')->first();
			if(!empty($cat)){
					return $cat->id;
			}
	}
	public function GetParentByID($id)
	{
		$paretn		= MPostCategory::where('parent_id', $id)->get();
		return $paretn;
	}


		public function getCategoryIdByParentId($id, $params)
		{
			$parent		= MPostCategory::where('parent_id', $id)->pluck('id');
			if($params['toArray']) {
				$parent = $parent->toArray();
			}
			return $parent;
		}

}
