<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="http://leafo.net/sticky-kit/src/jquery.sticky-kit.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.2/jquery.fancybox.min.js"></script>
<script type="text/javascript">
$('#Achiev').on('slide.bs.carousel', function (e) {
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});

$('#Achiev').carousel({
    interval: 4000
});
$('#LMedia').on('slide.bs.carousel', function (e) {
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
          if (e.direction=="left") {
              $('.carousel-item').eq(i).appendTo('.carousel-inner');
          }else{
              $('.carousel-item').eq(0).appendTo('.carousel-inner');
          }
        }
      }
});

$('#LMedia').carousel({
    interval: 4000
});

$('#GalleriesHome').on('slide.bs.carousel', function (e) {
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
          if (e.direction=="left") {
              $('.carousel-item').eq(i).appendTo('.carousel-inner');
          }else{
              $('.carousel-item').eq(0).appendTo('.carousel-inner');
          }
        }
      }
});

$('#GalleriesHome').carousel({
    interval: 3334000
});


</script>

<script type="text/javascript">
$(".sticky_column").stick_in_parent({
    });

$('[data-fancybox="images"]').fancybox({
  infobar : false,
  openEffect	: 'elastic',
	closeEffect	: 'elastic',
  overlay: {
    css: {'background-color': '#6cd900'}
  },
  helpers : {
    		title : {
    			type : 'inside'
    		}
    	}
})

$(function() {
    $("#video-player a").on("click", function() {
          $(".videoarea iframe").remove();
          $('<iframe width="720" height="405" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>')
              .attr("src", 'https://www.youtube-nocookie.com/embed/'+$(this).attr('videoid')+'?rel=0&amp;controls=0')
              .appendTo(".videoarea");
    });
    $("#video-player a").first().trigger('click');


    $("#gallery-player a").on("click", function() {
          $("#gallery-player .area img").remove();
          $('<img class="img-responsive" src="" alt="">')
              .attr("src", $(this).attr('photo-src'))
              .appendTo("#gallery-player .area");
    });
    $("#gallery-player a").first().trigger('click');

});


</script>
