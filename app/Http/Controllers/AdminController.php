<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\MPost;
use App\Classes\Report;

use Carbon;
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	protected $report;
    public function __construct()
    {
        $this->middleware('auth');
		$this->report = new Report;
    }
    public function index()
    {
        $user 			= User::count();
		$company 		= User::count();

		$dates = new Carbon\Carbon;
		$dates->addDays(30);

		$post				= MPost::where('post_author',\Auth::user()->id)->where('created_at','<', $dates->toDateTimeString())->orderBy('post_hits', 'DESC')->limit(10)->get();


		$dateS 					=  Carbon\Carbon::now()->startOfMonth()->subMonth(12);
		$dateE 					=  Carbon\Carbon::now()->startOfMonth();

		$startMonth				= $dateS->subMonth()->format('F Y');
		$endMonth				= $dateE->format('F Y');

		$chrt					= $this->report->ViewerByuser();

        return view('home', compact('cat','user','company','post','chrt'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
